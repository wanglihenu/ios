//
//  GlenCountryTableViewCell.m
//  GlenBit
//
//  Created by Lee on 2019/1/24.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenCountryTableViewCell.h"

@interface GlenCountryTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *titlte;

@end

@implementation GlenCountryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setModel:(id)model{
    if ([model isKindOfClass:[NSString class]]) {
        self.titlte.text = model;
    }
    if ([model isKindOfClass:[GlenCountryModel class]]) {
        _model = model;
        GlenCountryModel *te = model;
        self.titlte.text = te.countryCode.displayValue;
    }
    
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
