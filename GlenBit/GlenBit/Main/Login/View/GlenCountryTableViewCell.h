//
//  GlenCountryTableViewCell.h
//  GlenBit
//
//  Created by Lee on 2019/1/24.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlenCountryTableViewCell : UITableViewCell

@property (strong, nonatomic)  id model;

@end
