//
//  GlenGoogleTwoStepView.m
//  GlenBit
//
//  Created by Lee on 2019/1/25.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenGoogleTwoStepView.h"

@interface GlenGoogleTwoStepView ()
@property (weak, nonatomic) IBOutlet UITextField *codeText;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *tips2;
@property (weak, nonatomic) IBOutlet UIButton *confiBtn;

@end

@implementation GlenGoogleTwoStepView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"GlenGoogleTwoStepView" owner:self options:nil] firstObject];
        self.frame = frame;
        _codeText.layer.borderColor = [UIColor whiteColor].CGColor;
        _codeText.layer.borderWidth = 1;
        _titleLab.text = kLocalizedString(@"GOOGLE_TWO_STEP", @"");
        _tips2.text = kLocalizedString(@"ENTERGOOGLECODE", @"");
        [_confiBtn setTitle:kLocalizedString(@"Determine", @"") forState:UIControlStateNormal];
    }
    return self;
}
- (IBAction)close:(id)sender {
    if (self.actionType) {
        self.actionType(0, @"0");
    }
}
- (IBAction)confirm:(id)sender {
    if (_codeText.text.length == 0) {
        [[Toast makeUpText:kLocalizedString(@"ENTERGOOGLECODE", @"")] showWithType:ShortTime];
        return;
    }
    if (self.actionType) {
        self.actionType(1, _codeText.text);
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
