//
//  GlenVerifyEmailViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/5.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenVerifyEmailViewController.h"
#import "GlenResetPassViewController.h"
#import "GlenProgressView.h"
#import "GlenNewPassWordViewController.h"
#import "GlenContryCountViewController.h"

@interface GlenVerifyEmailViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet UIView *emaiView;
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet GlenProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *detail;

@property (weak, nonatomic) IBOutlet UIButton *loginType1;
@property (weak, nonatomic) IBOutlet UIButton *loginType2;
@property (assign, nonatomic)  NSInteger loginType;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectWidth;

@property (weak, nonatomic) IBOutlet UILabel *quhao;

@property (strong, nonatomic)  GlenCountryModel *codeModel;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIButton *getCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;


@end

@implementation GlenVerifyEmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.top.constant = StatusBar_height + 60;
    self.emaiView.layer.borderColor = [UIColor colorWithHexString:@"#353B41"].CGColor;
    self.emaiView.layer.borderWidth = 1;
    _loginType = 0;
    [_backBtn setTitle:kLocalizedString(@"<LOGIN", @"登录") forState:UIControlStateNormal];
    _titleLab.text = kLocalizedString(@"RESETLOGIN", @"验证");
    [_emailText addTarget:self action:@selector(changedTextField:) forControlEvents:UIControlEventEditingChanged];
    [_progressView showProgressViewWithTitleArr:@[kLocalizedString(@"VERIFY", @"验证"),kLocalizedString(@"ENTERNEWPW", @"重置密码")] curentIndex:0];
    _codeModel = self.countryData[0];
    self.quhao.text = [NSString stringWithFormat:@"+%@",_codeModel.countryCode.rawValue];
    [_loginType1 setTitle:kLocalizedString(@"EMAIL", @"电子邮箱") forState:UIControlStateNormal];
    [_loginType2 setTitle:kLocalizedString(@"Phone", @"手机号") forState:UIControlStateNormal];
    _emailText.placeholder = kLocalizedString(@"EMAIL_PLZ", @"请填写邮箱地址");
    _detail.text = @"";
    [_getCodeBtn setTitle:kLocalizedString(@"Tips", @"获取验证码") forState:UIControlStateNormal];
}
- (void)setCountryData:(NSArray *)countryData{
    _countryData = countryData;
}
- (IBAction)tap:(id)sender {
    GlenWeakSelf;
    GlenContryCountViewController *conty = [GlenContryCountViewController new];
    conty.countryData = _countryData;
    conty.valueDidSelect = ^(NSInteger row) {
        weakSelf.codeModel = weakSelf.countryData[row];
        weakSelf.quhao.text = [NSString stringWithFormat:@"+%@",weakSelf.codeModel.countryCode.rawValue];
    };
    [self.navigationController pushViewController:conty animated:YES];
}
- (IBAction)changeLoginType:(UIButton *)sender {
    
    _loginType = sender.tag - 10;
    if (_loginType == 0) {
        [_loginType1 setTitle:kLocalizedString(@"EMAIL", @"电子邮箱") forState:UIControlStateNormal];
        [_loginType2 setTitle:kLocalizedString(@"Phone", @"手机号") forState:UIControlStateNormal];
        _emailText.placeholder = kLocalizedString(@"EMAIL_PLZ", @"请填写邮箱地址");
        _selectWidth.constant = 0;
    }else{
        [_loginType1 setTitle:@"Phone" forState:UIControlStateNormal];
        [_loginType2 setTitle:@"Email" forState:UIControlStateNormal];
        _emailText.placeholder = kLocalizedString(@"Eenter_Phone_Number", @"请输入手机号");
        _selectWidth.constant = 99;
    }
}
-(void)changedTextField:(UITextField *)textField
{
    if (textField.text.length == 0) {
        _detail.text = @"";
    }else{
        _detail.text = [NSString stringWithFormat:@"%@ %@",kLocalizedString(@"SENDCODETO", @"发送验证码到"),textField.text];
    }
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)getCode:(id)sender {
    
    if (_loginType == 0) {
        if (![_emailText.text isEmail]) {
            [[Toast makeUpText:kLocalizedString(@"EMAIL_INVALID", @"请正确输入电子邮箱")] showWithType:ShortTime];
            return;
        }
    }else{
        if (_emailText.text.length == 0) {
            [[Toast makeUpText:kLocalizedString(@"Valid_Phone_Number", @"请输入正确的手机号")] showWithType:ShortTime];
            return;
        }
    }

    GlenNewPassWordViewController *signup = [GlenNewPassWordViewController new];
    signup.email = self.emailText.text;
    signup.type = _loginType;
    signup.contryCount = _codeModel.countryCode.rawValue;
    [self.navigationController pushViewController:signup animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
