//
//  GlenResetPassViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/5.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenResetPassViewController.h"
#import "GlenProgressView.h"

@interface GlenResetPassViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet UIView *emaiView;
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet GlenProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *detail;
@property (weak, nonatomic) IBOutlet UIButton *resendBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIButton *sumBtnStr;
@property (weak, nonatomic) IBOutlet UILabel *enterCode;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation GlenResetPassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.top.constant = StatusBar_height + 60;
    self.emaiView.layer.borderColor = [UIColor colorWithHexString:@"#353B41"].CGColor;
    self.emaiView.layer.borderWidth = 1;
    GlenWeakSelf;
    [_backBtn setTitle:kLocalizedString(@"<LOGIN", @"登录") forState:UIControlStateNormal];
    [_sumBtnStr setTitle:kLocalizedString(@"SUBMIT", @"提交") forState:UIControlStateNormal];
    _titleLab.text = kLocalizedString(@"RESETLOGIN", @"验证");
    _detail.text = [NSString stringWithFormat:@"%@ %@",kLocalizedString(@"SENDCODETO", @"发送验证码到"),_email];
    [_progressView showProgressViewWithTitleArr:@[kLocalizedString(@"VERIFY", @"验证"),kLocalizedString(@"ENTERNEWPW", @"重置密码")] curentIndex:1];
    _detail.text = [NSString stringWithFormat:@"A confirmation email has been sent to your bind email %@. Please check your mailbox.",_email];
    _enterCode.text = kLocalizedString(@"Verification_Code", @"验证码");
    _emailText.placeholder = kLocalizedString(@"Enter_verification_Code", @"请输入验证码");
    [BYTimer timerWithTimeout:60 handlerBlock:^(int presentTime) {
        [weakSelf.resendBtn setTitle:[NSString stringWithFormat:@"%ds",presentTime] forState:UIControlStateNormal];
    } finish:^{
        [weakSelf.resendBtn setTitle:[NSString stringWithFormat:@"%@",kLocalizedString(@"Tips", @"获取验证码")] forState:UIControlStateNormal];
    }];
    
}
- (void)setEmail:(NSString *)email{
    _email = email;
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)submit:(id)sender {
    if (_emailText.text.length == 0) {
        [[Toast makeUpText:kLocalizedString(@"Enter_verification_Code", @"请输入验证码")] showWithType:ShortTime];
        return;
    }
    GlenWeakSelf;
    [self showHUBWith:@""];
    if (_type == 0) {
        [[GlenHttpTool shareInstance] resetPassWordWithEmail:_email passWord:_passWord code:_emailText.text Completion:^(NSString *succes) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:succes] showWithType:ShortTime];
        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:error] showWithType:ShortTime];
        }];
    }else{
        [[GlenHttpTool shareInstance] resetPassWordWithPhone:_email CountryCode:_contryCount passWord:_passWord code:_emailText.text Completion:^(NSString *succes) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:succes] showWithType:ShortTime];
        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:error] showWithType:ShortTime];
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
