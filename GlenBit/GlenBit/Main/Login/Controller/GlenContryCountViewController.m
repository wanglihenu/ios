//
//  GlenContryCountViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/24.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenContryCountViewController.h"
#import "GlenCountryTableViewCell.h"

@interface GlenContryCountViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@end

@implementation GlenContryCountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hideNav = YES;
    if (_type == 2) {
        _titleLab.text = kLocalizedString(@"Language", @"选择国家");
    }else{
        _titleLab.text = kLocalizedString(@"County", @"选择国家");
    }
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    [_tableView registerNib:[UINib nibWithNibName:@"GlenCountryTableViewCell" bundle:nil] forCellReuseIdentifier:@"GlenCountryTableViewCell"];
}
- (void)setType:(NSInteger)type{
    _type = type;
}
- (void)setCountryData:(NSArray *)countryData{
    _countryData = countryData;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _countryData.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GlenCountryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GlenCountryTableViewCell" forIndexPath:indexPath];
    cell.model = _countryData[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.valueDidSelect) {
        self.valueDidSelect(indexPath.row);
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)back:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
