//
//  GlenContryCountViewController.h
//  GlenBit
//
//  Created by Lee on 2019/1/24.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBaseViewController.h"

@interface GlenContryCountViewController : GlenBaseViewController

@property (assign, nonatomic) NSInteger type;

@property (strong, nonatomic) NSArray *countryData;

@property (nonatomic, copy) void (^valueDidSelect)(NSInteger row);

@end
