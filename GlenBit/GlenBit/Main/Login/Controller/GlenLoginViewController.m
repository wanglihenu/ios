//
//  GlenLoginViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/5.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenLoginViewController.h"
#import "GlenSignUpViewController.h"
#import "GlenVerifyEmailViewController.h"
#import "GlenContryCountViewController.h"

#import "GlenGoogleTwoStepView.h"
#import "GKCover.h"

@interface GlenLoginViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet UIView *emaiView;
@property (weak, nonatomic) IBOutlet UIView *passWordView;
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UITextField *passWordText;

@property (weak, nonatomic) IBOutlet UIButton *loginType1;
@property (weak, nonatomic) IBOutlet UIButton *loginType2;
@property (assign, nonatomic)  NSInteger loginType;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectWidth;

@property (strong, nonatomic)  NSMutableArray *country;
@property (strong, nonatomic)  GlenCountryModel *codeModel;

@property (weak, nonatomic) IBOutlet UILabel *quhao;


@property (weak, nonatomic) IBOutlet UILabel *loginStr;
@property (weak, nonatomic) IBOutlet UILabel *passWordStr;
@property (weak, nonatomic) IBOutlet UIButton *forgetPassWord;
@property (weak, nonatomic) IBOutlet UIButton *loginBtnStr;
@property (weak, nonatomic) IBOutlet UILabel *detaiStr;
@property (weak, nonatomic) IBOutlet UIButton *signUpStr;



@end

@implementation GlenLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.top.constant = StatusBar_height + 60;
    _loginStr.text = kLocalizedString(@"LOGIN", @"登录");
    [_loginBtnStr setTitle:kLocalizedString(@"LOGIN", @"登录") forState:UIControlStateNormal];
    _passWordStr.text = kLocalizedString(@"PASSWORD", @"密码");
    [_forgetPassWord setTitle:kLocalizedString(@"FORGET_PASSWORD", @"忘记密码?") forState:UIControlStateNormal];
    _detaiStr.text = kLocalizedString(@"NOT_USER_YET", @"还没有GlenBit的账户?");
    [_signUpStr setTitle:kLocalizedString(@"SIGNUP_NOW", @"立即注册") forState:UIControlStateNormal];
    [_loginType1 setTitle:kLocalizedString(@"EMAIL", @"电子邮箱") forState:UIControlStateNormal];
    [_loginType2 setTitle:kLocalizedString(@"Phone", @"手机号") forState:UIControlStateNormal];

    
    _emailText.placeholder = kLocalizedString(@"EMAIL_PLZ", @"请填写邮箱地址");
    _passWordText.placeholder = kLocalizedString(@"Please_fill_password", @"请输入密码");
    _loginType = 0;
    self.passWordView.layer.borderColor = self.emaiView.layer.borderColor = [UIColor colorWithHexString:@"#353B41"].CGColor;
    self.passWordView.layer.borderWidth = self.emaiView.layer.borderWidth = 1;
    [self getCountryData];
}
- (void)getCode{
    
    GlenWeakSelf;
    GlenGoogleTwoStepView *google = [[GlenGoogleTwoStepView alloc]initWithFrame:CGRectMake(0, 0, size_width - 20, 240)];
    google.actionType = ^(NSInteger index, NSString *code) {
        if (index == 0) {
            [GKCover hide];
        }else if (index == 1){
            NSString *phone;
            if (weakSelf.loginType == 0) {
                phone = weakSelf.emailText.text;
            }else{
                phone =[NSString stringWithFormat:@"%@%@",weakSelf.codeModel.countryCode.rawValue,weakSelf.emailText.text];
            }
            [GKCover hide];
            [[GlenHttpTool shareInstance] login_gaWithEmail:phone password:weakSelf.passWordText.text token:code Completion:^(UserModel *users) {
                [weakSelf hiddenHUB];
                
                [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%ld",(long)weakSelf.loginType] forKey:@"loginType"];
                [[NSUserDefaults standardUserDefaults] setValue:weakSelf.emailText.text forKey:@"phone"];
                [[NSUserDefaults standardUserDefaults] setValue:weakSelf.codeModel.countryCode.rawValue forKey:@"countyCode"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                [UerManger shareUserManger].loginType = weakSelf.loginType;
                if (weakSelf.loginSuccess) {
                    weakSelf.loginSuccess(YES);
                    [weakSelf dismissViewControllerAnimated:YES completion:nil];
                }else{
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
                
            } fail:^(NSString *error) {
                [weakSelf hiddenHUB];
                [[Toast makeUpText:error] showWithType:ShortTime];
            }];
        }
    };
    [GKCover coverFrom:[UIApplication sharedApplication].keyWindow contentView:google style:GKCoverStyleTranslucent showStyle:GKCoverShowStyleCenter showAnimStyle:GKCoverShowAnimStyleCenter hideAnimStyle:GKCoverHideAnimStyleCenter notClick:NO showBlock:^{
        
    } hideBlock:^{
        [UerManger cleanLogin];
    }];

}
- (void)getCountryData{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    for (int i = 0; i < json.count; i++) {
        // Manually
        GlenCountryModel *model = [GlenCountryModel yy_modelWithDictionary:json[i]];
        [self.country addObject:model];
    }
    _codeModel = self.country[0];
    self.quhao.text = [NSString stringWithFormat:@"+%@",_codeModel.countryCode.rawValue];
}
- (NSMutableArray *)country{
    if (!_country) {
        _country = [NSMutableArray new];
    }
    return _country;
}
- (IBAction)tap:(id)sender {
    GlenWeakSelf;
    GlenContryCountViewController *conty = [GlenContryCountViewController new];
    conty.countryData = _country;
    conty.valueDidSelect = ^(NSInteger row) {
        weakSelf.codeModel = self.country[row];
        weakSelf.quhao.text = [NSString stringWithFormat:@"+%@",weakSelf.codeModel.countryCode.rawValue];
    };
    [self.navigationController pushViewController:conty animated:YES];
}
- (IBAction)changeLoginType:(UIButton *)sender {
    
    _loginType = sender.tag - 10;
    if (_loginType == 0) {
        [_loginType1 setTitle:kLocalizedString(@"EMAIL", @"电子邮箱") forState:UIControlStateNormal];
        [_loginType2 setTitle:kLocalizedString(@"Phone", @"手机号") forState:UIControlStateNormal];
        _emailText.placeholder = kLocalizedString(@"EMAIL_PLZ", @"请填写邮箱地址");
        _selectWidth.constant = 0;
    }else{
        [_loginType1 setTitle:kLocalizedString(@"Phone", @"手机号") forState:UIControlStateNormal];
        [_loginType2 setTitle:kLocalizedString(@"EMAIL", @"电子邮箱") forState:UIControlStateNormal];
        _emailText.placeholder = kLocalizedString(@"Eenter_Phone_Number", @"请输入手机号");
        _selectWidth.constant = 99;
    }
}

- (IBAction)signUp:(id)sender {
    GlenSignUpViewController *signup = [GlenSignUpViewController new];
    signup.countryData = _country;
    [self.navigationController pushViewController:signup animated:YES];
}
- (IBAction)forgetPassWord:(id)sender {
    GlenVerifyEmailViewController *signup = [GlenVerifyEmailViewController new];
    signup.countryData = _country;
    [self.navigationController pushViewController:signup animated:YES];
}
- (IBAction)login:(id)sender {
    
    if (_loginType == 0) {
        if (![_emailText.text isEmail]) {
            [[Toast makeUpText:kLocalizedString(@"EMAIL_INVALID", @"请正确输入电子邮箱")] showWithType:ShortTime];
            return;
        }
        if (_passWordText.text.length < 6) {
            [[Toast makeUpText:kLocalizedString(@"PASSWORD_INVALID", @"密码应由至少6位的字符组成")] showWithType:ShortTime];
            return;
        }
//        [self showHUBWith:@""];
        GlenWeakSelf;
        [[GlenHttpTool shareInstance] loginWithEmail:_emailText.text password:_passWordText.text Completion:^(UserModel *user) {
            [weakSelf hiddenHUB];
//            if (weakSelf.loginSuccess) {
//                weakSelf.loginSuccess(YES);
//            }
                [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%ld",(long)weakSelf.loginType] forKey:@"loginType"];
                [[NSUserDefaults standardUserDefaults] setValue:weakSelf.emailText.text forKey:@"phone"];
                [[NSUserDefaults standardUserDefaults] setValue:weakSelf.codeModel.countryCode.rawValue forKey:@"countyCode"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                [UerManger shareUserManger].loginType = weakSelf.loginType;
            if (weakSelf.loginSuccess) {
                weakSelf.loginSuccess(YES);
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
            }else{
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }
            
        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
            if ([error isEqualToString:@"4006"]) {
                [weakSelf getCode];
            }else{
            [[Toast makeUpText:error] showWithType:ShortTime];
            }
        }];
    }else{
        if (_emailText.text.length == 0) {
            [[Toast makeUpText:kLocalizedString(@"Valid_Phone_Number", @"请输入正确的手机号")] showWithType:ShortTime];
            return;
        }
        if (_passWordText.text.length < 6) {
            [[Toast makeUpText:kLocalizedString(@"PASSWORD_INVALID", @"密码应由至少6位的字符组成")] showWithType:ShortTime];
            return;
        }
//        [self showHUBWith:@""];
        GlenWeakSelf;
        [[GlenHttpTool shareInstance] loginWithEmail:[NSString stringWithFormat:@"%@%@",_codeModel.countryCode.rawValue,_emailText.text] password:_passWordText.text Completion:^(UserModel *user) {
            [weakSelf hiddenHUB];
            if (user.isTwoStepEnabled) {
                [weakSelf getCode];
            }else{
                [UerManger shareUserManger].loginType = weakSelf.loginType;
                [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%ld",(long)weakSelf.loginType] forKey:@"loginType"];
                [[NSUserDefaults standardUserDefaults] setValue:weakSelf.emailText.text forKey:@"phone"];
                [[NSUserDefaults standardUserDefaults] setValue:weakSelf.codeModel.countryCode.rawValue forKey:@"countyCode"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                if (weakSelf.loginSuccess) {
                    weakSelf.loginSuccess(YES);
                    [weakSelf dismissViewControllerAnimated:YES completion:nil];
                }else{
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }

                
            }
        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
            if ([error isEqualToString:@"4006"]) {
                [weakSelf getCode];
            }else{
                [[Toast makeUpText:error] showWithType:ShortTime];
            }
        }];
    }
    
}
- (void)openRight{
    
    if (self.loginSuccess) {
        self.loginSuccess(NO);
    }
    if ([self isPresent]) {
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
