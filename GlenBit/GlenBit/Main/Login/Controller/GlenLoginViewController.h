//
//  GlenLoginViewController.h
//  GlenBit
//
//  Created by Lee on 2019/1/5.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBaseViewController.h"

@interface GlenLoginViewController : GlenBaseViewController

@property (nonatomic, copy) void(^loginSuccess)(BOOL loginSuccess);

@end
