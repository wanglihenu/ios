//
//  GlenSignUpViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/5.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenSignUpViewController.h"
#import "GlenEmailActivationViewController.h"
#import "GlenContryCountViewController.h"
#import "EWebViewController.h"


@interface GlenSignUpViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet UIView *emaiView;
@property (weak, nonatomic) IBOutlet UIView *passWordView;
@property (weak, nonatomic) IBOutlet UIView *enPassWordView;
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UITextField *passWordText;
@property (weak, nonatomic) IBOutlet UITextField *enPassWordText;
@property (weak, nonatomic) IBOutlet UIView *codeView;
@property (weak, nonatomic) IBOutlet UIView *referralView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *codeHight;
@property (weak, nonatomic) IBOutlet UITextField *codeText;
@property (weak, nonatomic) IBOutlet UITextField *referralText;


@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;

@property (assign, nonatomic)  NSInteger loginType;
@property (weak, nonatomic) IBOutlet UIButton *loginType1;
@property (weak, nonatomic) IBOutlet UIButton *loginType2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectWidth;
@property (weak, nonatomic) IBOutlet UILabel *quhao;

@property (strong, nonatomic)  GlenCountryModel *codeModel;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;

@property (weak, nonatomic) IBOutlet UILabel *signUpStr;

@property (weak, nonatomic) IBOutlet UILabel *codeStr;
@property (weak, nonatomic) IBOutlet UILabel *passWordStr;
@property (weak, nonatomic) IBOutlet UILabel *enPassWordStr;
@property (weak, nonatomic) IBOutlet UILabel *referStr;
@property (weak, nonatomic) IBOutlet UILabel *agreeStr;
@property (weak, nonatomic) IBOutlet UIButton *Service;
@property (weak, nonatomic) IBOutlet UIButton *sumBtnStr;

@end

@implementation GlenSignUpViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.top.constant = StatusBar_height + 60;
    
    
    _signUpStr.text = kLocalizedString(@"SIGNUP", @"注册");
    _codeStr.text = kLocalizedString(@"Verification_Code", @"验证码");
    [_sumBtnStr setTitle:kLocalizedString(@"SUBMIT", @"提交") forState:UIControlStateNormal];
    _passWordStr.text = kLocalizedString(@"PASSWORD", @"密码");
    _enPassWordStr.text = kLocalizedString(@"COMFIRM_PASSWORD", @"确认密码");
    _agreeStr.text = kLocalizedString(@"AGREED", @"我已经阅读并同意");
    [_sumBtnStr setTitle:kLocalizedString(@"SUBMIT", @"提交") forState:UIControlStateNormal];
    [_loginType1 setTitle:kLocalizedString(@"EMAIL", @"电子邮箱") forState:UIControlStateNormal];
    [_loginType2 setTitle:kLocalizedString(@"Phone", @"手机号") forState:UIControlStateNormal];
    [_sendBtn setTitle:kLocalizedString(@"Tips", @"获取验证码") forState:UIControlStateNormal];
    [_Service setTitle:kLocalizedString(@"TERMS_OF_SERVICE", @"服务条款") forState:UIControlStateNormal];
    
    _emailText.placeholder = kLocalizedString(@"EMAIL_PLZ", @"请填写邮箱地址");
    _passWordText.placeholder = kLocalizedString(@"Please_fill_password", @"请输入密码");
    _enPassWordText.placeholder = kLocalizedString(@"CONFIRM_PASSWORD_PLZ", @"请再次输入相同的密码");
    _codeText.placeholder = kLocalizedString(@"Enter_verification_Code", @"请输入验证码");
    
    
    
    
    _codeHight.constant = 0;
    _selectWidth.constant = 0;
    self.referralView.layer.borderColor = self.codeView.layer.borderColor = self.enPassWordView.layer.borderColor = self.passWordView.layer.borderColor = self.emaiView.layer.borderColor = [UIColor colorWithHexString:@"#353B41"].CGColor;
    self.referralView.layer.borderWidth = self.codeView.layer.borderWidth = self.enPassWordView.layer.borderWidth = self.passWordView.layer.borderWidth = self.emaiView.layer.borderWidth = 1;
    _codeModel = self.countryData[0];
    self.quhao.text = [NSString stringWithFormat:@"+%@",_codeModel.countryCode.rawValue];
}
- (void)setCountryData:(NSArray *)countryData{
    _countryData = countryData;
}
- (IBAction)xiey:(id)sender {
    EWebViewController * web = [EWebViewController new];
    web.isXieyi = YES;
    [self.navigationController pushViewController:web animated:YES];
}

- (IBAction)tap:(id)sender {
    GlenWeakSelf;
    GlenContryCountViewController *conty = [GlenContryCountViewController new];
    conty.countryData = _countryData;
    conty.valueDidSelect = ^(NSInteger row) {
        weakSelf.codeModel = weakSelf.countryData[row];
        weakSelf.quhao.text = [NSString stringWithFormat:@"+%@",weakSelf.codeModel.countryCode.rawValue];
    };
    [self.navigationController pushViewController:conty animated:YES];
}
- (void)openRight{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)changeLoginType:(UIButton *)sender {
    
    _loginType = sender.tag - 10;
    if (_loginType == 0) {
        [_loginType1 setTitle:kLocalizedString(@"EMAIL", @"电子邮箱") forState:UIControlStateNormal];
        [_loginType2 setTitle:kLocalizedString(@"Phone", @"手机号") forState:UIControlStateNormal];
        _emailText.placeholder = kLocalizedString(@"EMAIL_PLZ", @"请填写邮箱地址");
        _codeHight.constant = 0;
        _selectWidth.constant = 0;
    }else{
        [_loginType1 setTitle:kLocalizedString(@"Phone", @"手机号") forState:UIControlStateNormal];
        [_loginType2 setTitle:kLocalizedString(@"EMAIL", @"电子邮箱") forState:UIControlStateNormal];
        _emailText.placeholder = kLocalizedString(@"Eenter_Phone_Number", @"请输入手机号");
        _codeHight.constant = 87;
        _selectWidth.constant = 99;
    }
}
- (IBAction)signUp:(id)sender {
    
    GlenWeakSelf;
    if (_loginType == 0) {
        if (!_agreeBtn.selected) {
            [[Toast makeUpText:@"请同意协议"] showWithType:ShortTime];
            return;
        }
        if (![_emailText.text isEmail]) {
            [[Toast makeUpText:kLocalizedString(@"EMAIL_INVALID", @"请正确输入电子邮箱")] showWithType:ShortTime];
            return;
        }
        if (_passWordText.text.length < 6) {
            [[Toast makeUpText:kLocalizedString(@"PASSWORD_INVALID", @"密码应由至少6位的字符组成")] showWithType:ShortTime];
            return;
        }
        if (![_passWordText.text isEqualToString:_enPassWordText.text]) {
            [[Toast makeUpText:kLocalizedString(@"CONFIRM_PASSWORD_INVALID", @"请确认两次输入的密码内容一致")] showWithType:ShortTime];
            return;
        }
        
//        [self showHUBWith:@""];
        [[GlenHttpTool shareInstance] signUpWithEmail:_emailText.text password:_passWordText.text Completion:^(NSString *succes) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:succes] showWithType:ShortTime];
            GlenEmailActivationViewController *signup = [GlenEmailActivationViewController new];
            signup.email = weakSelf.emailText.text;
            [weakSelf.navigationController pushViewController:signup animated:YES];
        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:error] showWithType:ShortTime];
        }];
    }else{
        if (!_agreeBtn.selected) {
            [[Toast makeUpText:@"请同意协议"] showWithType:ShortTime];
            return;
        }
        if (_emailText.text.length == 0) {
            [[Toast makeUpText:kLocalizedString(@"Valid_Phone_Number", @"请输入正确的手机号")] showWithType:ShortTime];
            return;
        }
        if (_passWordText.text.length < 6) {
            [[Toast makeUpText:kLocalizedString(@"PASSWORD_INVALID", @"密码应由至少6位的字符组成")] showWithType:ShortTime];
            return;
        }
        if (_codeText.text.length == 0) {
            [[Toast makeUpText:kLocalizedString(@"Enter_verification_Code", @"请输入验证码")] showWithType:ShortTime];
            return;
        }
        if (![_passWordText.text isEqualToString:_enPassWordText.text]) {
            [[Toast makeUpText:kLocalizedString(@"CONFIRM_PASSWORD_INVALID", @"请确认两次输入的密码内容一致")] showWithType:ShortTime];
            return;
        }
        
        [[GlenHttpTool shareInstance] signUpWithPhone:_emailText.text password:_passWordText.text countryCode:_codeModel.countryCode.rawValue code:_codeText.text Completion:^(NSString *succes) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:succes] showWithType:ShortTime];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:error] showWithType:ShortTime];
        }];
        
        
    }
    

}

- (IBAction)send:(UIButton *)sender {
    if (_emailText.text.length == 0) {
        [[Toast makeUpText:kLocalizedString(@"Valid_Phone_Number", @"请输入正确的手机号")] showWithType:ShortTime];
        return;
    }
    GlenWeakSelf;
    [self showHUBWith:@""];
    [[GlenHttpTool shareInstance] sendSmsWithCountryCode:_codeModel.countryCode.rawValue phone:_emailText.text Completion:^(NSString *succes) {
        [weakSelf hiddenHUB];
        [[Toast makeUpText:succes] showWithType:ShortTime];
        [BYTimer timerWithTimeout:60 handlerBlock:^(int presentTime) {
            [weakSelf.sendBtn setTitle:[NSString stringWithFormat:@"%ds",presentTime] forState:UIControlStateNormal];
        } finish:^{
            [weakSelf.sendBtn setTitle:[NSString stringWithFormat:@"%@",kLocalizedString(@"Tips", @"获取验证码")] forState:UIControlStateNormal];
        }];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
        [[Toast makeUpText:error] showWithType:ShortTime];
    }];
}

- (IBAction)agree:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
