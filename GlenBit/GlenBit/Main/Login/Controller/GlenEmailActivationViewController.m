//
//  GlenEmailActivationViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/5.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenEmailActivationViewController.h"

@interface GlenEmailActivationViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet UILabel *detail;

@end

@implementation GlenEmailActivationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.top.constant = StatusBar_height + 60;
    _detail.text = [NSString stringWithFormat:@"%@ %@",kLocalizedString(@"ACTIVATION_EMAIL_SENT", @"我们已向以下邮箱发送了包含激活链接的邮件。 请注意查收 ="),_email];
}
- (void)setEmail:(NSString *)email{
    _email = email;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
