//
//  GlenNewPassWordViewController.h
//  GlenBit
//
//  Created by Lee on 2019/1/24.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBaseViewController.h"

@interface GlenNewPassWordViewController : GlenBaseViewController

@property (strong, nonatomic)  NSString *email;
@property (assign, nonatomic)  NSInteger type;
@property (strong, nonatomic)  NSString *contryCount;

@end
