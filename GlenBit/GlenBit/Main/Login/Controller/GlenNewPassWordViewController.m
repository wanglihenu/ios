//
//  GlenNewPassWordViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/24.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenNewPassWordViewController.h"
#import "GlenProgressView.h"
#import "GlenResetPassViewController.h"

@interface GlenNewPassWordViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet UIView *passWordView;
@property (weak, nonatomic) IBOutlet UITextField *passWordText;
@property (weak, nonatomic) IBOutlet UIView *enpassWordView;
@property (weak, nonatomic) IBOutlet UITextField *enpassWordText;

@property (weak, nonatomic) IBOutlet GlenProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *detail;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@property (weak, nonatomic) IBOutlet UILabel *passWordStr;
@property (weak, nonatomic) IBOutlet UILabel *enPassWordStr;
@property (weak, nonatomic) IBOutlet UIButton *getCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation GlenNewPassWordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.top.constant = StatusBar_height + 60;
    self.enpassWordView.layer.borderColor = self.passWordView.layer.borderColor = [UIColor colorWithHexString:@"#353B41"].CGColor;
    self.enpassWordView.layer.borderWidth = self.passWordView.layer.borderWidth = 1;
    [_backBtn setTitle:kLocalizedString(@"<LOGIN", @"登录") forState:UIControlStateNormal];
    _titleLab.text = kLocalizedString(@"RESETLOGIN", @"验证");
    _detail.text = [NSString stringWithFormat:@"%@ %@",kLocalizedString(@"SENDCODETO", @"发送验证码到"),_email];
    [_progressView showProgressViewWithTitleArr:@[kLocalizedString(@"VERIFY", @"验证"),kLocalizedString(@"ENTERNEWPW", @"重置密码")] curentIndex:1];
    _passWordStr.text = kLocalizedString(@"PASSWORD", @"密码");
    _enPassWordStr.text = kLocalizedString(@"COMFIRM_PASSWORD", @"确认密码");
    [_getCodeBtn setTitle:kLocalizedString(@"Tips", @"获取验证码") forState:UIControlStateNormal];
    _passWordText.placeholder = kLocalizedString(@"Please_fill_password", @"请输入密码");
    _enpassWordText.placeholder = kLocalizedString(@"CONFIRM_PASSWORD_PLZ", @"请再次输入相同的密码");
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)getCode:(id)sender {
    
    if (_passWordText.text.length < 6) {
        [[Toast makeUpText:kLocalizedString(@"PASSWORD_INVALID", @"密码应由至少6位的字符组成")] showWithType:ShortTime];
        return;
    }
    if (![_passWordText.text isEqualToString:_enpassWordText.text]) {
        [[Toast makeUpText:kLocalizedString(@"CONFIRM_PASSWORD_INVALID", @"请确认两次输入的密码内容一致")] showWithType:ShortTime];
        return;
    }
    GlenWeakSelf;
    [self showHUBWith:@""];
    if (_type == 0) {
        
        
        [[GlenHttpTool shareInstance] tryRestPassWordWithEmai:_email Completion:^(NSString *succes) {
            [weakSelf hiddenHUB];
            GlenResetPassViewController *signup = [GlenResetPassViewController new];
            signup.email = weakSelf.email;
            signup.type = weakSelf.type;
            signup.passWord = weakSelf.passWordText.text;
            [weakSelf.navigationController pushViewController:signup animated:YES];
        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:error] showWithType:ShortTime];
        }];
    }else{
        [[GlenHttpTool shareInstance] tryRestPassWordWithPhone:_email CountryCode:_contryCount Completion:^(NSString *succes) {
            [weakSelf hiddenHUB];
            GlenResetPassViewController *signup = [GlenResetPassViewController new];
            signup.email = weakSelf.email;
            signup.type = weakSelf.type;
            signup.contryCount = weakSelf.contryCount;
            signup.passWord = weakSelf.passWordText.text;
            [weakSelf.navigationController pushViewController:signup animated:YES];
        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:error] showWithType:ShortTime];
        }];
    }
    
    
}

- (void)setEmail:(NSString *)email{
    _email = email;
}
- (void)setType:(NSInteger)type{
    _type = type;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
