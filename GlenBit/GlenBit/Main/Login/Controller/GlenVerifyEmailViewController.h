//
//  GlenVerifyEmailViewController.h
//  GlenBit
//
//  Created by Lee on 2019/1/5.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBaseViewController.h"

@interface GlenVerifyEmailViewController : GlenBaseViewController

@property (strong, nonatomic) NSArray *countryData;

@end
