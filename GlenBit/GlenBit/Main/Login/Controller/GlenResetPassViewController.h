//
//  GlenResetPassViewController.h
//  GlenBit
//
//  Created by Lee on 2019/1/5.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBaseViewController.h"

@interface GlenResetPassViewController : GlenBaseViewController

@property (strong, nonatomic)  NSString *email;
@property (assign, nonatomic)  NSInteger type;
@property (strong, nonatomic)  NSString *passWord;
@property (strong, nonatomic)  NSString *contryCount;

@end
