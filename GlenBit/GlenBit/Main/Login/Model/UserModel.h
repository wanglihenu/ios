//
//  EUserModel.h
//  JBT
//
//  Created by Lee on 2018/1/21.
//  Copyright © 2018年 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    TeacherShingqing = 0,
    TeacherIS,
    TeacherNO,
    TeacherQT,
} TeacherStatus;


@interface recentLogins :NSObject
@property (nonatomic , copy) NSString              * createdAt;
@property (nonatomic , copy) NSString              * ip;

@end

@interface UserModel : NSObject
@property (nonatomic , copy) NSString              * email;
@property (nonatomic , copy) NSString              * phone;
@property (nonatomic , copy) NSString              * currentName;
@property (nonatomic , assign) BOOL               isSmsVerified;

@property (nonatomic , copy) NSString              * createdAt;
@property (nonatomic , strong) NSArray              * recentLogins;
@property (nonatomic , strong) NSArray              * sortRecenLogin;
@property (nonatomic , assign) BOOL               isTwoStepEnabled;


@end

@interface UerManger : NSObject

@property (nonatomic, assign) BOOL isLogin;
@property (nonatomic, assign) NSInteger loginType;
@property (nonatomic, strong) SortMarkets *markes;
@property (nonatomic, strong) NSArray *marke_price;
@property (nonatomic, strong) NSArray *coins;
@property (nonatomic, strong) NSArray *tikes;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *countyCode;
@property (nonatomic, strong) UserModel *userModel;


+ (void)cleanLogin;
+ (UerManger *)shareUserManger;

@end

