//
//  EUserModel.m
//  JBT
//
//  Created by Lee on 2018/1/21.
//  Copyright © 2018年 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "UserModel.h"

@implementation recentLogins


@end

@implementation UserModel

- (NSString *)currentName{
    if (!_currentName) {
        _currentName = [UerManger shareUserManger].loginType == 0?[UerManger shareUserManger].userModel.email:[UerManger shareUserManger].userModel.phone;
    }
    return _currentName;
}
- (NSArray *)sortRecenLogin{
    if (!_sortRecenLogin) {
        NSSortDescriptor *createdAt = [NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:NO];
        _sortRecenLogin = [[_recentLogins sortedArrayUsingDescriptors:@[createdAt]] mutableCopy];
    }
    return _sortRecenLogin;
}
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"recentLogins" : [recentLogins class]
             };
}
@end

@implementation UerManger

+ (UerManger *)shareUserManger
{
    static UerManger *userManger = nil;
    static dispatch_once_t onceToKen;
    dispatch_once(&onceToKen, ^{
        userManger = [[UerManger alloc]init];
        
    });
    return userManger;
}

- (BOOL)isLogin{
    
    NSString *cookie = [[NSUserDefaults standardUserDefaults] valueForKey:@"cookie"];
    if (cookie.length > 0) {
        return YES;
    }else{
        return NO;
    }
    
//    if (self.userModel){
//        return YES;
//    }
    return NO;
}
- (NSString *)phone{
    _phone = [[NSUserDefaults standardUserDefaults] valueForKey:@"phone"];
    return _phone;
}
- (NSString *)countyCode{
    _countyCode = [[NSUserDefaults standardUserDefaults] valueForKey:@"countyCode"];
    return _countyCode;
}
- (NSInteger)loginType{
    NSString *loginType = [[NSUserDefaults standardUserDefaults] valueForKey:@"loginType"];
    return loginType.integerValue;
}
+ (void)cleanLogin{
    NSUserDefaults *de = [NSUserDefaults  standardUserDefaults];
    [de setObject:@"" forKey:@"cookie"];
    [de synchronize];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"contentSizeDidChangeNotification" object:nil];
}
@end
