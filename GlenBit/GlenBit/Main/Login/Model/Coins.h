//
//  Coins.h
//  GlenBit
//
//  Created by Lee on 2019/1/21.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BalanceModel.h"

@interface Coins : NSObject

@property (nonatomic , copy) NSString              * _id;
@property (nonatomic , copy) NSString              * minWithdraw;
@property (nonatomic , copy) NSString              * withdrawPrecision;
@property (nonatomic , copy) NSString              * sortOrder;
@property (nonatomic , assign) BOOL                  canWithdraw;
@property (nonatomic , copy) NSString              * fee;
@property (nonatomic , assign) BOOL                  canDeposit;
@property (nonatomic , copy) NSString              * name;

@property (strong , nonatomic) BalanceModel        * balance;

@end

