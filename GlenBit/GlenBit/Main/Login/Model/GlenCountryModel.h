//
//  GlenCountryModel.h
//  GlenBit
//
//  Created by Lee on 2019/1/24.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface countryCode :NSObject
@property (nonatomic , copy) NSString              * rawValue;
@property (nonatomic , copy) NSString              * displayValue;

@end

@interface GlenCountryModel :NSObject
@property (nonatomic , copy) NSString              * language;
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , strong) countryCode              * countryCode;
@property (nonatomic , copy) NSString              * sortOrder;
@property (nonatomic , copy) NSString              * iconUrl;

@end
