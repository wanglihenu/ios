//
//  GlenGoogleThirdViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/25.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenGoogleThirdViewController.h"
#import "GlenProgressView.h"

@interface GlenGoogleThirdViewController ()
@property (weak, nonatomic) IBOutlet GlenProgressView *progressView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *staus;
@property (weak, nonatomic) IBOutlet UIButton *sumBtnStr;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation GlenGoogleThirdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_progressView showProgressViewWithTitleArr:@[kLocalizedString(@"VERIFY", @"验证"),kLocalizedString(@"ScanQRcode", @"扫描二维码"),kLocalizedString(@"SUCCESS", @"成功")] curentIndex:2];
    _titleLab.text = kLocalizedString(@"GOOGLE_TWO_STEP", @"谷歌二次认证");
    _staus.text = kLocalizedString(@"SUCCESS", @"成功");
    [_sumBtnStr setTitle:kLocalizedString(@"Return", @"返回") forState:UIControlStateNormal];
    [_backBtn setTitle:kLocalizedString(@"<Account", @"Account") forState:UIControlStateNormal];
    self.top.constant = StatusBar_height + 60;
}
- (IBAction)back:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
