//
//  GlenGoogleViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/8.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenGoogleViewController.h"
#import "GlenProgressView.h"
#import "HMScanner.h"
#import "GlenGoogleThirdViewController.h"

@interface GlenGoogleViewController ()

@property (weak, nonatomic) IBOutlet GlenProgressView *progressView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet UIView *privetView;
@property (weak, nonatomic) IBOutlet UITextField *privetText;
@property (weak, nonatomic) IBOutlet UIView *codeView;
@property (weak, nonatomic) IBOutlet UITextField *codeText;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@property (weak, nonatomic) IBOutlet UILabel *siyaoStr;

@property (weak, nonatomic) IBOutlet UIButton *sumBtnStr;
@property (weak, nonatomic) IBOutlet UILabel *codeStr;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;


@end

@implementation GlenGoogleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _titleLab.text = kLocalizedString(@"GOOGLE_TWO_STEP", @"谷歌二次认证");
    _siyaoStr.text = kLocalizedString(@"privatekey", @"私钥");
    [_progressView showProgressViewWithTitleArr:@[kLocalizedString(@"VERIFY", @"验证"),kLocalizedString(@"ScanQRcode", @"扫描二维码"),kLocalizedString(@"SUCCESS", @"成功")] curentIndex:2];
    self.top.constant = StatusBar_height + 60;
    self.privetView.layer.borderColor = self.codeView.layer.borderColor = [UIColor colorWithHexString:@"#353B41"].CGColor;
    self.privetView.layer.borderWidth = self.codeView.layer.borderWidth = 1;
    _codeStr.text = _codeText.placeholder = kLocalizedString(@"ENTERGOOGLECODE", @"谷歌二次验证码");
    [_sumBtnStr setTitle:kLocalizedString(@"SUBMIT", @"提交") forState:UIControlStateNormal];
    [_backBtn setTitle:kLocalizedString(@"<Account", @"Account") forState:UIControlStateNormal];
    [self getURl];
}
- (void)setCode:(NSString *)code{
    _code = code;
}
- (void)getURl{
    [self showHUBWith:@""];
    GlenWeakSelf;
    
    [[GlenHttpTool shareInstance] twpStepVerifyCode:_code Completion:^(GoogleModel *model) {
        [weakSelf hiddenHUB];
        weakSelf.privetText.text = model.key;
        [HMScanner qrImageWithString:model.url avatar:nil completion:^(UIImage *image) {
            weakSelf.imageView.image = image;
        }];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
        [[Toast makeUpText:error] showWithType:ShortTime];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)submit:(id)sender {
    if (_codeText.text.length == 0) {
        [[Toast makeUpText:kLocalizedString(@"Enter_verification_Code", @"请输入验证码")] showWithType:ShortTime];
        return;
    }
    
    [self showHUBWith:@""];
    GlenWeakSelf;
    
    
    [[GlenHttpTool shareInstance] enableTwpStepVerifyToken:_codeText.text Completion:^(NSString *succes) {
        [weakSelf hiddenHUB];
        GlenGoogleThirdViewController *google = [GlenGoogleThirdViewController new];
        [weakSelf.navigationController pushViewController:google animated:YES];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
        [[Toast makeUpText:error] showWithType:ShortTime];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
