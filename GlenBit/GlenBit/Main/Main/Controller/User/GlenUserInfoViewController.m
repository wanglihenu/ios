//
//  GlenUserInfoViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenUserInfoViewController.h"
#import "GlenUserSecurityCollectionViewCell.h"
#import "GlenUserLastloginCollectionViewCell.h"
#import "GlenUserAPISettingsCollectionViewCell.h"
#import "GlenUserHeadCollectionReusableView.h"
#import "GlenUserSectionCollectionReusableView.h"
#import "GlenUserInfoCollectionViewCell.h"

#import "GlenUserSecretKeyView.h"
#import "GlenUserSecretDeletView.h"
#import "GlenUserCreateApiViewController.h"

#import "GlenResetEmailViewController.h"
#import "GlenGoogleViewController.h"
#import "GlenIDVerificationViewController.h"
#import "GlenGoogleOneViewController.h"
#import "GKCover.h"
#import "GlenGoogleTwoStepView.h"

#import "GlenNewMobileViewController.h"

#import "GlenAletView.h"
#import "GKCover.h"
#import "GlenContryCountViewController.h"
#import "NODATACollectionViewCell.h"

@interface GlenUserInfoViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) NSIndexPath *selectindex;

@end

@implementation GlenUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithHexString:@"#E8E8E8"];
    [self.view addSubview:self.collectionView];
}
- (void)upDataLoca{
    self.title = kLocalizedString(@"ACCOUNT", @"账号");
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    GlenWeakSelf;
    [[GlenHttpTool shareInstance] profileCompletion:^(UserModel *user) {
        [weakSelf updata];
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.collectionView reloadData];
        });
    } fail:^(NSString *error) {

    }];
    


}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    GlenWeakSelf;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [weakSelf.collectionView reloadData];
//    });
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }else if (section == 1){
        return 3;
    }else if (section == 2){
        return 2;
    }else if (section == 3){
        return 2;
    }else if (section == 4){
        NSInteger cou = [UerManger shareUserManger].userModel.sortRecenLogin.count;
        return cou;
    }else if (section == 5){
        return 1;
    }
    return 0;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 7;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeZero;
//        return CGSizeMake(size_width, 54);
    }else if (indexPath.section == 1){
        return CGSizeMake(size_width, 60);
    }else if (indexPath.section == 2){
        return CGSizeMake(size_width, 60);
    }else if (indexPath.section == 3){
        return CGSizeMake(size_width, 60);
    }else if (indexPath.section == 4){
        return CGSizeMake(size_width, 66);
    }else if (indexPath.section == 5){
        if ([indexPath compare:_selectindex] == NSOrderedSame) {
            return CGSizeMake(size_width, 152);
        }
        return CGSizeMake(size_width, 88);
    }
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGSizeMake(size_width, 104);
    }else if (section == 3){
        return CGSizeMake(size_width, 62);
    }else if (section == 4){
        return CGSizeMake(size_width, 62);
    }else if (section == 5){
//        return CGSizeZero;
        return CGSizeMake(size_width, 62);
    }
    return CGSizeZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(1, 0, 20, 0);
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    GlenWeakSelf;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        
        if (indexPath.section == 0) {
            GlenUserHeadCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenUserHeadCollectionReusableView" forIndexPath:indexPath];
            [header updataInfo];
            return header;
        }else if (indexPath.section == 3){
            GlenUserSectionCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenUserSectionCollectionReusableView" forIndexPath:indexPath];
            header.index = indexPath;
            return header;
        }else if (indexPath.section == 4){
            GlenUserSectionCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenUserSectionCollectionReusableView" forIndexPath:indexPath];
            header.index = indexPath;
            return header;
        }else if (indexPath.section == 5){
            GlenUserSectionCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenUserSectionCollectionReusableView" forIndexPath:indexPath];
            header.index = indexPath;
            header.creatApi = ^{
                
            };
            return header;
        }
        
    }
    return nil;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GlenWeakSelf;
    if (indexPath.section == 0) {
        GlenUserInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenUserInfoCollectionViewCell" forIndexPath:indexPath];
        cell.index = indexPath;
        return cell;
    }else if (indexPath.section == 1){
        GlenUserInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenUserInfoCollectionViewCell" forIndexPath:indexPath];
        cell.index = indexPath;
        return cell;
    }else if (indexPath.section == 2){
        GlenUserInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenUserInfoCollectionViewCell" forIndexPath:indexPath];
        cell.index = indexPath;
        return cell;
    }else if (indexPath.section == 3){
        GlenUserSecurityCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenUserSecurityCollectionViewCell" forIndexPath:indexPath];
        cell.index = indexPath;
        return cell;
    }else if (indexPath.section == 4){
        GlenUserLastloginCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenUserLastloginCollectionViewCell" forIndexPath:indexPath];
        cell.model = [UerManger shareUserManger].userModel.sortRecenLogin[indexPath.row];
        return cell;
    }else if (indexPath.section == 5){
        
        NODATACollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NODATACollectionViewCell" forIndexPath:indexPath];
        return cell;
//        GlenUserAPISettingsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenUserAPISettingsCollectionViewCell" forIndexPath:indexPath];
//        cell.openAsset = ^(NSInteger index) {
//            if (index == 0) {
//                GlenUserSecretKeyView *withDraw = [[GlenUserSecretKeyView alloc]initWithFrame:CGRectMake(0, 0, size_width, size_height)];
//                withDraw.closeSelf = ^{
//                    [weakSelf dismissSemiModalView];
//                };
//                [weakSelf presentSemiView:withDraw withOptions:@{KNSemiModalOptionKeys.pushParentBack : @(NO),KNSemiModalOptionKeys.parentAlpha : @(0.8)}];
//            }else if (index == 2){
//                GlenUserSecretDeletView *withDraw = [[GlenUserSecretDeletView alloc]initWithFrame:CGRectMake(0, 0, size_width, size_height)];
//                withDraw.closeSelf = ^(NSInteger index) {
//                    [weakSelf dismissSemiModalView];
//                };
//                [weakSelf presentSemiView:withDraw withOptions:@{KNSemiModalOptionKeys.pushParentBack : @(NO),KNSemiModalOptionKeys.parentAlpha : @(0.8)}];
//            }else if (index == 1){
//                GlenUserCreateApiViewController *creat = [GlenUserCreateApiViewController new];
//                [weakSelf.navigationController pushViewController:creat animated:YES];
//            }
//        };
//        return cell;
    }
    
    
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    GlenWeakSelf;
    switch (indexPath.section) {
//        case 6:
//        {
//            _selectindex = indexPath;
//            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]];
//        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:{
//                    GlenResetEmailViewController *reset = [GlenResetEmailViewController new];
//                    reset.type = 1;
//                    [self.navigationController pushViewController:reset animated:YES];
                }
                    
                    break;
                case 1:{
                    GlenResetEmailViewController *reset = [GlenResetEmailViewController new];
                    reset.type = 2;
                    [self.navigationController pushViewController:reset animated:YES];
                }
                    
                    break;
                case 2:{
                    [self logout];
                }
                    
                    break;
                default:
                    break;
            }
        }
            break;
        case 3:
        {
            switch (indexPath.row) {
                case 0:{
                    if ([UerManger shareUserManger].userModel.isSmsVerified) {
                        
                    }else{
                        GlenNewMobileViewController *reset = [GlenNewMobileViewController new];
                        
                        [self.navigationController pushViewController:reset animated:YES];
                    }
                    
                }
                    
                    break;
                case 1:{
                    if ([UerManger shareUserManger].userModel.isTwoStepEnabled) {
                        [self UnanbleGoogle];
                    }else{
                        GlenGoogleOneViewController *reset = [GlenGoogleOneViewController new];
                        [self.navigationController pushViewController:reset animated:YES];
                    }
                    
                }
                    
                    break;
                    
                    
                default:
                    break;
            }
        }
            break;
        case 2:
        {
            switch (indexPath.row) {
                case 1:{
                    GlenWeakSelf;
                        GlenContryCountViewController *reset = [GlenContryCountViewController new];
                    reset.countryData = @[kLocalizedString(@"chinese", @""),kLocalizedString(@"English", @"")];
                    reset.type = 2;
                    reset.valueDidSelect = ^(NSInteger row) {
                        [[HXLanguageManager shareInstance] setUserlanguage:row == 0?CHINESE:ENGLISH];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"laugu" object:nil];
                    };
                    [self.navigationController pushViewController:reset animated:YES];
                    
                    
                }
                    
                    break;

                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
}
- (void)UnanbleGoogle{
    
    GlenWeakSelf;
    GlenGoogleTwoStepView *google = [[GlenGoogleTwoStepView alloc]initWithFrame:CGRectMake(0, 0, size_width - 20, 240)];
    google.actionType = ^(NSInteger index, NSString *code) {
        if (index == 0) {
            [GKCover hide];
        }else if (index == 1){
            [GKCover hide];
            [[GlenHttpTool shareInstance] disableTwpStepVerifyToken:code Completion:^(NSString *succes) {
                [weakSelf hiddenHUB];
                [[Toast makeUpText:succes] showWithType:ShortTime];
                [UerManger shareUserManger].userModel.isTwoStepEnabled = NO;
                [weakSelf.collectionView reloadData];
            } fail:^(NSString *error) {
                [weakSelf hiddenHUB];
                [[Toast makeUpText:error] showWithType:ShortTime];
            }];
        }
    };
    [GKCover coverFrom:[UIApplication sharedApplication].keyWindow contentView:google style:GKCoverStyleTranslucent showStyle:GKCoverShowStyleCenter showAnimStyle:GKCoverShowAnimStyleCenter hideAnimStyle:GKCoverHideAnimStyleCenter notClick:NO showBlock:^{
        
    } hideBlock:^{
        
    }];
}
- (void)logout{
    
    GlenWeakSelf;
    GlenAletView *alet = [[GlenAletView alloc]initWithFrame:CGRectMake(0, 0, 250 , 140)];
    [alet showTitle:kLocalizedString(@"LOGOUTTIPS", @"") confirm:^{
        [GKCover hide];
        [weakSelf showHUBWith:@""];
        [[GlenHttpTool shareInstance] logoutCompletion:^(NSString *succes) {
            [weakSelf hiddenHUB];
            [UerManger cleanLogin];
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
            weakSelf.cyl_tabBarController.selectedIndex = 0;
        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:error] showWithType:ShortTime];
        }];
    } cancel:^{
        [GKCover hide];
    }];
    [GKCover coverFrom:[UIApplication sharedApplication].keyWindow contentView:alet style:GKCoverStyleTranslucent showStyle:GKCoverShowStyleCenter showAnimStyle:GKCoverShowAnimStyleCenter hideAnimStyle:GKCoverHideAnimStyleCenter notClick:NO showBlock:^{
        
    } hideBlock:^{
        
    }];
    
}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        // 布局对象
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.minimumLineSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, StatusBar_height + 60, size_width, size_height - 60 - StatusBar_height) collectionViewLayout:flowLayout];
        _collectionView.delegate=self;
        _collectionView.dataSource=self;
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenUserSecurityCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenUserSecurityCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenUserLastloginCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenUserLastloginCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenUserInfoCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenUserInfoCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"NODATACollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"NODATACollectionViewCell"];
        
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenUserAPISettingsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenUserAPISettingsCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenUserHeadCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenUserHeadCollectionReusableView"];
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenUserSectionCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenUserSectionCollectionReusableView"];
    }
    return _collectionView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
