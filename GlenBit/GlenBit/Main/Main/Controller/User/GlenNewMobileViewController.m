//
//  GlenNewMobileViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/8.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenNewMobileViewController.h"
#import "GlenProgressView.h"
#import "GlenResetEmailSubmitCodeViewController.h"
#import "GlenContryCountViewController.h"

@interface GlenNewMobileViewController ()

@property (weak, nonatomic) IBOutlet GlenProgressView *progressView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet UIView *emaiView;
@property (weak, nonatomic) IBOutlet UITextField *emailText;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectWidth;

@property (strong, nonatomic)  NSMutableArray *country;
@property (strong, nonatomic)  GlenCountryModel *codeModel;

@property (weak, nonatomic) IBOutlet UILabel *quhao;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation GlenNewMobileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_progressView showProgressViewWithTitleArr:@[@"Enter mobile",kLocalizedString(@"VERIFY", @"验证")] curentIndex:0];
    [_backBtn setTitle:kLocalizedString(@"<Account", @"Account") forState:UIControlStateNormal];
    self.top.constant = StatusBar_height + 60;
    self.emaiView.layer.borderColor = [UIColor colorWithHexString:@"#353B41"].CGColor;
    self.emaiView.layer.borderWidth = 1;
    _selectWidth.constant = 99;
    [self getCountryData];
}
- (void)getCountryData{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    for (int i = 0; i < json.count; i++) {
        // Manually
        GlenCountryModel *model = [GlenCountryModel yy_modelWithDictionary:json[i]];
        [self.country addObject:model];
    }
    _codeModel = self.country[0];
    self.quhao.text = [NSString stringWithFormat:@"+%@",_codeModel.countryCode.rawValue];
}
- (NSMutableArray *)country{
    if (!_country) {
        _country = [NSMutableArray new];
    }
    return _country;
}
- (IBAction)tap:(id)sender {
    GlenWeakSelf;
    GlenContryCountViewController *conty = [GlenContryCountViewController new];
    conty.countryData = _country;
    conty.valueDidSelect = ^(NSInteger row) {
        weakSelf.codeModel = self.country[row];
        weakSelf.quhao.text = [NSString stringWithFormat:@"+%@",weakSelf.codeModel.countryCode.rawValue];
    };
    [self.navigationController pushViewController:conty animated:YES];
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)submit:(id)sender {
    
    if (_emailText.text.length == 0) {
        [[Toast makeUpText:@"请输入手机号"] showWithType:ShortTime];
        return;
    }
    GlenWeakSelf;
    [self showHUBWith:@""];
    [[GlenHttpTool shareInstance] sendSmsWithCountryCode:_codeModel.countryCode.rawValue phone:_emailText.text Completion:^(NSString *succes) {
        [weakSelf hiddenHUB];
        [[Toast makeUpText:succes] showWithType:ShortTime];
        GlenResetEmailSubmitCodeViewController *getCode = [GlenResetEmailSubmitCodeViewController new];
        getCode.type = 4;
        getCode.phone = weakSelf.emailText.text;
        getCode.contryCount = weakSelf.codeModel.countryCode.rawValue;
        [weakSelf.navigationController pushViewController:getCode animated:YES];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
        [[Toast makeUpText:error] showWithType:ShortTime];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
