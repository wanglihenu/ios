//
//  GlenGoogleViewController.h
//  GlenBit
//
//  Created by Lee on 2019/1/8.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBaseViewController.h"

@interface GlenGoogleViewController : GlenBaseViewController

@property (strong, nonatomic) NSString *code;

@end
