//
//  GlenResetPassWordViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/8.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenResetPassWordViewController.h"
#import "GlenProgressView.h"


@interface GlenResetPassWordViewController ()

@property (weak, nonatomic) IBOutlet GlenProgressView *progressView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet UIView *passWordView;
@property (weak, nonatomic) IBOutlet UITextField *passWordText;
@property (weak, nonatomic) IBOutlet UIView *enpassWordView;
@property (weak, nonatomic) IBOutlet UITextField *enpassWordText;

@property (weak, nonatomic) IBOutlet UILabel *passWordStr;
@property (weak, nonatomic) IBOutlet UILabel *enPassWordStr;

@property (weak, nonatomic) IBOutlet UIButton *sumBtnStr;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;


@end

@implementation GlenResetPassWordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _passWordStr.text = kLocalizedString(@"PASSWORD", @"密码");
    _enPassWordStr.text = kLocalizedString(@"COMFIRM_PASSWORD", @"确认密码");
    
    [_progressView showProgressViewWithTitleArr:@[kLocalizedString(@"VERIFY", @"验证"),kLocalizedString(@"ENTERNEWPW", @"重置密码")] curentIndex:0];
    self.top.constant = StatusBar_height + 60;
    self.passWordView.layer.borderColor = self.enpassWordView.layer.borderColor = [UIColor colorWithHexString:@"#353B41"].CGColor;
    self.passWordView.layer.borderWidth = self.enpassWordView.layer.borderWidth = 1;
    _passWordText.placeholder = kLocalizedString(@"Please_fill_password", @"请输入密码");
    _enpassWordText.placeholder = kLocalizedString(@"CONFIRM_PASSWORD_PLZ", @"请再次输入相同的密码");
    [_sumBtnStr setTitle:kLocalizedString(@"SUBMIT", @"提交") forState:UIControlStateNormal];
    [_backBtn setTitle:kLocalizedString(@"<Account", @"Account") forState:UIControlStateNormal];
}
- (void)setCode:(NSString *)code{
    _code = code;
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)submit:(id)sender {
    if (_passWordText.text.length < 6) {
        [[Toast makeUpText:kLocalizedString(@"PASSWORD_INVALID", @"密码应由至少6位的字符组成")] showWithType:ShortTime];
        return;
    }
    if (![_passWordText.text isEqualToString:_enpassWordText.text]) {
        [[Toast makeUpText:kLocalizedString(@"CONFIRM_PASSWORD_INVALID", @"请确认两次输入的密码内容一致")] showWithType:ShortTime];
        return;
    }
    [self showHUBWith:@""];
    GlenWeakSelf;
    if ([UerManger shareUserManger].loginType == 0) {
        
        
        [[GlenHttpTool shareInstance] modif_passWordWithEmailPassWord:_passWordText.text code:_code Completion:^(NSString *succes) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:succes] showWithType:ShortTime];
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:error] showWithType:ShortTime];
        }];
    }else{
        
        [[GlenHttpTool shareInstance] modif_passWordWithPhonePassWord:_passWordText.text code:[UerManger shareUserManger].countyCode Completion:^(NSString *succes) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:succes] showWithType:ShortTime];
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        } fail:^(NSString *error) {
            [[Toast makeUpText:error] showWithType:ShortTime];
        }];
        
    }
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
