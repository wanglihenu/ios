//
//  GlenGoogleOneViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/25.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenGoogleOneViewController.h"
#import "GlenProgressView.h"
#import "GlenGoogleViewController.h"

@interface GlenGoogleOneViewController ()
@property (weak, nonatomic) IBOutlet GlenProgressView *progressView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet UIView *codeView;
@property (weak, nonatomic) IBOutlet UITextField *codeText;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet UILabel *detail;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIButton *sumBtnStr;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;


@end

@implementation GlenGoogleOneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _titleLab.text = kLocalizedString(@"GOOGLE_TWO_STEP", @"谷歌二次认证");
    [_progressView showProgressViewWithTitleArr:@[kLocalizedString(@"VERIFY", @"验证"),kLocalizedString(@"ScanQRcode", @"扫描二维码"),kLocalizedString(@"SUCCESS", @"成功")] curentIndex:0];
    self.top.constant = StatusBar_height + 60;
    self.codeView.layer.borderColor = [UIColor colorWithHexString:@"#353B41"].CGColor;
    self.codeView.layer.borderWidth = 1;
    _detail.text = [NSString stringWithFormat:@"%@ %@",kLocalizedString(@"SENDCODETO", @"发送验证码到"),[UerManger shareUserManger].userModel.currentName];
    [_sendBtn setTitle:kLocalizedString(@"Tips", @"获取验证码") forState:UIControlStateNormal];
    _codeText.placeholder = kLocalizedString(@"Enter_verification_Code", @"请输入验证码");
    [_sumBtnStr setTitle:kLocalizedString(@"SUBMIT", @"提交") forState:UIControlStateNormal];
    [_backBtn setTitle:kLocalizedString(@"<Account", @"Account") forState:UIControlStateNormal];
    [self send:nil];
}
- (IBAction)send:(id)sender {
    [self showHUBWith:@""];
    GlenWeakSelf;
    [[GlenHttpTool shareInstance] resetTwoStepCompletion:^(NSString *succes) {
        [weakSelf hiddenHUB];
        [[Toast makeUpText:succes] showWithType:ShortTime];
        

        
        [BYTimer timerWithTimeout:60 handlerBlock:^(int presentTime) {
            weakSelf.sendBtn.backgroundColor = [UIColor colorWithHexString:@"#D0D0D0"];
            [weakSelf.sendBtn setTitle:[NSString stringWithFormat:@"%ds",presentTime] forState:UIControlStateNormal];
            weakSelf.sendBtn.userInteractionEnabled = NO;
        } finish:^{
            weakSelf.sendBtn.backgroundColor = [UIColor colorWithHexString:@"#00ACDC"];
            [weakSelf.sendBtn setTitle:[NSString stringWithFormat:@"%@",kLocalizedString(@"Tips", @"获取验证码")] forState:UIControlStateNormal];
            weakSelf.sendBtn.userInteractionEnabled = YES;
        }];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
        [[Toast makeUpText:error] showWithType:ShortTime];
    }];
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)sumb:(id)sender {
    if (_codeText.text.length == 0) {
        [[Toast makeUpText:kLocalizedString(@"Enter_verification_Code", @"请输入验证码")] showWithType:ShortTime];
        return;
    }
    GlenGoogleViewController *google = [GlenGoogleViewController new];
    google.code = _codeText.text;
    [self.navigationController pushViewController:google animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
