//
//  GlenResetEmailViewController.h
//  GlenBit
//
//  Created by Lee on 2019/1/8.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBaseViewController.h"

@interface GlenResetEmailViewController : GlenBaseViewController
@property (assign, nonatomic) NSInteger type;///<1:重置邮箱 2:重置密码 3:重置手机号

@end
