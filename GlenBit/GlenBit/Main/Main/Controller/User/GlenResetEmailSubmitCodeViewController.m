//
//  GlenResetEmailSubmitCodeViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/8.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenResetEmailSubmitCodeViewController.h"
#import "GlenProgressView.h"
#import "GlenResetEmailNewEmailViewController.h"
#import "GlenNewMobileViewController.h"

@interface GlenResetEmailSubmitCodeViewController ()

@property (weak, nonatomic) IBOutlet GlenProgressView *progressView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet UIView *emaiView;
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation GlenResetEmailSubmitCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (_type == 3){
        [_progressView showProgressViewWithTitleArr:@[kLocalizedString(@"VERIFY", @"验证"),@"Confirm mobile",@"Enter mobile"] curentIndex:1];
    }else if (_type == 4){
        [_progressView showProgressViewWithTitleArr:@[@"Enter mobile",kLocalizedString(@"VERIFY", @"验证")] curentIndex:1];
    }else{
        [_progressView showProgressViewWithTitleArr:@[kLocalizedString(@"VERIFY", @"验证"),@"Confirm Email",@"Enter new mail"] curentIndex:_type];
    }
    [_backBtn setTitle:kLocalizedString(@"<Account", @"Account") forState:UIControlStateNormal];
    self.top.constant = StatusBar_height + 60;
    self.emaiView.layer.borderColor = [UIColor colorWithHexString:@"#353B41"].CGColor;
    self.emaiView.layer.borderWidth = 1;
}
- (void)setType:(NSInteger)type{
    _type = type;
}
- (void)setPhone:(NSString *)phone{
    _phone = phone;
}
- (void)setContryCount:(NSString *)contryCount{
    _contryCount = contryCount;
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)submit:(id)sender {
    GlenWeakSelf;
    if (_type == 2) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    if (_type == 3) {
        NSLog(@"新手机号");
        GlenNewMobileViewController *getCode = [GlenNewMobileViewController new];
        [self.navigationController pushViewController:getCode animated:YES];
        return;
    }
    if (_type == 4) {
        
        if (_emailText.text.length == 0) {
            [[Toast makeUpText:@"请输入验证码"] showWithType:ShortTime];
            return;
        }
        [[GlenHttpTool shareInstance] VerifySmsWithCountryCode:_contryCount phone:_phone code:_emailText.text Completion:^(NSString *succes) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:succes] showWithType:ShortTime];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:error] showWithType:ShortTime];;
        }];
        
        
        return;
    }
    GlenResetEmailNewEmailViewController *getCode = [GlenResetEmailNewEmailViewController new];
    [self.navigationController pushViewController:getCode animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
