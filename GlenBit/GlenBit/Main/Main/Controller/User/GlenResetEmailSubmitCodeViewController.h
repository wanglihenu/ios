//
//  GlenResetEmailSubmitCodeViewController.h
//  GlenBit
//
//  Created by Lee on 2019/1/8.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBaseViewController.h"

@interface GlenResetEmailSubmitCodeViewController : GlenBaseViewController

@property (assign, nonatomic) NSInteger type;///<1:旧 2新 3:旧重置手机号 4:新重置手机号

@property (strong, nonatomic)  NSString *phone;
@property (strong, nonatomic)  NSString *contryCount;

@end
