//
//  GlenResetEmailViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/8.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenResetEmailViewController.h"
#import "GlenProgressView.h"
#import "GlenResetEmailGetCodeViewController.h"
#import "GlenResetPassWordViewController.h"

@interface GlenResetEmailViewController ()
@property (weak, nonatomic) IBOutlet GlenProgressView *progressView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet UIView *emaiView;
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *detail;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;


@end

@implementation GlenResetEmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (_type == 1 ) {
        _titleLab.text = kLocalizedString(@"RESETEMAIL", @"重置邮箱地址");
        [_progressView showProgressViewWithTitleArr:@[kLocalizedString(@"VERIFY", @"验证"),@"Confirm Email",@"Enter new mail"] curentIndex:0];
    }else if (_type == 2){
        _titleLab.text = kLocalizedString(@"RESETLOGIN", @"验证");
        _detail.text = kLocalizedString(@"Verification_Code", @"验证码");
        [_progressView showProgressViewWithTitleArr:@[kLocalizedString(@"VERIFY", @"验证"),kLocalizedString(@"ENTERNEWPW", @"重置密码")] curentIndex:0];
        _emailText.placeholder = kLocalizedString(@"Enter_verification_Code", @"请输入验证码");
        [self getCode];
    }else if (_type == 3){
        _titleLab.text = kLocalizedString(@"RESETMobile", @"手机号");
        [_progressView showProgressViewWithTitleArr:@[kLocalizedString(@"VERIFY", @"验证"),@"Confirm mobile",@"Enter mobile"] curentIndex:0];
    }
    self.top.constant = StatusBar_height + 60;
    self.emaiView.layer.borderColor = [UIColor colorWithHexString:@"#353B41"].CGColor;
    self.emaiView.layer.borderWidth = 1;
    [_backBtn setTitle:kLocalizedString(@"<Account", @"Account") forState:UIControlStateNormal];
}
- (void)getCode{
    [self showHUBWith:@""];
    GlenWeakSelf;
    if ([UerManger shareUserManger].loginType == 0) {
        
        
        [[GlenHttpTool shareInstance] tryRestPassWordWithEmai:[UerManger shareUserManger].userModel.email Completion:^(NSString *succes) {
            [weakSelf hiddenHUB];

        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:error] showWithType:ShortTime];
        }];
    }else{
        [[GlenHttpTool shareInstance] tryRestPassWordWithPhone:[UerManger shareUserManger].phone  CountryCode:[UerManger shareUserManger].countyCode Completion:^(NSString *succes) {
            [weakSelf hiddenHUB];

        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:error] showWithType:ShortTime];
        }];
    }
}
- (void)setType:(NSInteger)type{
    _type = type;
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)submit:(id)sender {
    
    if (_type == 1 ) {
        GlenResetEmailGetCodeViewController *getCode = [GlenResetEmailGetCodeViewController new];
        getCode.type = _type;
        [self.navigationController pushViewController:getCode animated:YES];
    }else if (_type == 2){
        if (_emailText.text.length == 0) {
            [[Toast makeUpText:@"请输入验证码"] showWithType:ShortTime];
            return;
        }
        GlenResetPassWordViewController *getCode = [GlenResetPassWordViewController new];
        getCode.code = _emailText.text;
        [self.navigationController pushViewController:getCode animated:YES];
    }else if (_type == 3){
        GlenResetEmailGetCodeViewController *getCode = [GlenResetEmailGetCodeViewController new];
        getCode.type = _type;
        [self.navigationController pushViewController:getCode animated:YES];
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
