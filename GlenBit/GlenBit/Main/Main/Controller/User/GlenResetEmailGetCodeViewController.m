//
//  GlenResetEmailGetCodeViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/8.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenResetEmailGetCodeViewController.h"
#import "GlenProgressView.h"
#import "GlenResetEmailSubmitCodeViewController.h"

@interface GlenResetEmailGetCodeViewController ()

@property (weak, nonatomic) IBOutlet GlenProgressView *progressView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation GlenResetEmailGetCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (_type == 3){
        [_progressView showProgressViewWithTitleArr:@[kLocalizedString(@"VERIFY", @"验证"),@"Confirm mobile",@"Enter mobile"] curentIndex:1];
    }else if (_type == 4){
        [_progressView showProgressViewWithTitleArr:@[kLocalizedString(@"VERIFY", @"验证"),@"Confirm mobile",@"Enter mobile"] curentIndex:1];
    }
    
    else{
        [_progressView showProgressViewWithTitleArr:@[kLocalizedString(@"VERIFY", @"验证"),@"Confirm Email",@"Enter new mail"] curentIndex:1];
    }
    [_backBtn setTitle:kLocalizedString(@"<Account", @"Account") forState:UIControlStateNormal];
    self.top.constant = StatusBar_height + 60;
}
- (void)setType:(NSInteger)type{
    _type = type;
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)submit:(id)sender {
    if (_type == 4) {
        NSLog(@"完成修改手机号");
    }else{
        GlenResetEmailSubmitCodeViewController *getCode = [GlenResetEmailSubmitCodeViewController new];
        
        getCode.type = _type;
        
        [self.navigationController pushViewController:getCode animated:YES];
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
