//
//  GlenTradeHistoryDefineViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenTradeHistoryDefineViewController.h"
#import "GlenPickerView.h"



@interface GlenTradeHistoryDefineViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;

@property (weak, nonatomic) IBOutlet UIButton *dateFirst;
@property (weak, nonatomic) IBOutlet UIButton *dateSecond;
@property (weak, nonatomic) IBOutlet UIButton *coinFirst;
@property (weak, nonatomic) IBOutlet UIButton *coinSecond;
@property (weak, nonatomic) IBOutlet UIButton *typesFirst;
@property (weak, nonatomic) IBOutlet UIButton *reset;
@property (nonatomic, strong) GlenPickerView *pickerView;
@property (nonatomic, strong) SortSecondMarkets *selectMarckt;
@property (nonatomic, strong) Markets *selectModel;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSDate *beginTime;
@property (nonatomic, strong) NSDate *endTime;


@end

@implementation GlenTradeHistoryDefineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hideNav = YES;
    _top.constant = StatusBar_height;
    _dateFirst.layer.borderWidth = _dateSecond.layer.borderWidth = _coinFirst.layer.borderWidth = _coinSecond.layer.borderWidth = _typesFirst.layer.borderWidth = _reset.layer.borderWidth = 1;
    _dateFirst.layer.borderColor = _dateSecond.layer.borderColor = _coinFirst.layer.borderColor = _coinSecond.layer.borderColor = _typesFirst.layer.borderColor = _reset.layer.borderColor = [UIColor colorWithHexString:@"#B4B7BB"].CGColor;
    self.pickerView = [[GlenPickerView alloc]init];
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)select:(UIButton *)sender {
    GlenWeakSelf;
    if (sender.tag == 10){
        [BRStringPickerView showStringPickerWithTitle:nil dataSource:[UerManger shareUserManger].markes.targetCoins defaultSelValue:nil resultBlock:^(id selectValue, NSInteger row) {
            weakSelf.selectMarckt = [UerManger shareUserManger].markes.markes[row];
            [sender setTitle:selectValue forState:UIControlStateNormal];
        }];
    }else if (sender.tag == 11){
        if (_selectMarckt) {
            [BRStringPickerView showStringPickerWithTitle:nil dataSource:_selectMarckt.marketCoins defaultSelValue:nil resultBlock:^(id selectValue, NSInteger row) {
                weakSelf.selectModel = weakSelf.selectMarckt.markes[row];
                [sender setTitle:selectValue forState:UIControlStateNormal];
            }];
        }
    }else if (sender.tag == 12){
        [BRStringPickerView showStringPickerWithTitle:nil dataSource:@[@"buy",@"sell"] defaultSelValue:nil resultBlock:^(id selectValue, NSInteger row) {
            weakSelf.selectModel = weakSelf.selectMarckt.markes[row];
            weakSelf.type = selectValue;
            [sender setTitle:selectValue forState:UIControlStateNormal];
        }];
    }else if (sender.tag == 13){
        [BRDatePickerView showDatePickerWithTitle:nil dateType:BRDatePickerModeYMD defaultSelValue:nil minDate:nil maxDate:[NSDate date] isAutoSelect:NO themeColor:nil resultBlock:^(NSString *selectValue) {
            weakSelf.beginTime = [NSString dateFromString:selectValue];
            [sender setTitle:selectValue forState:UIControlStateNormal];
        }];
    }else if (sender.tag == 14){
        [BRDatePickerView showDatePickerWithTitle:nil dateType:BRDatePickerModeYMD defaultSelValue:nil minDate:self.beginTime maxDate:[NSDate date] isAutoSelect:NO themeColor:nil resultBlock:^(NSString *selectValue) {
            weakSelf.endTime = [NSString dateFromString:selectValue];
            [sender setTitle:selectValue forState:UIControlStateNormal];
        }];
    }
}
- (IBAction)searchBtn:(id)sender {
    if (_selectModel ||_type||_beginTime||_endTime) {
        if (self.select) {
            GlenContionModel *contionModel = [GlenContionModel new];
            contionModel.marke = _selectModel;
            contionModel.typeStr = _type;
            contionModel.beginTime = [NSString TimestampFromDate:_beginTime];
            contionModel.endTime = [NSString TimestampFromDate:_endTime];
            self.select(contionModel);
            [self.navigationController popViewControllerAnimated:YES];
        }
    }else{
        [[Toast makeUpText:@"请选择筛选条件"] showWithType:ShortTime];
    }
}
- (IBAction)resetBtn:(id)sender {
    _type = nil;
    _selectModel = nil;
    _beginTime = nil;
    _endTime = nil;
    [_coinFirst setTitle:kLocalizedString(@"ALL", @"所有") forState:UIControlStateNormal];
    [_coinSecond setTitle:kLocalizedString(@"ALL", @"所有") forState:UIControlStateNormal];
    [_typesFirst setTitle:kLocalizedString(@"ALL", @"所有") forState:UIControlStateNormal];
    [_dateFirst setTitle:kLocalizedString(@"ALL", @"所有") forState:UIControlStateNormal];
    [_dateSecond setTitle:kLocalizedString(@"ALL", @"所有") forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
