//
//  GlenOrderViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenOrderViewController.h"
#import "GlenOrderHeadCollectionReusableView.h"
#import "GlenContiorCollectionReusableView.h"
#import "GlenTradeCondiorCollectionReusableView.h"
#import "GlenOrderCollectionViewCell.h"
#import "GlenOpenOrderDefineViewController.h"
#import "GlenTradeHistoryDefineViewController.h"
#import "UIScrollView+DRRefresh.h"
#import "NODATACollectionViewCell.h"

#import "GlenAletView.h"
#import "GKCover.h"

@interface GlenOrderViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) NSIndexPath *selectIndex;
@property (strong, nonatomic) NSMutableArray *openOrder;
@property (strong, nonatomic) NSMutableArray *histroyOrder;
@property (strong, nonatomic) NSMutableArray *openTypeOrder;

@property (assign, nonatomic) NSInteger historyPage;
@property (strong, nonatomic) GlenContionModel *openContion;
@property (strong, nonatomic) GlenContionModel *historyContion;

@end

@implementation GlenOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _historyPage = 1;
    [self.view addSubview:self.collectionView];
    [self addresh];
    [self getOpen];
}
- (void)upDataLoca{
    self.title = kLocalizedString(@"ORDERS", @"委托记录");
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    if (_currentIndex == 0) {
//        [self getOpen];
//        [_collectionView hideFooterRefresh];
//    }else{
//        [self getHistory];
//        [_collectionView openFooterRefresh];
//    }
}
- (void)addresh{
    GlenWeakSelf;
    [_collectionView setRefreshWithHeaderBlock:^{
        if (weakSelf.currentIndex == 0) {
            [weakSelf getOpen];
        }else{
            [weakSelf getHistory];
        }
    } footerBlock:^{
        weakSelf.historyPage ++ ;
        [weakSelf getHistory];
    }];
}
- (void)getOpen{
    GlenWeakSelf;
    [self showHUBWith:@""];
    [weakSelf.openTypeOrder removeAllObjects];
    [weakSelf.openOrder removeAllObjects];
    [[GlenHttpTool shareInstance] orderOpenPair:_openContion.marke Completion:^(NSArray *orders) {
        [weakSelf hiddenHUB];
        NSSortDescriptor *ageSD = [NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:NO];//ascending:YES
        
        [weakSelf.openOrder addObjectsFromArray:[orders sortedArrayUsingDescriptors:@[ageSD]]];
        if (weakSelf.openContion) {
            [weakSelf.openTypeOrder removeAllObjects];
            if (weakSelf.openContion.typeStr.length) {
                for (orderModel *model in weakSelf.openOrder) {
                    if ([model.type isEqualToString:weakSelf.openContion.type == 0?@"buy":@"sell"]) {
                        [weakSelf.openTypeOrder addObject:model];
                    }
                }
            }else{
                [weakSelf.openTypeOrder addObjectsFromArray:orders];
            }
            
        }
        [weakSelf.collectionView footerEndRefreshing];
        [weakSelf.collectionView headerEndRefreshing];
        [weakSelf.collectionView reloadData];
    } fail:^(NSString *error) {
        [weakSelf.collectionView footerEndRefreshing];
        [weakSelf.collectionView headerEndRefreshing];
        [weakSelf hiddenHUB];
        [[Toast makeUpText:error] showWithType:ShortTime];
    }];
}
- (void)getHistory{
    GlenWeakSelf;
    if (!_historyContion.marke) {
        [[Toast makeUpText:@"请选择交易对"] showWithType:ShortTime];
        return;
    }
    [self showHUBWith:@""];
    if (_historyPage == 1) {
        [self.histroyOrder removeAllObjects];
    }
    [[GlenHttpTool shareInstance] orderDealtPair:_historyContion page:_historyPage limit:10 Completion:^(NSArray *orders) {
        [weakSelf hiddenHUB];
        NSSortDescriptor *ageSD = [NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:NO];//ascending:YES
        [weakSelf.collectionView footerEndRefreshing];
        [weakSelf.collectionView headerEndRefreshing];
        [weakSelf.histroyOrder addObjectsFromArray:[orders sortedArrayUsingDescriptors:@[ageSD]]];
        [weakSelf.collectionView reloadData];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
        [weakSelf.collectionView footerEndRefreshing];
        [weakSelf.collectionView headerEndRefreshing];
        [[Toast makeUpText:error] showWithType:ShortTime];
    }];
}
- (void)setCurrentIndex:(NSInteger)currentIndex{
    _currentIndex = currentIndex;
    [_collectionView hideHeaderRefresh];
    if (currentIndex == 0) {
        [self getOpen];
        [_collectionView hideFooterRefresh];
    }else{
        [self getHistory];
        [_collectionView openFooterRefresh];
    }
}
- (NSMutableArray *)openOrder{
    if (!_openOrder) {
        _openOrder = [NSMutableArray new];
    }
    return _openOrder;
}
- (NSMutableArray *)openTypeOrder{
    if (!_openTypeOrder) {
        _openTypeOrder = [NSMutableArray new];
    }
    return _openTypeOrder;
}
- (NSMutableArray *)histroyOrder{
    if (!_histroyOrder) {
        _histroyOrder = [NSMutableArray new];
    }
    return _histroyOrder;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }else if (section == 1){
        if (_currentIndex == 0) {
            if (_openContion) {
                return _openTypeOrder.count?:1;
            }else{
                return _openOrder.count?:1;
            }
        }else{
            return _histroyOrder.count?:1;
        }
    }
    return 0;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeZero;
    }else if (indexPath.section == 1){
        if ([indexPath compare:_selectIndex] == NSOrderedSame) {
            if (_currentIndex == 0) {
                return CGSizeMake(size_width, 132);
            }else if (_currentIndex == 1){
                return CGSizeMake(size_width, 66);
            }
            
        }
        return CGSizeMake(size_width, 66);
    }
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGSizeMake(size_width, 145);
    }else if (section == 1){
        if (_currentIndex == 0) {
            return CGSizeMake(size_width, 200);
        }else if (_currentIndex == 1){
            return CGSizeMake(size_width, 200);
        }
        
    }else{
        return CGSizeZero;
    }
    return CGSizeZero;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    GlenWeakSelf;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section == 0) {
            GlenOrderHeadCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenOrderHeadCollectionReusableView" forIndexPath:indexPath];
            footer.currentIndex = _currentIndex;
            
            footer.changeCurrent = ^(NSInteger index) {
                weakSelf.selectIndex = nil;
                weakSelf.currentIndex = index;
                [weakSelf.collectionView reloadData];
            };
            return footer;
        }
        if (indexPath.section == 1) {
            if (_currentIndex == 0) {
                GlenContiorCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenContiorCollectionReusableView" forIndexPath:indexPath];
                footer.model = _openContion;
            
                footer.selectContior = ^{
                    GlenOpenOrderDefineViewController *defin = [GlenOpenOrderDefineViewController new];
                    defin.ty = 0;
                    defin.select = ^(GlenContionModel *contion) {
                        [weakSelf.collectionView reloadData];
                        weakSelf.openContion = contion;
                        [weakSelf getOpen];
                    };
                    [weakSelf.navigationController pushViewController:defin animated:YES];
                };
                return footer;
            }else if (_currentIndex == 1){
                GlenContiorCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenContiorCollectionReusableView" forIndexPath:indexPath];
                footer.model = _historyContion;
                footer.selectContior = ^{
                    GlenOpenOrderDefineViewController *defin = [GlenOpenOrderDefineViewController new];
                    defin.ty = 1;
                    defin.select = ^(GlenContionModel *contion) {
                        weakSelf.historyPage = 1;
                        [weakSelf.collectionView reloadData];
                        weakSelf.historyContion = contion;
                        [weakSelf getHistory];
                    };
                    [weakSelf.navigationController pushViewController:defin animated:YES];
                };
                return footer;
            }
            
        }
    }
    return nil;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GlenWeakSelf;
    if (indexPath.section == 1){
        
        if (_currentIndex == 0) {
            if (_openContion) {
                if (_openTypeOrder.count) {
                    GlenOrderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenOrderCollectionViewCell" forIndexPath:indexPath];
                    orderModel *temModel = _openOrder[indexPath.row];
                    
                    temModel.pair = temModel.pair.length?temModel.pair:[NSString stringWithFormat:@"%@-%@",_openContion.marke.targetCoin,_openContion.marke.marketCoin];
                    cell.mode = _openTypeOrder[indexPath.row];
                    cell.openAsset = ^(NSInteger index) {
                        orderModel *model = weakSelf.openTypeOrder[indexPath.row];
                        [weakSelf cancel:model];
                    };
                    return cell;
                }else{
                    NODATACollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NODATACollectionViewCell" forIndexPath:indexPath];
                    return cell;
                }
            }else{
                if (_openOrder.count) {
                    GlenOrderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenOrderCollectionViewCell" forIndexPath:indexPath];
                    cell.mode = _openOrder[indexPath.row];
                    cell.openAsset = ^(NSInteger index) {
                        orderModel *model = weakSelf.openOrder[indexPath.row];
                        [weakSelf cancel:model];
                    };
                    return cell;
                }else{
                    NODATACollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NODATACollectionViewCell" forIndexPath:indexPath];
                    return cell;
                }
            }
            
        }else if (_currentIndex == 1){
            if (_histroyOrder.count) {
                GlenOrderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenOrderCollectionViewCell" forIndexPath:indexPath];
                orderModel *temModel = _histroyOrder[indexPath.row];
                temModel.pair = [NSString stringWithFormat:@"%@-%@",_historyContion.marke.targetCoin,_historyContion.marke.marketCoin];
                cell.mode = temModel;
                cell.openAsset = ^(NSInteger index) {
                    
                };
                return cell;
            }else{
                NODATACollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NODATACollectionViewCell" forIndexPath:indexPath];
                return cell;
            }

        }
    }
    
    
    return nil;
}
- (void)cancel:(orderModel *)order{
    GlenWeakSelf;
    GlenAletView *alet = [[GlenAletView alloc]initWithFrame:CGRectMake(0, 0, 250 , 140)];
    [alet showTitle:[NSString stringWithFormat:@"%@?",kLocalizedString(@"CANCEL", @"")] confirm:^{
        [GKCover hide];
        [weakSelf showHUBWith:@""];
        
        [[GlenHttpTool shareInstance]orderCancelPair:order.pair type:order.type orderId:order.orderId Completion:^(NSString *succes) {
            [weakSelf hiddenHUB];
            [weakSelf getOpen];
            weakSelf.selectIndex = nil;
        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:error] showWithType:ShortTime];
        }];
        
    } cancel:^{
        [GKCover hide];
    }];
    [GKCover coverFrom:[UIApplication sharedApplication].keyWindow contentView:alet style:GKCoverStyleTranslucent showStyle:GKCoverShowStyleCenter showAnimStyle:GKCoverShowAnimStyleCenter hideAnimStyle:GKCoverHideAnimStyleCenter notClick:NO showBlock:^{
        
    } hideBlock:^{
        
    }];
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        _selectIndex = indexPath;
        [collectionView reloadData];
    }
}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        // 布局对象
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.minimumLineSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, StatusBar_height + 60, size_width, size_height - 60 - StatusBar_height) collectionViewLayout:flowLayout];
        _collectionView.delegate=self;
        _collectionView.dataSource=self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenOrderCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenOrderCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"NODATACollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"NODATACollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenOrderHeadCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenOrderHeadCollectionReusableView"];
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenContiorCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenContiorCollectionReusableView"];
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenTradeCondiorCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenTradeCondiorCollectionReusableView"];
    }
    return _collectionView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
