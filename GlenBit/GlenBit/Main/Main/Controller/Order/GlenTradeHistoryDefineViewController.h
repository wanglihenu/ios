//
//  GlenTradeHistoryDefineViewController.h
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBaseViewController.h"

@interface GlenTradeHistoryDefineViewController : GlenBaseViewController

@property (nonatomic, copy) void(^select)(GlenContionModel *contion);

@end
