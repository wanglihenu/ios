//
//  GlenOpenOrderDefineViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenOpenOrderDefineViewController.h"
#import "GlenPickerView.h"




@interface GlenOpenOrderDefineViewController ()
@property (weak, nonatomic) IBOutlet UIButton *coinFirst;
@property (weak, nonatomic) IBOutlet UIButton *coinSecond;
@property (weak, nonatomic) IBOutlet UIButton *typesFirst;
@property (weak, nonatomic) IBOutlet UIButton *reset;
@property (weak, nonatomic) IBOutlet UIButton *se;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;

@property (nonatomic, strong) GlenPickerView *pickerView;

@property (nonatomic, strong) SortSecondMarkets *selectMarckt;
@property (nonatomic, strong) Markets *selectModel;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, assign) NSInteger typeRow;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@property (weak, nonatomic) IBOutlet UILabel *coinStr;
@property (weak, nonatomic) IBOutlet UILabel *typeStr;
@end

@implementation GlenOpenOrderDefineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hideNav = YES;
    _top.constant = StatusBar_height;
    _titleLab.text = kLocalizedString(@"Refine", @"");
    
    _coinStr.text = kLocalizedString(@"PAIR", @"");
    _typeStr.text = kLocalizedString(@"BUY_OR_SELL", @"");
    [_reset setTitle:kLocalizedString(@"RESET", @"") forState:UIControlStateNormal];
    [_se setTitle:kLocalizedString(@"SEARCH", @"") forState:UIControlStateNormal];
    
    _coinFirst.layer.borderWidth = _coinSecond.layer.borderWidth = _typesFirst.layer.borderWidth = _reset.layer.borderWidth = 1;
    _coinFirst.layer.borderColor = _coinSecond.layer.borderColor = _typesFirst.layer.borderColor = _reset.layer.borderColor = [UIColor colorWithHexString:@"#B4B7BB"].CGColor;
    self.pickerView = [[GlenPickerView alloc]init];
    [_coinFirst setTitle:kLocalizedString(@"ALL", @"所有") forState:UIControlStateNormal];
    [_coinSecond setTitle:kLocalizedString(@"ALL", @"所有") forState:UIControlStateNormal];
    [_typesFirst setTitle:kLocalizedString(@"ALL", @"所有") forState:UIControlStateNormal];
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)setTy:(NSInteger)ty{
    _ty = ty;
}
- (IBAction)select:(UIButton *)sender {
    GlenWeakSelf;
    if (sender.tag == 10){
        
        
        [BRStringPickerView showStringPickerWithTitle:nil dataSource:[UerManger shareUserManger].markes.targetCoins defaultSelValue:nil resultBlock:^(id selectValue, NSInteger row) {
            weakSelf.selectMarckt = [UerManger shareUserManger].markes.markes[row];
            [sender setTitle:selectValue forState:UIControlStateNormal];
        }];
    }else if (sender.tag == 11){
        if (_selectMarckt) {
            [BRStringPickerView showStringPickerWithTitle:nil dataSource:_selectMarckt.marketCoins defaultSelValue:nil resultBlock:^(id selectValue, NSInteger row) {
                weakSelf.selectModel = weakSelf.selectMarckt.markes[row];
                [sender setTitle:selectValue forState:UIControlStateNormal];
            }];
        }
    }else if (sender.tag == 12){
        
        [BRStringPickerView showStringPickerWithTitle:nil dataSource:@[kLocalizedString(@"BUY", @""),kLocalizedString(@"SELL", @"")] defaultSelValue:nil resultBlock:^(id selectValue, NSInteger row) {
            weakSelf.type = selectValue;
            weakSelf.typeRow = row;
            [sender setTitle:selectValue forState:UIControlStateNormal];
        }];
    }
}
- (IBAction)searchBtn:(id)sender {
    if (_ty == 0) {
        if (_selectModel ||_type) {
            if (self.select) {
                GlenContionModel *contionModel = [GlenContionModel new];
                contionModel.marke = _selectModel;
                contionModel.typeStr = _type;
                contionModel.type = _typeRow;
                self.select(contionModel);
                [self.navigationController popViewControllerAnimated:YES];
            }
        }else{
            if (self.select) {;
                self.select(nil);
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }else{
        if (_selectModel) {
            if (self.select) {
                GlenContionModel *contionModel = [GlenContionModel new];
                contionModel.marke = _selectModel;
                contionModel.typeStr = _type;
                contionModel.type = _typeRow;
                self.select(contionModel);
                [self.navigationController popViewControllerAnimated:YES];
            }
        }else{
            [[Toast makeUpText:@"请选择交易对"] showWithType:ShortTime];
        }
    }
}
- (IBAction)resetBtn:(id)sender {
    _type = nil;
    _selectModel = nil;
    [_coinFirst setTitle:kLocalizedString(@"ALL", @"所有") forState:UIControlStateNormal];
    [_coinSecond setTitle:kLocalizedString(@"ALL", @"所有") forState:UIControlStateNormal];
    [_typesFirst setTitle:kLocalizedString(@"ALL", @"所有") forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
