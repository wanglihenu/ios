//
//  GlenOpenOrderDefineViewController.h
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBaseViewController.h"

@interface GlenOpenOrderDefineViewController : GlenBaseViewController

@property (nonatomic, copy) void(^select)(GlenContionModel *contion);
@property (nonatomic,assign) NSInteger ty;

@end
