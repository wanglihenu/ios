//
//  GlenLeftMenuViewController.m
//  Glenbit
//
//  Created by Lee on 2019/1/3.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenRightMenuViewController.h"
#import "GlenMenuCollectionViewCell.h"
#import "GlenMenuCollectionReusableView.h"
#import "GlenMenuCloseCollectionReusableView.h"
#import "GlenAssetViewController.h"
#import "GlenOrderViewController.h"

@interface GlenRightMenuViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *dataArr;
@end

@implementation GlenRightMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
    self.hideNav = YES;
}
- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc]initWithArray:@[@{@"title":@"",@"list":@[@{@"title":@"Home",@"Class":@"GlenMainViewController"},@{@"title":@"Exchange",@"Class":@""}]},@{@"title":@"Wallet",@"list":@[@{@"title":@"Asset Rortfolio",@"Class":@"GlenAssetViewController"},@{@"title":@"Deposit Records",@"Class":@"GlenAssetViewController"},@{@"title":@"Withdrawal Records",@"Class":@"GlenAssetViewController"}]},@{@"title":@"Orders",@"list":@[@{@"title":@"Open Orders",@"Class":@"GlenOrderViewController"},@{@"title":@"Deposit Records",@"Class":@"GlenOrderViewController"}]}]];
    }
    return _dataArr;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSDictionary *dic = self.dataArr[section];
    NSArray *arr = dic[@"list"];
    return arr.count;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.dataArr.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(rightWidth, 44);
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGSizeMake(rightWidth, 60);
    }
    NSDictionary *dic = self.dataArr[section];
    NSString *titlte = dic[@"title"];
    if (titlte.length) {
        return CGSizeMake(rightWidth, 45);
    }else{
        return CGSizeZero;
    }
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    
    
    NSDictionary *dic = self.dataArr[indexPath.section];
    NSString *titlte = dic[@"title"];
    if (titlte.length) {
        if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
            GlenMenuCollectionReusableView *head = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenMenuCollectionReusableView" forIndexPath:indexPath];
            
            [head setStr:titlte];
            return head;
        }
    }else{
        if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
            GlenWeakSelf;
            GlenMenuCloseCollectionReusableView *head = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenMenuCloseCollectionReusableView" forIndexPath:indexPath];
            head.closeRight = ^{
                [weakSelf.mm_drawerController closeDrawerAnimated:YES completion:nil];
            };
            return head;
        }
    }
    
    return nil;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dic = self.dataArr[indexPath.section][@"list"][indexPath.row];
    NSString *tepStr = dic[@"title"];
    
    GlenMenuCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenMenuCollectionViewCell" forIndexPath:indexPath];
    [cell setStr:tepStr];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = self.dataArr[indexPath.section][@"list"][indexPath.row];
    NSString *className = dic[@"Class"];
    Class vc = NSClassFromString(className);
    UIViewController *view = [vc new];
    if ([view isKindOfClass:[GlenAssetViewController class]]) {
        GlenAssetViewController *kc = (GlenAssetViewController *)view;
        kc.currentIndex = indexPath.row;
    }
    if ([view isKindOfClass:[GlenOrderViewController class]]) {
        GlenOrderViewController *kc = (GlenOrderViewController *)view;
        kc.currentIndex = indexPath.row;
    }
    UINavigationController * nav = [[GlenNavBaseViewController alloc] initWithRootViewController:view];
    
    
    
    [self.mm_drawerController
     setCenterViewController:nav
     withCloseAnimation:YES
     completion:nil];
}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        // 布局对象
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.minimumLineSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, StatusBar_height, rightWidth, size_height - StatusBar_height) collectionViewLayout:flowLayout];
        _collectionView.delegate=self;
        _collectionView.dataSource=self;
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"#000913"];
        _collectionView.showsVerticalScrollIndicator = NO;
        
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenMenuCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenMenuCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenMenuCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenMenuCollectionReusableView"];
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenMenuCloseCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenMenuCloseCollectionReusableView"];
        
    }
    return _collectionView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
