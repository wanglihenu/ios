//
//  GlenMainViewController.m
//  Glenbit
//
//  Created by Lee on 2019/1/3.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenMainViewController.h"
#import "GlenCycleCollectionViewCell.h"
#import "GlenMainSearchCollectionReusableView.h"
#import "GlenMainCollectionViewCell.h"
#import "GlenSelcetTableViewCell.h"
#import <YBPopupMenu/YBPopupMenu.h>
#import "FLSocketManager.h"
#import "HomeModel.h"
#import <libkern/OSAtomic.h>
#import "GlenExchangeViewController.h"

@interface GlenMainViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YBPopupMenuDelegate>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) UITableView *tableview;

@property (strong, nonatomic) NSMutableArray *detaArr;
@property (strong, nonatomic) NSMutableArray *searchRelutArr;
@property (strong, nonatomic) NSIndexPath *selectIndex;
@property (assign, nonatomic) BOOL issearch;
@property (strong, nonatomic) NSString *search;

@property (strong, nonatomic) NSString *Nowsearch;
@property (strong, nonatomic) NSString *markteSearch;
@property (strong, nonatomic) NSMutableArray *emark;
@end

@implementation GlenMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
    [self getMarket];
    [self getSorct];
}
- (NSMutableArray *)emark{
    if (!_emark) {
        _emark = [NSMutableArray new];
    }
    return _emark;
}
- (void)getMarket{
    [self showHUBWith:@""];
    GlenWeakSelf;
    [[GlenHttpTool shareInstance] marketsCompletion:^(NSArray *coins) {
        [weakSelf.emark addObjectsFromArray:coins];
        NSMutableSet *set = [NSMutableSet new];
        for (Markets *market in coins) {
            [set addObject:market.targetCoin];
        }
        SortMarkets *mark = [SortMarkets new];
        for (NSString *key in set) {
            SortSecondMarkets *second = [SortSecondMarkets new];
            NSPredicate  *predicate = [NSPredicate predicateWithFormat:@"targetCoin CONTAINS %@",key];
            second.markes = [coins filteredArrayUsingPredicate:predicate];
            for (Markets *market in second.markes) {
                [second.marketCoins addObject:market.marketCoin];
            }
            [mark.targetCoins addObject:key];;
            [mark.markes addObject:second];
        }
        [UerManger shareUserManger].markes = mark;
        [weakSelf getMarkPrice];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
    }];
}
- (void)getMarkPrice{
    GlenWeakSelf;
    [[GlenHttpTool shareInstance] marketPricesCompletion:^(NSArray *coins) {
        NSMutableArray *a = [NSMutableArray new];
        [a addObjectsFromArray:coins];
        [a enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            marketPrices *p = obj;
            
            if (p.price.floatValue == 1) {
                *stop = YES;
                
                if (*stop == YES) {
                    [a removeObject:p];
                }
                *stop = NO;
            }
        }];
        [UerManger shareUserManger].marke_price = a;
        
        [[GlenHttpTool shareInstance]coinsCompletion:^(NSArray *coins) {
            [weakSelf hiddenHUB];
            
            [UerManger shareUserManger].coins = coins;
            NSSortDescriptor *ageSD = [NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES];//ascending:YES
            
            [UerManger shareUserManger].coins = [[[UerManger shareUserManger].coins sortedArrayUsingDescriptors:@[ageSD]] mutableCopy];
        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
        }];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
    }];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    GlenWeakSelf;
    [[GlenHttpTool shareInstance] profileCompletion:^(UserModel *user) {
        [weakSelf updata];
    } fail:^(NSString *error) {
        
    }];
}
- (void)upDataLoca{
    self.title = kLocalizedString(@"HOME", @"主页");
}
- (void)getSorct{
    GlenWeakSelf;
    dispatch_semaphore_t signal = dispatch_semaphore_create(1);
    dispatch_time_t overTime = dispatch_time(DISPATCH_TIME_NOW, 0);
    
    NSString *url = @"wss://stream-tickers.glenbit.com/websocket";
    [[FLSocketManager shareManager] fl_open:url connect:^{
        NSLog(@"成功连接");
    } receive:^(id message, FLSocketReceiveType type) {
        NSData *jsonData = nil;
        if ([message isKindOfClass:[NSString class]]) {
            jsonData = [(NSString *)message dataUsingEncoding : NSUTF8StringEncoding];
        }
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:NULL];
        HomeModel *model = [HomeModel yy_modelWithDictionary:dic];
        if (model.pair.length >0) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_semaphore_wait(signal, overTime); //signal 值 -1
                NSLog(@"线程1 等待ing");
                
                
                for (int i = 0; i < weakSelf.detaArr.count; i ++) {
                    HomeModel *tem = weakSelf.detaArr [i];
                    if ([tem isEqual:model]) {//数组中已经存在该对象
                        [weakSelf.detaArr replaceObjectAtIndex:i withObject:model];
                        
                        break;
                    }
                }
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    NSNotification *notification =[NSNotification notificationWithName:NOTIFTICKER object:nil userInfo:@{@"tiker":weakSelf.detaArr}];;
                    [[NSNotificationCenter defaultCenter] postNotification:notification];
                    [UerManger shareUserManger].tikes = weakSelf.detaArr;
                    if (weakSelf.issearch) {
                        [weakSelf getSearchArr:weakSelf.Nowsearch];
                    }else{
                        [weakSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:1]];
                    }
                });
                dispatch_semaphore_signal(signal); //signal 值 +1
                NSLog(@"线程1 发送信号");
            });
        }else if(dic.allKeys.count > 0){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSLog(@"线程2 等待ing");
                dispatch_semaphore_wait(signal, overTime);
                [weakSelf.detaArr removeAllObjects];
                NSArray *allKey = dic.allKeys;
                for (NSString *key in allKey) {
                    NSDictionary *tempDic = dic[key];
                    HomeModel *model = [HomeModel yy_modelWithDictionary:tempDic];
                    [weakSelf.detaArr addObject:model];
                }
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    NSNotification *notification =[NSNotification notificationWithName:NOTIFTICKER object:nil userInfo:@{@"tiker":weakSelf.detaArr}];;
                    [[NSNotificationCenter defaultCenter] postNotification:notification];
                    [UerManger shareUserManger].tikes = weakSelf.detaArr;
                    if (weakSelf.issearch) {
                        [weakSelf getSearchArr:weakSelf.Nowsearch];
                    }else{
                        [weakSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:1]];
                    }
                    
                });
                
                dispatch_semaphore_signal(signal);
                NSLog(@"线程2 发送信号");
            });
            
        }
        
    } failure:^(NSError *error) {
        NSLog(@"连接失败");
    }];
}
- (NSMutableArray *)detaArr{
    if (!_detaArr) {
        _detaArr = [NSMutableArray new];
    }
    return _detaArr;
}
- (NSMutableArray *)searchRelutArr{
    if (!_searchRelutArr) {
        _searchRelutArr = [NSMutableArray new];
    }
    return _searchRelutArr;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section > 0){
        return _issearch?self.searchRelutArr.count:self.detaArr.count;
    }
    return 0;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake(size_width, size_width);
    }else if (indexPath.section > 0){
        return CGSizeMake(size_width, 66);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return CGSizeMake(size_width, 84);
    }else{
        return CGSizeZero;
    }
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    GlenWeakSelf;
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        if (indexPath.section == 0) {
            GlenMainSearchCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"GlenMainSearchCollectionReusableView" forIndexPath:indexPath];
            [footer setCoin:_selectIndex.row == 0?nil: [UerManger shareUserManger].coins[_selectIndex.row - 1] select:_selectIndex];
            footer.select = ^(UIView *view) {
                
                CGRect absoluteRect = [view convertRect:view.bounds toView:weakSelf.collectionView];
                
                [weakSelf.collectionView setContentOffset:CGPointMake(0, absoluteRect.origin.y)];
                
                [YBPopupMenu showRelyOnView:view titles:[UerManger shareUserManger].coins icons:nil menuWidth:size_width otherSettings:^(YBPopupMenu *popupMenu) {
                    popupMenu.dismissOnSelected = NO;
                    //                    popupMenu.isShowShadow = YES;
                    popupMenu.delegate = self;
                    popupMenu.itemHeight = 64;
                    popupMenu.minSpace = 0;
                    popupMenu.arrowWidth = popupMenu.arrowHeight = 0;
                    weakSelf.tableview = popupMenu.tableView;
                }];
            };
            footer.search = ^(NSString *search) {
                if (search.length == 0) {
                    weakSelf.issearch = NO;
                    [weakSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:1]];
                }else{
                    weakSelf.issearch = YES;
                }
                
                weakSelf.Nowsearch = search;
                weakSelf.selectIndex = nil;
                weakSelf.search = search;
                [weakSelf getSearchArr:search];
            };
            return footer;
        }
    }
    return nil;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        GlenCycleCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenCycleCollectionViewCell" forIndexPath:indexPath];
        return cell;
    }else if (indexPath.section > 0){
        GlenMainCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenMainCollectionViewCell" forIndexPath:indexPath];
        cell.homeModel = _issearch?_searchRelutArr[indexPath.row]:_detaArr[indexPath.row];
        cell.type = 0;
        return cell;
    }
    
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section > 0) {
        HomeModel *ho = _issearch?_searchRelutArr[indexPath.row]:_detaArr[indexPath.row];
        Markets *mak;
        for (Markets *ma in _emark) {
            if ([ho.pair isEqualToString:[NSString stringWithFormat:@"%@-%@",ma.targetCoin,ma.marketCoin]]) {
                NSLog(@"2222");
                mak = ma;
                break;
            }
        }
       UINavigationController *root = self.cyl_tabBarController.viewControllers[1];
        [root popToRootViewControllerAnimated:NO];
        GlenExchangeViewController *kk = root.viewControllers[0];
        kk.marketModel = mak;
        self.cyl_tabBarController.selectedIndex = 1;
    }
}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        // 布局对象
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.minimumLineSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, StatusBar_height + 60, size_width, size_height - TabBarHeight - StatusBar_height) collectionViewLayout:flowLayout];
        _collectionView.delegate=self;
        _collectionView.dataSource=self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        
        
        [_collectionView registerClass:[GlenCycleCollectionViewCell class] forCellWithReuseIdentifier:@"GlenCycleCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenMainCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenMainCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenMainSearchCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"GlenMainSearchCollectionReusableView"];
    }
    return _collectionView;
}
#pragma mark - YBPopupMenuDelegate
- (void)ybPopupMenu:(YBPopupMenu *)ybPopupMenu didSelectedAtIndex:(NSInteger)index
{
    _selectIndex = [NSIndexPath indexPathForRow:index inSection:0];
    if (index == 0) {
        _issearch = NO;
        [self.collectionView reloadData];
        [self.tableview reloadData];
        [ybPopupMenu dismiss];
        return;
    }
    _issearch = YES;
    [self.collectionView reloadData];
    //推荐回调
    Coins *c = [UerManger shareUserManger].coins[index - 1];
    _markteSearch = c._id;
    [self getSearchArr:c._id];
    [self.tableview reloadData];
    [ybPopupMenu dismiss];
}
- (UITableViewCell *)ybPopupMenu:(YBPopupMenu *)ybPopupMenu cellForRowAtIndex:(NSInteger)index
{
    static NSString * identifier = @"GlenSelcetTableViewCell";
    GlenSelcetTableViewCell * cell = [ybPopupMenu.tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"GlenSelcetTableViewCell" owner:self options:nil] firstObject];
    }
    if (index == 0) {
        Coins *co = [Coins new];
        co._id = kLocalizedString(@"ALL", @"");
        [cell setTitle:co selectIndex:_selectIndex current:[NSIndexPath indexPathForRow:index inSection:0]];
        return cell;
    }
    [cell setTitle:[UerManger shareUserManger].coins[index - 1] selectIndex:_selectIndex current:[NSIndexPath indexPathForRow:index inSection:0]];
    return cell;
}
- (void)getSearchArr:(NSString *)search{
    if (_Nowsearch.length == 0 && _markteSearch.length == 0) {
        return;
    }
    
    [self.searchRelutArr removeAllObjects];
    if (_Nowsearch.length ||_markteSearch.length) {
        for (HomeModel *coin in _detaArr) {
            if (_Nowsearch.length && _markteSearch.length) {
                if ([coin.marketCoin.lowercaseString containsString:_markteSearch.lowercaseString ]&&[coin.pair.lowercaseString containsString:_Nowsearch]) {
                    [_searchRelutArr addObject:coin];
                }
            }else if (_Nowsearch.length && !_markteSearch.length){
                if ([coin.pair.lowercaseString containsString:_Nowsearch]) {
                    [_searchRelutArr addObject:coin];
                }
            }else if (!_Nowsearch.length && _markteSearch.length){
                if ([coin.marketCoin.lowercaseString containsString:_markteSearch.lowercaseString ]) {
                    [_searchRelutArr addObject:coin];
                }
            }
            
        }
    }
    
    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:1]];
}
-(NSMutableArray *)getOutRepeatObjectFrom:(NSArray *)arr{
    NSMutableArray *needArr = [[NSMutableArray alloc]init];
    for (HomeModel *model in arr) {
        __block BOOL isExist = NO;
        [needArr enumerateObjectsUsingBlock:^(HomeModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isEqual:model]) {//数组中已经存在该对象
                *stop = YES;
                isExist = YES;
                [needArr replaceObjectAtIndex:idx withObject:model];
            }
        }];
        if (!isExist) {//如果不存在就添加进去
            [needArr addObject:model];
        }
    }
    
    return needArr;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
