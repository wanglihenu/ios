//
//  GlenBuyViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBuyViewController.h"
#import "GlenExchangeTabCollectionReusableView.h"
#import "GlenExchangeRataCollectionReusableView.h"
#import "GlenExchangeCollectionViewCell.h"
#import "GlenExchangeCollectionReusableView.h"
#import "GlenExchangeKLineCollectionViewCell.h"
#import "GlenExchangeKlineSelectReusableView.h"

#import "GlenBuyCollectionViewCell.h"

#import "ExchangeModel.h"


@interface GlenBuyViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottom;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;

@property (weak, nonatomic) IBOutlet UILabel *titlteLab;


@property (assign, nonatomic) NSInteger type;


@property (strong, nonatomic) exchangeDetail *best;
@property (strong, nonatomic) exchangeDetail *select;



@property (strong, nonatomic) NSString *price;
@property (strong, nonatomic) NSString *amount;
@property (strong, nonatomic) NSString *maxvalue;
@property (weak, nonatomic) IBOutlet UIButton *actio;

@property (strong, nonatomic) NSArray *tikers;
@property (strong, nonatomic) HomeModel *tikeDetal;

@end

@implementation GlenBuyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _type = 1;
    _bottom.constant = SafeMargin;
    _top.constant = StatusBar_height;
    if (_exChangetype == 0) {
        _titlteLab.text = kLocalizedString(@"BUY", @"BUY");
        [_actio setTitle:kLocalizedString(@"BUY", @"BUY") forState:UIControlStateNormal];
    }else{
        _titlteLab.text = kLocalizedString(@"SELL", @"SELL");
        [_actio setTitle:kLocalizedString(@"SELL", @"SELL") forState:UIControlStateNormal];
        _actio.backgroundColor = [UIColor colorWithHexString:@"#F04A5D"];
    }
    self.view.backgroundColor = [UIColor colorWithHexString:@"#182027"];
    [self config];
    self.hideNav = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getTrading:) name:NOTIFTRADING object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeHi:) name:NOTIFTICKER object:nil];
    _tikers = [UerManger shareUserManger].tikes;
    [self getHI];
}
- (void)changeHi:(NSNotification *)notfic{
    NSMutableArray *arr = notfic.userInfo[@"tiker"];
    _tikers = arr;
    [self getHI];
}
- (void)getHI{
    NSString *to = [NSString stringWithFormat:@"%@-%@",_marketModel.targetCoin,_marketModel.marketCoin];
    
    for (HomeModel *model in _tikers) {
        if ([model.pair isEqualToString:to]) {
            _tikeDetal = model;
            break;
        }
    }
    [self.collectionView reloadData];
}
- (void)getTrading:(NSNotification *)notfic{
    ExchangeModel *temMode = notfic.userInfo[@"trading"];
    _model = temMode;
    
    if (_exChangetype == 0) {
        NSSortDescriptor *ageSD = [NSSortDescriptor sortDescriptorWithKey:@"price" ascending:NO];//ascending:YES
        _best = [_model.buys sortedArrayUsingDescriptors:@[ageSD]][0];
    }
    if (_exChangetype == 1) {
        NSSortDescriptor *ageSD = [NSSortDescriptor sortDescriptorWithKey:@"price" ascending:YES];//ascending:YES
        _best = [_model.sells sortedArrayUsingDescriptors:@[ageSD]][0];
    }

    [self.collectionView reloadData];
    _select = nil;
}
- (void)setModel:(ExchangeModel *)model{
    _model = model;
    if (_exChangetype == 0) {
        NSSortDescriptor *ageSD = [NSSortDescriptor sortDescriptorWithKey:@"price" ascending:NO];//ascending:YES
        _best = [_model.buys sortedArrayUsingDescriptors:@[ageSD]][0];
    }
    if (_exChangetype == 1) {
        NSSortDescriptor *ageSD = [NSSortDescriptor sortDescriptorWithKey:@"price" ascending:YES];//ascending:YES
        _best = [_model.sells sortedArrayUsingDescriptors:@[ageSD]][0];
    }
    _select = _best;
    [self.collectionView reloadData];
}
- (void)setMarketModel:(Markets *)marketModel{
    _marketModel = marketModel;
}
- (void)setExChangetype:(NSInteger)exChangetype{
    _exChangetype = exChangetype;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (_type == 0) {
        if (section == 0) {
            return 1;
        }else if (section == 1){
            return 1;
        }
    }else if (_type == 1){
        if (section == 0) {
            return 1;
        }else if (section == 1){
            return 0;
        }else if (section == 2){
            return _model.sells.count > 12?12:_model.sells.count;
        }else if (section == 3){
            return _model.buys.count > 12?12:_model.buys.count;;
        }
    }

    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_type == 0) {
        if (indexPath.section == 0) {
            return CGSizeMake(size_width, 262);
        }else if (indexPath.section == 1){
            return CGSizeMake(size_width, 250);
        }
    }else if (_type == 1){
        if (indexPath.section == 0) {
            return CGSizeMake(size_width, 262);
        }else if (indexPath.section == 1){
            return CGSizeZero;
        }else if (indexPath.section == 2){
            return CGSizeMake(size_width, 20);
        }else if (indexPath.section == 3){
            return CGSizeMake(size_width, 20);
        }
    }
    return CGSizeZero;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (_type == 0) {
        return 2;
    }else if (_type == 1){
        return 4;
    }
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
//    if (_type == 1) {
//        if (section == 1){
//            return CGSizeMake(size_width, 60);
//        }
//    }
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    if (_type == 0) {
        if (section == 1){
//            return CGSizeMake(size_width, 52);
            return CGSizeZero;
        }
    }else if (_type == 1){
        if (section == 1){
//            return CGSizeMake(size_width, 52);
            return CGSizeZero;
        }else if (section == 2){
//            return CGSizeZero;
            return CGSizeMake(size_width, 30);
        }else if (section == 3){
            return CGSizeMake(size_width, 40);
        }
    }
    return CGSizeZero;

}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    GlenWeakSelf;
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        if (indexPath.section == 1){
            GlenExchangeCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"GlenExchangeCollectionReusableView" forIndexPath:indexPath];
            return footer;
        }
    }if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section == 1){
            GlenExchangeKlineSelectReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenExchangeKlineSelectReusableView" forIndexPath:indexPath];
            footer.type = 2;
            footer.openAsset = ^(NSInteger index) {
                weakSelf.type = index;
                [weakSelf.collectionView reloadData];
            };
            return footer;
        }else if (indexPath.section == 2){
            GlenExchangeTabCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenExchangeTabCollectionReusableView" forIndexPath:indexPath];
            footer.type = 0;
            return footer;
        }else if (indexPath.section == 3){
            GlenExchangeRataCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenExchangeRataCollectionReusableView" forIndexPath:indexPath];
            footer.model = _tikeDetal;
            return footer;
        }
    }
    return nil;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GlenWeakSelf;
    if (indexPath.section == 0) {
        GlenBuyCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenBuyCollectionViewCell" forIndexPath:indexPath];
        [cell setType:_exChangetype market:_marketModel bestPrice:_best select:_select];
        cell.upData = ^(NSString *price, NSString *amount, NSString *max) {
            weakSelf.price = price;
            weakSelf.amount = amount;
            weakSelf.maxvalue = max;
        };
        return cell;
    }
    else if (indexPath.section == 1) {
        GlenExchangeKLineCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenExchangeKLineCollectionViewCell" forIndexPath:indexPath];
        cell.model = _model;
        return cell;
    }else if (indexPath.section == 2){
        GlenExchangeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenExchangeCollectionViewCell" forIndexPath:indexPath];
        cell.type = 0;
        if (indexPath.row < _model.sortsells.count) {
            cell.detail = _model.sortsells[indexPath.row];
        }
        return cell;
    }else if (indexPath.section == 3){
        GlenExchangeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenExchangeCollectionViewCell" forIndexPath:indexPath];
        cell.type = 1;
        if (indexPath.row < _model.buys.count) {
            cell.detail = _model.buys[indexPath.row];
        }
        return cell;
    }
    
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 2) {
        _select = _model.sells[indexPath.row];
        [collectionView reloadData];
    }
    if (indexPath.section == 3) {
        _select = _model.buys[indexPath.row];
        [collectionView reloadData];
        _select = nil;
    }
}
- (void)config{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    _collectionView.collectionViewLayout = flowLayout;
    _collectionView.delegate=self;
    _collectionView.dataSource=self;
    _collectionView.showsVerticalScrollIndicator = NO;
    
    
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenExchangeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenExchangeCollectionViewCell"];
    
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenBuyCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenBuyCollectionViewCell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenExchangeKLineCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenExchangeKLineCollectionViewCell"];
    
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenExchangeKlineSelectReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenExchangeKlineSelectReusableView"];
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenExchangeTabCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenExchangeTabCollectionReusableView"];
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenExchangeRataCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenExchangeRataCollectionReusableView"];
    
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenExchangeCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"GlenExchangeCollectionReusableView"];
}
- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)buy:(UIButton *)sender {
    _maxvalue = [_maxvalue stringByReplacingOccurrencesOfString:@"," withString:@""];
    if (_exChangetype == 0) {
        if (_price.floatValue == 0) {
            [[Toast makeUpText:[NSString stringWithFormat:@"%@%@",kLocalizedString(@"PENTER", @""),kLocalizedString(@"BUY_PRICE", @"")]] showWithType:ShortTime];
            return;
        }
        if (_amount.floatValue == 0) {
            [[Toast makeUpText:[NSString stringWithFormat:@"%@%@",kLocalizedString(@"PENTER", @""),kLocalizedString(@"BUY_AMOUNT", @"")]] showWithType:ShortTime];
            return;
        }
//        if (_amount.floatValue > _maxvalue.floatValue) {
//            [[Toast makeUpText:@"总价钱超了"] showWithType:ShortTime];
//            return;
//        }
    }else if (_exChangetype == 1) {
        if (_price.floatValue == 0) {
            [[Toast makeUpText:[NSString stringWithFormat:@"%@%@",kLocalizedString(@"PENTER", @""),kLocalizedString(@"SELL_PRICE", @"")]] showWithType:ShortTime];
            return;
        }
        if (_amount.floatValue == 0) {
            [[Toast makeUpText:[NSString stringWithFormat:@"%@%@",kLocalizedString(@"PENTER", @""),kLocalizedString(@"SELL_AMOUNT", @"")]] showWithType:ShortTime];
            return;
        }
        if (_amount.floatValue > _maxvalue.floatValue) {
            [[Toast makeUpText:@"卖出量超了"] showWithType:ShortTime];
            return;
        }
    }
    GlenWeakSelf;
    [self showHUBWith:@""];
    [[GlenHttpTool shareInstance] orderMakePair:[NSString stringWithFormat:@"%@-%@",_marketModel.targetCoin,_marketModel.marketCoin] type:_exChangetype price:_price amount:_amount Completion:^(NSString *succes) {
        [weakSelf hiddenHUB];
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
        [[Toast makeUpText:error] showWithType:ShortTime];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
