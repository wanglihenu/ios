//
//  GlenExchangeViewController.h
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBaseViewController.h"

@interface GlenExchangeViewController : GlenBaseViewController

@property (strong, nonatomic) Markets *marketModel;


@end
