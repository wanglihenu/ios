//
//  GlenBuyViewController.h
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBaseViewController.h"
#import "ExchangeModel.h"


@interface GlenBuyViewController : GlenBaseViewController

@property (assign, nonatomic) NSInteger exChangetype;//0:买 1:卖
@property (strong, nonatomic) Markets *marketModel;
@property (strong, nonatomic) ExchangeModel *model;

@end
