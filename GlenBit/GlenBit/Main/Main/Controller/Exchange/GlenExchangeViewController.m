//
//  GlenExchangeViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenExchangeViewController.h"
#import "GlenExchangHeadInfoCollectionReusableView.h"
#import "GlenExchangeTabCollectionReusableView.h"
#import "GlenExchangeRataCollectionReusableView.h"
#import "GlenExchangeCollectionViewCell.h"
#import "GlenExchangeCollectionReusableView.h"
#import "GlenExchangeTradeCollectionReusableView.h"
#import "GlenExchangeKLineCollectionViewCell.h"
#import "GlenExchangeKlineSelectReusableView.h"

#import "GlenBuyViewController.h"
#import "FLSocketManager2.h"
#import "ExchangeModel.h"
#import "HomeModel.h"
#import "EWebCollectionViewCell.h"
#import "KCollectionViewCell.h"

#import "ZXDataReformer.h"

@interface GlenExchangeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottom;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (strong, nonatomic) ExchangeModel *model;

@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (weak, nonatomic) IBOutlet UIButton *sellBtn;
@property (assign, nonatomic)  NSInteger selectInex;
@property (assign, nonatomic)  BOOL isCanUpdataWeb;

@property (strong, nonatomic)  NSMutableArray *klineArr;

@property (strong, nonatomic) NSArray *tikers;
@property (strong, nonatomic) HomeModel *tikeDetal;
@property (strong, nonatomic) GlenExchangeRataCollectionReusableView *kk;

@end

@implementation GlenExchangeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _isCanUpdataWeb = YES;
    _bottom.constant = TabBarHeight + SafeMargin;
    _top.constant = StatusBar_height + 60;
    [self config];
    _selectInex = 0;
    if (_marketModel) {
        
    }else{
        SortSecondMarkets * selectMarckt = [UerManger shareUserManger].markes.markes[0];
        _marketModel = selectMarckt.markes[0];
    }

    [self getSorct];
//    [self getKline];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeHi:) name:NOTIFTICKER object:nil];
    _tikers = [UerManger shareUserManger].tikes;
    [self getHI];
}
- (void)changeHi:(NSNotification *)notfic{
    NSMutableArray *arr = notfic.userInfo[@"tiker"];
    _tikers = arr;
    [self getHI];
}
- (void)getHI{
    NSString *to = [NSString stringWithFormat:@"%@-%@",_marketModel.targetCoin,_marketModel.marketCoin];
    
    for (HomeModel *model in _tikers) {
        if ([model.pair isEqualToString:to]) {
            _tikeDetal = model;
            break;
        }
    }
    if (_kk) {
        _kk.model = _tikeDetal;
    }
}
- (void)setMarketModel:(Markets *)marketModel{
    _marketModel = marketModel;
    _isCanUpdataWeb = YES;
    [self getSorct];
}
- (NSMutableArray *)klineArr{
    if (!_klineArr) {
        _klineArr = [NSMutableArray new];
    }
    return _klineArr;
}
- (void)getKline{
    GlenWeakSelf;
    [self showHUBWith:@""];
    [[GlenHttpTool shareInstance] kLineType:0 pair:[NSString stringWithFormat:@"%@-%@",_marketModel.targetCoin,_marketModel.marketCoin] Completion:^(NSArray *result) {
        weakSelf.isCanUpdataWeb = YES;
        
        NSArray *transformedDataArray =  [[ZXDataReformer sharedInstance] transformDataWithOriginalDataArray:result currentRequestType:@"M1"];
        
        [weakSelf.klineArr addObjectsFromArray:transformedDataArray];
        [weakSelf.collectionView reloadData];
        [weakSelf hiddenHUB];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
    }];
}
- (void)upDataLoca{
    [_buyBtn setTitle:kLocalizedString(@"BUY", @"") forState:UIControlStateNormal];
    [_sellBtn setTitle:kLocalizedString(@"SELL", @"") forState:UIControlStateNormal];
    self.title = kLocalizedString(@"TRADE", @"交易");
    [_collectionView reloadData];
}
- (void)getSorct{
    GlenWeakSelf;
//    [[FLSocketManager shareManager] fl_close:nil];
    NSString *url = [NSString stringWithFormat:@"wss://stream-main.glenbit.com/%@-%@",_marketModel.targetCoin,_marketModel.marketCoin];

    [[FLSocketManager2 shareManager] fl_open:url connect:^{
        NSLog(@"成功连接");
    } receive:^(id message, FLSocketReceiveType type) {
        [weakSelf hiddenHUB];
        NSData *jsonData = nil;
        if ([message isKindOfClass:[NSString class]]) {
            jsonData = [(NSString *)message dataUsingEncoding : NSUTF8StringEncoding];
        }
        weakSelf.model = [ExchangeModel yy_modelWithJSON:message];
        if (weakSelf.model.buys.count == 11){
            NSLog(@"");
        }
        if (weakSelf.model.buys.count && weakSelf.model.sells.count && weakSelf.model.histories.count) {
//            [weakSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:1]];
//            [weakSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:2]];
//            [weakSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:3]];
            [weakSelf.collectionView reloadData];
        }
        if (weakSelf.model) {
            NSNotification *notification =[NSNotification notificationWithName:NOTIFTRADING object:nil userInfo:@{@"trading":weakSelf.model}];;
            [[NSNotificationCenter defaultCenter] postNotification:notification];
        }
        
    } failure:^(NSError *error) {
        NSLog(@"连接失败");
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }else if (section == 1){
        return 1;
    }else if (section == 2){
        return _model.sells.count>12?12:_model.sells.count;
    }else if (section == 3){
        return _model.buys.count>12?12:_model.buys.count;
    }else if (section == 4){
        return _model.histories.count;
    }
    return 0;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 5;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        return CGSizeMake(size_width, size_width);
    }else if (indexPath.section == 2){
        return CGSizeMake(size_width, 20);
    }else if (indexPath.section == 3){
        return CGSizeMake(size_width, 20);
    }else if (indexPath.section == 4){
        return CGSizeMake(size_width, 20);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if (section == 3){
        return CGSizeMake(size_width, 100);
    }else if (section == 1){
//        return CGSizeMake(size_width, 60);
        return CGSizeZero;
    }
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGSizeMake(size_width, 191);
    }else if (section == 1){
        return CGSizeZero;
    }else if (section == 2){
        return CGSizeMake(size_width, 30);
    }else if (section == 3){
        return CGSizeMake(size_width, 40);
    }else if (section == 4){
        return CGSizeMake(size_width, 30);
    }
    else{
        return CGSizeZero;
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    GlenWeakSelf;
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        if (indexPath.section == 3){
            GlenExchangeTradeCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"GlenExchangeTradeCollectionReusableView" forIndexPath:indexPath];
            [footer setT:kLocalizedString(@"trade_history", @"")];
            return footer;
        }else if (indexPath.section == 1){
            GlenExchangeCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"GlenExchangeCollectionReusableView" forIndexPath:indexPath];
            return footer;
        }
    }if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section == 0) {
            GlenExchangHeadInfoCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenExchangHeadInfoCollectionReusableView" forIndexPath:indexPath];
            footer.selectModel = _marketModel;
            footer.select = ^(Markets *markes) {
                weakSelf.marketModel = markes;
                weakSelf.isCanUpdataWeb = YES;
                [weakSelf getSorct];
            };
            return footer;
        }else if (indexPath.section == 1){
            GlenExchangeKlineSelectReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenExchangeKlineSelectReusableView" forIndexPath:indexPath];
            footer.openAsset = ^(NSInteger index) {
                weakSelf.selectInex = index;
                [weakSelf.collectionView reloadData];
            };
            return footer;
        }else if (indexPath.section == 2){
            GlenExchangeTabCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenExchangeTabCollectionReusableView" forIndexPath:indexPath];
            footer.type = 0;
            return footer;
        }else if (indexPath.section == 3){
            GlenExchangeRataCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenExchangeRataCollectionReusableView" forIndexPath:indexPath];
            footer.model = _tikeDetal;
            _kk = footer;
            return footer;
        }else if (indexPath.section == 4){
            GlenExchangeTabCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenExchangeTabCollectionReusableView" forIndexPath:indexPath];
            footer.type = 1;
            return footer;
        }
    }
    return nil;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        if (_selectInex == 0) {
            EWebCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EWebCollectionViewCell" forIndexPath:indexPath];
            if (_isCanUpdataWeb) {

                [cell setWebUrl:[NSString stringWithFormat:@"http://192.168.0.117:3001?id=%@-%@",_marketModel.targetCoin,_marketModel.marketCoin] allCor:YES];
                _isCanUpdataWeb = NO;
            }
            return cell;
            
//                        KCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"KCollectionViewCell" forIndexPath:indexPath];
//                        if (_isCanUpdataWeb) {
//
//                            [cell reloadKLineData:_klineArr];
//                            _isCanUpdataWeb = NO;
//                        }
//                        return cell;
        }else{
            GlenExchangeKLineCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenExchangeKLineCollectionViewCell" forIndexPath:indexPath];
            cell.model = _model;
            return cell;
        }
        
    }else if (indexPath.section == 2){
        GlenExchangeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenExchangeCollectionViewCell" forIndexPath:indexPath];
        cell.type = 0;
        if (indexPath.row < _model.sortsells.count) {
            cell.detail = _model.sortsells[indexPath.row];
        }
        
        return cell;
    }else if (indexPath.section == 3){
        GlenExchangeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenExchangeCollectionViewCell" forIndexPath:indexPath];
        cell.type = 1;
        if (indexPath.row < _model.buys.count) {
            cell.detail = _model.buys[indexPath.row];
        }
    
        return cell;
    }else if (indexPath.section == 4){
        GlenExchangeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenExchangeCollectionViewCell" forIndexPath:indexPath];
        cell.type = 2;
        if (indexPath.row < _model.sorthistories.count) {
             cell.detail = _model.sorthistories[indexPath.row];
        }
        
       
        return cell;
    }
    
    return nil;
}
- (void)config{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    _collectionView.collectionViewLayout = flowLayout;
    _collectionView.delegate=self;
    _collectionView.dataSource=self;
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.showsVerticalScrollIndicator = NO;
    
    
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenExchangeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenExchangeCollectionViewCell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenExchangeKLineCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenExchangeKLineCollectionViewCell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"EWebCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"EWebCollectionViewCell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"KCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"KCollectionViewCell"];
    
    
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenExchangHeadInfoCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenExchangHeadInfoCollectionReusableView"];
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenExchangeKlineSelectReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenExchangeKlineSelectReusableView"];
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenExchangeTabCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenExchangeTabCollectionReusableView"];
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenExchangeRataCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenExchangeRataCollectionReusableView"];
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenExchangeTradeCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"GlenExchangeTradeCollectionReusableView"];
    
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenExchangeCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"GlenExchangeCollectionReusableView"];
}
- (IBAction)buy:(UIButton *)sender {
    GlenWeakSelf;
    [self showHUBWith:@""];
    [[GlenHttpTool shareInstance] walletBalanceCompletion:^(NSArray *result) {
        for (BalanceModel *model in result) {
            for (Coins *temp in [UerManger shareUserManger].coins) {
                if ([model.coin isEqualToString:temp._id]) {
                    temp.balance = model;
                    break;
                }
            }
        }
        
        NSSortDescriptor *ageSD = [NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES];//ascending:YES
        
        [UerManger shareUserManger].coins = [[[UerManger shareUserManger].coins sortedArrayUsingDescriptors:@[ageSD]] mutableCopy];
        [weakSelf hiddenHUB];
        GlenBuyViewController *buy = [GlenBuyViewController new];
        buy.exChangetype = sender.tag - 10;
        buy.marketModel = weakSelf.marketModel;
        buy.model = weakSelf.model;
        [weakSelf presentViewController:buy animated:YES completion:nil];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
        [[Toast makeUpText:error] showWithType:ShortTime];
    }];


}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
