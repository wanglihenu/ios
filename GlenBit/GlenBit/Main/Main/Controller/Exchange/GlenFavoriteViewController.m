//
//  GlenFavoriteViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/10.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenFavoriteViewController.h"
#import "GlenMainCollectionViewCell.h"

@interface GlenFavoriteViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottom;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSArray *tikers;
@property (weak, nonatomic) IBOutlet UILabel *titlLab;

@end

@implementation GlenFavoriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hideNav = YES;
    _titlLab.text = kLocalizedString(@"Favorite", @"");
    _top.constant = StatusBar_height;
    _bottom.constant = SafeMargin;
    [self config];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeHi:) name:NOTIFTICKER object:nil];
    _tikers = [UerManger shareUserManger].tikes;
}
- (void)changeHi:(NSNotification *)notfic{
    NSMutableArray *arr = notfic.userInfo[@"tiker"];
    _tikers = arr;
    [self.collectionView reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _tikers.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(size_width, 66);
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        GlenMainCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenMainCollectionViewCell" forIndexPath:indexPath];
        cell.homeModel = _tikers[indexPath.row];
        return cell;
    }
    return nil;
}
- (void)config{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    _collectionView.collectionViewLayout = flowLayout;
    _collectionView.delegate=self;
    _collectionView.dataSource=self;
    _collectionView.showsVerticalScrollIndicator = NO;
    
    
    [_collectionView registerNib:[UINib nibWithNibName:@"GlenMainCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenMainCollectionViewCell"];
}
- (IBAction)close:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
