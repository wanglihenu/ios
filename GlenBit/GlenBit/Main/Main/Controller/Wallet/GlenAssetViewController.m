//
//  GlenAssetportfolioViewController.m
//  GlenBit
//
//  Created by Lee on 2019/1/5.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenAssetViewController.h"
#import "GlenAssetTotalCollectionViewCell.h"
#import "GlenAssetPendingCollectionViewCell.h"
#import "GlenMainCollectionViewCell.h"
#import "GlenAssetMenuCollectionReusableView.h"
#import "GlenAssetPendingCollectionReusableView.h"
#import "GlenSelcetTableViewCell.h"

#import "GlenAssetDepositView.h"
#import "GlenWithDrawView.h"
#import "GlenGoogleOneViewController.h"
#import "NODATACollectionViewCell.h"
#import "YBPopupMenu.h"

@interface GlenAssetViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YBPopupMenuDelegate>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) UITableView *tableview;
@property (strong, nonatomic) NSIndexPath *selectIndex;
@property (strong, nonatomic) NSIndexPath *selectCellIndex;
@property (strong, nonatomic) NSIndexPath *dczSelectIndex;
@property (strong, nonatomic) NSMutableArray *balanceArr;
@property (strong, nonatomic) NSMutableArray *daiquerenArr;
@property (strong, nonatomic) NSMutableArray *chongzhiArr;
@property (strong, nonatomic) NSMutableArray *tixianArr;
@property (strong, nonatomic) NSMutableArray *searchRelutArr;
@property (strong, nonatomic) NSMutableArray *dczRelutArr;
@property (strong, nonatomic) NSString *search;
@property (assign, nonatomic) BOOL issearch;
@property (assign, nonatomic) BOOL isDCZ;
@end

@implementation GlenAssetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.collectionView];
    
//    NSString *gg = [GlenTools rateFrom:@"BTC" to:@"USD" lastRate:nil];
//    NSLog(@"---%@",gg);
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self getMarkPrice];
}
- (void)upDataLoca{
    self.title = kLocalizedString(@"WALLET", @"钱包");
}
- (void)getMarkPrice{
    GlenWeakSelf;
    [self showHUBWith:@""];
    [[GlenHttpTool shareInstance] marketPricesCompletion:^(NSArray *coins) {
        NSMutableArray *a = [NSMutableArray new];
        [a addObjectsFromArray:coins];
        [a enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            marketPrices *p = obj;
            
            if (p.price.floatValue == 1) {
                *stop = YES;
                
                if (*stop == YES) {
                    [a removeObject:p];
                }
                *stop = NO;
            }
        }];
        [UerManger shareUserManger].marke_price = a;
        [weakSelf getCoins];
        
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
        [[Toast makeUpText:error] showWithType:ShortTime];
    }];
}
- (void)getCoins{
    GlenWeakSelf;
    [[GlenHttpTool shareInstance]coinsCompletion:^(NSArray *coins) {
        [UerManger shareUserManger].coins = coins;
        NSSortDescriptor *ageSD = [NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES];//ascending:YES
        
        [UerManger shareUserManger].coins = [[[UerManger shareUserManger].coins sortedArrayUsingDescriptors:@[ageSD]] mutableCopy];
        [weakSelf getOpen];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
        [[Toast makeUpText:error] showWithType:ShortTime];
    }];
}
- (void)getOpen{
    GlenWeakSelf;
    [self.balanceArr removeAllObjects];
    [[GlenHttpTool shareInstance] walletBalanceCompletion:^(NSArray *result) {
        for (BalanceModel *model in result) {
            for (Coins *temp in [UerManger shareUserManger].coins) {
                if ([model.coin isEqualToString:temp._id]) {
                    temp.balance = model;
                    break;
                }
            }
        }
        NSSortDescriptor *ageSD = [NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES];//ascending:YES
        
        [UerManger shareUserManger].coins = [[[UerManger shareUserManger].coins sortedArrayUsingDescriptors:@[ageSD]] mutableCopy];
        [weakSelf.balanceArr addObjectsFromArray:result];
        [weakSelf getchongzhi];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
        [[Toast makeUpText:error] showWithType:ShortTime];
    }];
}
- (void)getchongzhi{
    GlenWeakSelf;
    [self.daiquerenArr removeAllObjects];
    [self.chongzhiArr removeAllObjects];
    [[GlenHttpTool shareInstance] depositHistoryCompletion:^(NSArray *result) {
        [result enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ChongzhiModel *temModel = (ChongzhiModel *)obj;
            if (NULLString(temModel.updatedAt)) {
                [weakSelf.daiquerenArr addObject:temModel];
            }
        }];
        [weakSelf.chongzhiArr addObjectsFromArray:result];
        [weakSelf getTixian];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
        [[Toast makeUpText:error] showWithType:ShortTime];
    }];
}
- (void)getTixian{
    GlenWeakSelf;
    [self.tixianArr removeAllObjects];
    [[GlenHttpTool shareInstance] withdrawHistoryCompletion:^(NSArray *result) {
        [weakSelf.tixianArr addObjectsFromArray:result];
        [weakSelf hiddenHUB];
        [weakSelf.collectionView reloadData];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
        [[Toast makeUpText:error] showWithType:ShortTime];
    }];
}
- (void)getSearchArr:(NSString *)search{
    
    if (_isDCZ) {
        [self.dczRelutArr removeAllObjects];
        for (ChongzhiModel *coin in _daiquerenArr) {
            if ([coin.coin.lowercaseString containsString:search.lowercaseString]) {
                [_dczRelutArr addObject:coin];
            }
        }
    }else{
        [self.searchRelutArr removeAllObjects];
        if (search.length) {
            if (_currentIndex == 0) {
                for (Coins *coin in [UerManger shareUserManger].coins) {
                    if ([coin._id.lowercaseString containsString:search.lowercaseString]||[coin.name.lowercaseString containsString:search.lowercaseString]) {
                        [_searchRelutArr addObject:coin];
                    }
                }
            }else if (_currentIndex == 1) {
                for (ChongzhiModel *coin in _chongzhiArr) {
                    if ([coin.coin.lowercaseString containsString:search.lowercaseString]) {
                        [_searchRelutArr addObject:coin];
                    }
                }
            }else if (_currentIndex == 2) {
                for (TixianModel *coin in _chongzhiArr) {
                    if ([coin.coin.lowercaseString containsString:search.lowercaseString]) {
                        [_searchRelutArr addObject:coin];
                    }
                }
            }
        }
    }
    
    [_collectionView reloadSections:[NSIndexSet indexSetWithIndex:2]];
}
- (NSMutableArray *)searchRelutArr{
    if (!_searchRelutArr) {
        _searchRelutArr = [NSMutableArray new];
    }
    return _searchRelutArr;
}
- (NSMutableArray *)balanceArr{
    if (!_balanceArr) {
        _balanceArr = [NSMutableArray new];
    }
    return _balanceArr;
}
- (NSMutableArray *)daiquerenArr{
    if (!_daiquerenArr) {
        _daiquerenArr = [NSMutableArray new];
    }
    return _daiquerenArr;
}
- (NSMutableArray *)chongzhiArr{
    if (!_chongzhiArr) {
        _chongzhiArr = [NSMutableArray new];
    }
    return _chongzhiArr;
}
- (NSMutableArray *)tixianArr{
    if (!_tixianArr) {
        _tixianArr = [NSMutableArray new];
    }
    return _tixianArr;
}
- (NSMutableArray *)dczRelutArr{
    if (!_dczRelutArr) {
        _dczRelutArr = [NSMutableArray new];
    }
    return _dczRelutArr;
}
- (void)setCurrentIndex:(NSInteger)currentIndex{
    _currentIndex = currentIndex;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        if (_isDCZ) {
            return _dczRelutArr.count?:1;
        }
        return _daiquerenArr.count?:1;
    }else if (section == 2){
        switch (_currentIndex) {
            case 0:
                if (_issearch) {
                    return _searchRelutArr.count;
                }
                return [UerManger shareUserManger].coins.count;
                break;
            case 1:
                if (_issearch) {
                    return _searchRelutArr.count?:1;
                }
                return _chongzhiArr.count?:1;
                break;
            case 2:
                if (_issearch) {
                    return _searchRelutArr.count?:1;
                }
                return _tixianArr.count?:1;
                break;
                
            default:
                break;
        }
    }
    return 0;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 3;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake(size_width, 211);
    }else if (indexPath.section == 1){
        return CGSizeMake(size_width, 66);
    }else if (indexPath.section == 2){
        if ([indexPath compare:_selectCellIndex] == NSOrderedSame) {
            if (_currentIndex == 0) {
                return CGSizeMake(size_width, 132);
            }else if (_currentIndex == 1){
                return CGSizeMake(size_width, 230);
            }else if (_currentIndex == 2){
                return CGSizeMake(size_width, 230);
            }
            
        }
        return CGSizeMake(size_width, 66);
    }
    return CGSizeZero;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGSizeZero;
    }else if (section == 1){
        return CGSizeMake(size_width, 84);
    }else{
        return CGSizeZero;
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if (section == 1){
        return CGSizeMake(size_width, 220);
    }else{
        return CGSizeZero;
    }
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    GlenWeakSelf;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section == 1) {
            GlenAssetPendingCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenAssetPendingCollectionReusableView" forIndexPath:indexPath];
            [footer setCoin:_dczSelectIndex.row == 0?nil:[UerManger shareUserManger].coins[_dczSelectIndex.row - 1] select:_dczSelectIndex];
            footer.select = ^(UIView *view) {
                weakSelf.isDCZ = YES;
                CGRect absoluteRect = [view convertRect:view.bounds toView:weakSelf.collectionView];
                
                [weakSelf.collectionView setContentOffset:CGPointMake(0, absoluteRect.origin.y)];
                
                
                [YBPopupMenu showRelyOnView:view titles:[UerManger shareUserManger].coins icons:nil menuWidth:size_width otherSettings:^(YBPopupMenu *popupMenu) {
                    popupMenu.dismissOnSelected = NO;
                    //                    popupMenu.isShowShadow = YES;
                    popupMenu.delegate = self;
                    popupMenu.itemHeight = 64;
                    popupMenu.minSpace = 0;
                    popupMenu.arrowWidth = popupMenu.arrowHeight = 0;
                    weakSelf.tableview = popupMenu.tableView;
                }];
            };
            return footer;
        }
        if (indexPath.section == 2) {
            GlenAssetMenuCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"GlenAssetMenuCollectionReusableView" forIndexPath:indexPath];
            [footer setCoin:_selectIndex.row == 0?nil:[UerManger shareUserManger].coins[_selectIndex.row - 1] select:_selectIndex seacrh:_search];
            footer.changeCurrent = ^(NSInteger index) {
                weakSelf.search = nil;
                weakSelf.issearch = NO;
                weakSelf.selectIndex = nil;
                weakSelf.currentIndex = index;
                [weakSelf.collectionView reloadData];
            };
            footer.search = ^(NSString *search) {
                weakSelf.selectIndex = nil;
                weakSelf.search = search;
                if (search.length == 0) {
                    weakSelf.issearch = NO;
                }else{
                    weakSelf.issearch = YES;
                }
                [weakSelf getSearchArr:search];
            };
            footer.currentIndex = _currentIndex;
            footer.select = ^(UIView *view) {
                weakSelf.isDCZ = NO;
                                CGRect absoluteRect = [view convertRect:view.bounds toView:weakSelf.collectionView];
                
                                [weakSelf.collectionView setContentOffset:CGPointMake(0, absoluteRect.origin.y)];
                
        
                                [YBPopupMenu showRelyOnView:view titles:[UerManger shareUserManger].coins icons:nil menuWidth:size_width otherSettings:^(YBPopupMenu *popupMenu) {
                                    popupMenu.dismissOnSelected = NO;
                                    //                    popupMenu.isShowShadow = YES;
                                    popupMenu.delegate = self;
                                    popupMenu.itemHeight = 64;
                                    popupMenu.minSpace = 0;
                                    popupMenu.arrowWidth = popupMenu.arrowHeight = 0;
weakSelf.tableview = popupMenu.tableView;
                                }];
            };
            return footer;
        }
    }
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        if (indexPath.section == 1) {
            GlenAssetMenuCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"GlenAssetMenuCollectionReusableView" forIndexPath:indexPath];
            [footer setCoin:_selectIndex.row == 0?nil:[UerManger shareUserManger].coins[_selectIndex.row - 1] select:_selectIndex seacrh:_search];
            footer.changeCurrent = ^(NSInteger index) {
                weakSelf.search = nil;
                weakSelf.issearch = NO;
                weakSelf.selectIndex = nil;
                weakSelf.currentIndex = index;
                [weakSelf.collectionView reloadData];
            };
            footer.search = ^(NSString *search) {
                weakSelf.selectIndex = nil;
                weakSelf.search = search;
                if (search.length == 0) {
                    weakSelf.issearch = NO;
                }else{
                    weakSelf.issearch = YES;
                }
                [weakSelf getSearchArr:search];
            };
            footer.currentIndex = _currentIndex;
            footer.select = ^(UIView *view) {
                weakSelf.isDCZ = NO;
                CGRect absoluteRect = [view convertRect:view.bounds toView:weakSelf.collectionView];
                
                [weakSelf.collectionView setContentOffset:CGPointMake(0, absoluteRect.origin.y)];
                
                
                [YBPopupMenu showRelyOnView:view titles:[UerManger shareUserManger].coins icons:nil menuWidth:size_width otherSettings:^(YBPopupMenu *popupMenu) {
                    popupMenu.dismissOnSelected = NO;
                    //                    popupMenu.isShowShadow = YES;
                    popupMenu.delegate = self;
                    popupMenu.itemHeight = 64;
                    popupMenu.minSpace = 0;
                    popupMenu.arrowWidth = popupMenu.arrowHeight = 0;
                    weakSelf.tableview = popupMenu.tableView;
                }];
            };
            return footer;
        }
    }
    return nil;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GlenWeakSelf;
    if (indexPath.section == 0) {
        GlenAssetTotalCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenAssetTotalCollectionViewCell" forIndexPath:indexPath];
        cell.balanece = _balanceArr;
        return cell;
    }else if (indexPath.section == 1){
        if ((_isDCZ && _dczRelutArr.count) || (!_isDCZ && _daiquerenArr.count)) {
            GlenAssetPendingCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenAssetPendingCollectionViewCell" forIndexPath:indexPath];
            cell.chongzhi = _isDCZ?_dczRelutArr[indexPath.row]:self.daiquerenArr[indexPath.row];
            return cell;
        }else{
            NODATACollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NODATACollectionViewCell" forIndexPath:indexPath];
            return cell;
        }
        
    }
    else if (indexPath.section == 2){
        
        if (_currentIndex == 0) {
            GlenMainCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenMainCollectionViewCell" forIndexPath:indexPath];
            cell.type = 1;
            cell.balanceModel = _issearch?_searchRelutArr[indexPath.row]:[UerManger shareUserManger].coins[indexPath.row];
            cell.openAsset = ^(NSInteger index) {
                if (index == 0) {
                    Coins *con = [UerManger shareUserManger].coins[indexPath.row];
                    if (!con.canDeposit) {
                        [[Toast makeUpText:@"暂不支持充值"] showWithType:ShortTime];
                        return ;
                    }
                    [weakSelf showHUBWith:@""];
                    [[GlenHttpTool shareInstance] addressCoin:con._id Completion:^(NSString *succes) {
                        NSLog(@"---%@",succes);
                        [weakSelf hiddenHUB];
                        GlenAssetDepositView *deppsit = [[GlenAssetDepositView alloc]initWithFrame:CGRectMake(0, 0, size_width, size_height)];
                        deppsit.coin = con;
                        deppsit.addressStr = succes;
                        deppsit.closeSelf = ^{
                            [weakSelf dismissSemiModalView];
                        };
                        [weakSelf presentSemiView:deppsit withOptions:@{KNSemiModalOptionKeys.pushParentBack : @(NO),KNSemiModalOptionKeys.parentAlpha : @(0.8)}];
                    } fail:^(NSString *error) {
                        [weakSelf hiddenHUB];
                        [[Toast makeUpText:error] showWithType:ShortTime];
                    }];
                    
                    
                    
                }
                if (index == 1) {
                    
                    if ([UerManger shareUserManger].userModel.isTwoStepEnabled) {
                        
                        Coins *con = [UerManger shareUserManger].coins[indexPath.row];
                        if (!con.canWithdraw) {
                            [[Toast makeUpText:@"暂不支持提现"] showWithType:ShortTime];
                            return ;
                        }
                        [weakSelf showHUBWith:@""];
                        
                        
                        [[GlenHttpTool shareInstance] tryWithdrawCoin:con._id Completion:^(TixianInfoModel *succes) {
                            [weakSelf hiddenHUB];
                            GlenWithDrawView *withDraw = [[GlenWithDrawView alloc]initWithFrame:CGRectMake(0, 0, size_width, size_height)];
                            withDraw.coin = con._id;
                            withDraw.model = succes;
                            withDraw.closeWithDraw = ^{
                                [weakSelf dismissSemiModalView];
                            };
                            [weakSelf presentSemiView:withDraw withOptions:@{KNSemiModalOptionKeys.pushParentBack : @(NO),KNSemiModalOptionKeys.parentAlpha : @(0.8)}];
                        } fail:^(NSString *error) {
                            [weakSelf hiddenHUB];
                            [[Toast makeUpText:error] showWithType:ShortTime];
                        }];
                    }else{
                        GlenGoogleOneViewController *reset = [GlenGoogleOneViewController new];
                        [weakSelf.navigationController pushViewController:reset animated:YES];
                    }
                    
                    
                    
                }
            };
            return cell;
        }else if (_currentIndex == 1){
            if ((_chongzhiArr.count && !_issearch)||(_searchRelutArr.count && _issearch)) {
                GlenAssetPendingCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenAssetPendingCollectionViewCell" forIndexPath:indexPath];
                cell.chongzhi = _issearch?_searchRelutArr[indexPath.row]:_chongzhiArr[indexPath.row];
                cell.type = 1;
                return cell;
            }else{
                NODATACollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NODATACollectionViewCell" forIndexPath:indexPath];
                return cell;
            }
        }else if (_currentIndex == 2){
            if ((_tixianArr.count && !_issearch)||(_searchRelutArr.count && _issearch)) {
                GlenAssetPendingCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GlenAssetPendingCollectionViewCell" forIndexPath:indexPath];
                cell.tixian = _issearch?_searchRelutArr[indexPath.row]:_tixianArr[indexPath.row];
                cell.type = 1;
                return cell;
            }else{
                NODATACollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NODATACollectionViewCell" forIndexPath:indexPath];
                return cell;
            }
        }
    }
    
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 2 && _currentIndex == 1) {
        if (_selectCellIndex == indexPath) {
            return;
        }
        _selectCellIndex = indexPath;
        ChongzhiModel *cz = _chongzhiArr[indexPath.row];
        GlenWeakSelf;
        [weakSelf showHUBWith:@""];
        [[GlenHttpTool shareInstance] addressCoin:cz.coin Completion:^(NSString *succes) {
            NSLog(@"---%@",succes);
            cz.DepositAddress = succes;
            [weakSelf hiddenHUB];
            [weakSelf.collectionView reloadData];
        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
            [[Toast makeUpText:error] showWithType:ShortTime];
        }];
    }else if (indexPath.section == 2 && _currentIndex == 0){
        if (_selectCellIndex == indexPath) {
            return;
        }
        _selectCellIndex = indexPath;
        [self.collectionView reloadData];
    }
}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        // 布局对象
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.minimumLineSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, StatusBar_height + 60, size_width, size_height - 60 - StatusBar_height) collectionViewLayout:flowLayout];
        _collectionView.delegate=self;
        _collectionView.dataSource=self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenMainCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenMainCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"NODATACollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"NODATACollectionViewCell"];
        
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenAssetTotalCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenAssetTotalCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenAssetPendingCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"GlenAssetPendingCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenAssetMenuCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"GlenAssetMenuCollectionReusableView"];
        [_collectionView registerNib:[UINib nibWithNibName:@"GlenAssetPendingCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GlenAssetPendingCollectionReusableView"];
    }
    return _collectionView;
}
#pragma mark - YBPopupMenuDelegate
- (void)ybPopupMenu:(YBPopupMenu *)ybPopupMenu didSelectedAtIndex:(NSInteger)index
{
    if (_isDCZ) {
        _dczSelectIndex = [NSIndexPath indexPathForRow:index inSection:0];
        if (index == 0) {
            _issearch = NO;
            _dczSelectIndex = nil;
            [self.collectionView reloadData];
            [self.tableview reloadData];
            [ybPopupMenu dismiss];
            return;
        }
    }else{
        _selectIndex = [NSIndexPath indexPathForRow:index inSection:0];
        if (index == 0) {
            _issearch = NO;
            [self.collectionView reloadData];
            [self.tableview reloadData];
            [ybPopupMenu dismiss];
            return;
        }
        _search = @"";
        _issearch = YES;
    }
    [self.collectionView reloadData];
    //推荐回调
    Coins *c = [UerManger shareUserManger].coins[index - 1];
    
    [self getSearchArr:c._id];
    [self.tableview reloadData];
    [ybPopupMenu dismiss];
}
- (UITableViewCell *)ybPopupMenu:(YBPopupMenu *)ybPopupMenu cellForRowAtIndex:(NSInteger)index
{
    static NSString * identifier = @"GlenSelcetTableViewCell";
    GlenSelcetTableViewCell * cell = [ybPopupMenu.tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"GlenSelcetTableViewCell" owner:self options:nil] firstObject];
    }
    
    if (index == 0) {
        if (_isDCZ) {
            Coins *co = [Coins new];
            co._id = kLocalizedString(@"ALL", @"");
            [cell setTitle:co selectIndex:_dczSelectIndex current:[NSIndexPath indexPathForRow:index inSection:0]];
            return cell;
        }else{
            Coins *co = [Coins new];
            co._id = kLocalizedString(@"ALL", @"");
            [cell setTitle:co selectIndex:_selectIndex current:[NSIndexPath indexPathForRow:index inSection:0]];
            return cell;
        }
        
    }
    if (_isDCZ) {
        [cell setTitle:[UerManger shareUserManger].coins[index - 1] selectIndex:_dczSelectIndex current:[NSIndexPath indexPathForRow:index inSection:0]];
        return cell;
    }else{
        [cell setTitle:[UerManger shareUserManger].coins[index - 1] selectIndex:_selectIndex current:[NSIndexPath indexPathForRow:index inSection:0]];
        return cell;
    }
    return nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
