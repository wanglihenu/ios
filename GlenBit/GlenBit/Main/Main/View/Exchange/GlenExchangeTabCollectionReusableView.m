//
//  GlenExchangeTabCollectionReusableView.m
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenExchangeTabCollectionReusableView.h"

@interface GlenExchangeTabCollectionReusableView ()
@property (weak, nonatomic) IBOutlet UILabel *lab1;
@property (weak, nonatomic) IBOutlet UILabel *lab2;
@property (weak, nonatomic) IBOutlet UILabel *lab3;

@end

@implementation GlenExchangeTabCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setType:(NSInteger)type{
    _type = type;
    switch (type) {
        case 0:
        {
            _lab1.text = kLocalizedString(@"PRICE", @"");
            _lab2.text = kLocalizedString(@"AMOUNT", @"");
            _lab3.text = kLocalizedString(@"TOTAL_PRICE", @"交易量");
        }
            break;
        case 1:
        {
            _lab1.text = kLocalizedString(@"TIME", @"");
            _lab2.text = kLocalizedString(@"PRICE", @"");
            _lab3.text = kLocalizedString(@"AMOUNT", @"");
        }
            break;
            
        default:
            break;
    }
}
@end
