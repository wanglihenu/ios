//
//  EWebCollectionViewCell.m
//  JBT
//
//  Created by Lee on 2018/4/3.
//  Copyright © 2018年 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "EWebCollectionViewCell.h"
#import <WebKit/WebKit.h>
#import "GlenBaseViewController.h"

@interface EWebCollectionViewCell ()<WKNavigationDelegate>
@property (weak, nonatomic) IBOutlet UIView *backView;
@property(strong,nonatomic) WKWebView *wkWebView;
@property(strong,nonatomic) NSString *function;
@property(strong,nonatomic) NSString *js;
@property(strong,nonatomic) UIView *mackView;
@property(strong,nonatomic) GlenBaseViewController *base;
@end

@implementation EWebCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setEcharts];
    _base = (GlenBaseViewController *)[self viewController];
}
- (void)setEcharts{
    self.wkWebView = [WKWebView new];
    self.wkWebView.frame = CGRectMake(0, 0, size_width, self.height);
    self.wkWebView.backgroundColor = [UIColor colorWithHexString:@"#353B41"];
    self.wkWebView.scrollView.backgroundColor = [UIColor colorWithHexString:@"#353B41"];
    [self.wkWebView setOpaque:NO];
    self.wkWebView.scrollView.scrollEnabled = NO;
    self.wkWebView.navigationDelegate = self;
    [self.backView addSubview: self.wkWebView];
    self.mackView = [[UIView alloc]initWithFrame:self.frame];
    self.mackView.backgroundColor = [UIColor colorWithHexString:@"#353B41"];
//    [self.backView addSubview:self.mackView];
//    [self setWebUrl:@"http://192.168.0.117:3001" allCor:YES];


//    NSString *bundlePath = [[NSBundle mainBundle] bundlePath];
//    NSString *basePath = [NSString stringWithFormat:@"%@/GlenBit/charting_library-master",bundlePath];
//    NSURL *baseUrl = [NSURL fileURLWithPath:basePath isDirectory:YES];
//    NSString *indexPath = [NSString stringWithFormat:@"%@/index.html",basePath];
//    NSString *indexContent = [NSString stringWithContentsOfURL:[NSURL URLWithString:indexPath] encoding:NSUTF8StringEncoding error:nil];
//    [self.wkWebView loadHTMLString:indexContent baseURL:baseUrl];

//    [self setWebUrl:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"] allCor:YES];
}
- (NSURL *)fileURLForBuggyWKWebView8:(NSURL *)fileURL {
    NSError *error = nil;
    if (!fileURL.fileURL || ![fileURL checkResourceIsReachableAndReturnError:&error]) {
        return nil;
    }
    // Create "/temp/www" directory
    NSFileManager *fileManager= [NSFileManager defaultManager];
    NSURL *temDirURL = [[NSURL fileURLWithPath:NSTemporaryDirectory()] URLByAppendingPathComponent:@"www"];
    [fileManager createDirectoryAtURL:temDirURL withIntermediateDirectories:YES attributes:nil error:&error];
    
    NSURL *dstURL = [temDirURL URLByAppendingPathComponent:fileURL.lastPathComponent];
    // Now copy given file to the temp directory
    [fileManager removeItemAtURL:dstURL error:&error];
    [fileManager copyItemAtURL:fileURL toURL:dstURL error:&error];
    // Files in "/temp/www" load flawlesly :)
    return dstURL;
}
- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures
{
    WKFrameInfo *frameInfo = navigationAction.targetFrame;
    if (![frameInfo isMainFrame]) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}

-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
//    self.mackView.hidden = YES;
//    [_base hiddenHUB];
    if (self.js.length>0) {
        [self.wkWebView evaluateJavaScript:[NSString stringWithFormat:@"%@(%@)",self.function,self.js] completionHandler:^(id _Nullable item, NSError * _Nullable error) {
            NSLog(@"alert");
        }];
    }
}
- (void)setFunctionName:(NSString *)functionName js:(NSString *)js{
    self.wkWebView.frame = CGRectMake(0, 0, size_width, self.height);
    
    self.function = functionName;
    self.js = js;
    [self.wkWebView evaluateJavaScript:[NSString stringWithFormat:@"%@(%@)",self.function,self.js] completionHandler:^(id _Nullable item, NSError * _Nullable error) {
        NSLog(@"alert");
    }];
}
- (void)setWebUrl:(NSString *)url  allCor:(BOOL)allCor{
    self.mackView.hidden = NO;
//    [_base showHUBWith:@""];
    self.wkWebView.frame = CGRectMake(0, 0, size_width, size_width);
//    self.mackView.frame = CGRectMake(0, 0, size_width, self.height);
    if(url){
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        [self.wkWebView loadRequest:request];
//        if ([[UIDevice currentDevice].systemVersion floatValue] >= 9.0) {
//            NSURL *fileURL = [NSURL fileURLWithPath:url];
//            if (@available(iOS 9.0, *)) {
//                [self.wkWebView loadFileURL:fileURL allowingReadAccessToURL:[NSURL fileURLWithPath:[NSBundle mainBundle].bundlePath]];
//            } else {
//                // Fallback on earlier versions
//            }
//        } else {
//            NSURL *fileURL = [self fileURLForBuggyWKWebView8:[NSURL fileURLWithPath:url]];
//            NSURLRequest *request = [NSURLRequest requestWithURL:fileURL];
//            [self.wkWebView loadRequest:request];
//        }
    }
}
@end
