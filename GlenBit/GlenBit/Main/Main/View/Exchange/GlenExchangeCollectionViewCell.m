//
//  GlenExchangeCollectionViewCell.m
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenExchangeCollectionViewCell.h"

@interface GlenExchangeCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *lab1;
@property (weak, nonatomic) IBOutlet UILabel *lab2;
@property (weak, nonatomic) IBOutlet UILabel *lab3;

@end

@implementation GlenExchangeCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setDetail:(exchangeDetail *)detail{
    _detail = detail;
    if (_type == 2) {
        if (detail.isUp) {
            _lab2.textColor = [UIColor colorWithHexString:@"#00ACDC"];
        }else{
            _lab2.textColor = [UIColor colorWithHexString:@"#F04A5D"];
        }
        _lab1.text = [NSString StringFromTimestamp:detail.createdAt];;
        _lab2.text = [GlenTools formatString:detail.price scale:6];
        _lab3.text = [GlenTools formatString:detail.amount scale:6];
    }else{
        _lab1.text = [GlenTools formatString:detail.price scale:6];
        _lab2.text = [GlenTools formatString:detail.amount scale:6];
        _lab3.text = [GlenTools total:detail.price amount:detail.amount scale:6];
    }

}
- (void)setType:(NSInteger)type{

    _type = type;
    switch (type) {
        case 0:
        {
            _lab1.textColor = [UIColor colorWithHexString:@"#F04A5D"];
            _lab2.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
            _lab3.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
        }
            break;
        case 1:
        {
            _lab1.textColor = [UIColor colorWithHexString:@"#00ACDC"];
            _lab2.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
            _lab3.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
        }
            break;
        case 2:
        {
            _lab1.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
            _lab2.textColor = [UIColor colorWithHexString:@"#F04A5D"];
            _lab3.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
        }
            break;
            
        default:
            break;
    }
}
@end
