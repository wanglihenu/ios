//
//  GlenExchangHeadInfoCollectionReusableView.m
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenExchangHeadInfoCollectionReusableView.h"
#import "GlenPickerView.h"
#import "GlenFavoriteViewController.h"
#import "HomeModel.h"

@interface GlenExchangHeadInfoCollectionReusableView ()
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UILabel *amont;
@property (weak, nonatomic) IBOutlet UILabel *volum;
@property (weak, nonatomic) IBOutlet UILabel *high;
@property (weak, nonatomic) IBOutlet UILabel *low;
@property (weak, nonatomic) IBOutlet UIButton *change;
@property (weak, nonatomic) IBOutlet UILabel *close;



@property (nonatomic, strong) GlenPickerView *pickerView;
@property (nonatomic, strong) SortSecondMarkets *selectMarckt;


@property (strong, nonatomic) NSArray *tikers;
@property (strong, nonatomic) HomeModel *tikeDetal;
@property (weak, nonatomic) IBOutlet UILabel *amountStr;
@property (weak, nonatomic) IBOutlet UILabel *highStr;
@property (weak, nonatomic) IBOutlet UILabel *VolStr;
@property (weak, nonatomic) IBOutlet UILabel *LowSTR;





@end

@implementation GlenExchangHeadInfoCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _amountStr.text = kLocalizedString(@"AMOUNT", @"");
    _highStr.text = kLocalizedString(@"24H_HIGH", @"");
    _VolStr.text = kLocalizedString(@"VOLUME", @"交易量");
    _LowSTR.text = kLocalizedString(@"24h 最低价", @"");
    
    
    self.pickerView = [[GlenPickerView alloc]init];
    
    _selectMarckt = [UerManger shareUserManger].markes.markes[0];
    _selectModel = _selectMarckt.markes[0];
    
    [_btn1 setTitle:_selectModel.targetCoin forState:UIControlStateNormal];
    [_btn2 setTitle:_selectModel.marketCoin forState:UIControlStateNormal];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeHi:) name:NOTIFTICKER object:nil];
    _tikers = [UerManger shareUserManger].tikes;
    [self getHI];
}
- (void)setSelectModel:(Markets *)selectModel{
    _selectModel = selectModel;
    [_btn1 setTitle:_selectModel.targetCoin forState:UIControlStateNormal];
    [_btn2 setTitle:_selectModel.marketCoin forState:UIControlStateNormal];
}
- (void)changeHi:(NSNotification *)notfic{
    NSMutableArray *arr = notfic.userInfo[@"tiker"];
    _tikers = arr;
    [self getHI];
}
- (void)getHI{
    NSString *to = [NSString stringWithFormat:@"%@-%@",_selectModel.targetCoin,_selectModel.marketCoin];
    
    for (HomeModel *model in _tikers) {
        if ([model.pair isEqualToString:to]) {
            _tikeDetal = model;
            
            _volum.text = [GlenTools formatString:_tikeDetal.volume scale:_selectModel.precision.integerValue];
            _high.text = _tikeDetal.high;
            _low.text = _tikeDetal.low;
            _close.text = _tikeDetal.close;
            _amont.text = [GlenTools total:_tikeDetal.volume amount:_tikeDetal.close scale:_selectModel.precision.integerValue];
            if (model.change.floatValue>0) {
                [_change setBackgroundColor:[UIColor colorWithHexString:@"#00ACDC"]];
            }else{
                [_change setBackgroundColor:[UIColor colorWithHexString:@"#F04A5D"]];
            }
            [_change setTitle:[GlenTools formatPesent:_tikeDetal.change scale:2] forState:UIControlStateNormal];
            break;
        }
    }
}
- (IBAction)selctBtn:(UIButton *)sender {
    GlenWeakSelf;
    if (sender.tag == 12) {
        GlenFavoriteViewController *fav = [GlenFavoriteViewController new];
        [[self viewController].navigationController pushViewController:fav animated:YES];
        
    }else if (sender.tag == 10){
        [BRStringPickerView showStringPickerWithTitle:nil dataSource:[UerManger shareUserManger].markes.targetCoins defaultSelValue:nil resultBlock:^(id selectValue, NSInteger row) {
            weakSelf.selectMarckt = [UerManger shareUserManger].markes.markes[row];
            [sender setTitle:selectValue forState:UIControlStateNormal];
            weakSelf.selectModel = weakSelf.selectMarckt.markes[0];
            [weakSelf.btn2 setTitle:@"请选择" forState:UIControlStateNormal];
        }];
    }else if (sender.tag == 11){
        if (_selectMarckt) {
            [BRStringPickerView showStringPickerWithTitle:nil dataSource:_selectMarckt.marketCoins defaultSelValue:nil resultBlock:^(id selectValue, NSInteger row) {
                weakSelf.selectModel = weakSelf.selectMarckt.markes[row];
                [sender setTitle:selectValue forState:UIControlStateNormal];
                if (weakSelf.select) {
                    weakSelf.select(weakSelf.selectModel);
                };
            }];
        }

    }
}

@end
