//
//  KCollectionViewCell.h
//  kdemo
//
//  Created by Lee on 2018/12/25.
//  Copyright © 2018 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KlineModel.h"

@interface KCollectionViewCell : UICollectionViewCell

- (void)reloadKLineData:(NSArray<KlineModel *> *)klineList;


- (void)getMA;
- (void)getEMA;
- (void)getBOLL;
- (void)getMACD;
- (void)getKDJ;
- (void)getRSI;


@end
