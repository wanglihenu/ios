//
//  KCollectionViewCell.m
//  kdemo
//
//  Created by Lee on 2018/12/25.
//  Copyright © 2018 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "KCollectionViewCell.h"
#import <Charts/Charts-Swift.h>
#import "BTCFinanceDetailViewConst.h"
#import "KlineXAxisFormatter.h"
#import "KlineModel.h"

@interface KCollectionViewCell ()<ChartViewDelegate,UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet CombinedChartView *combinedChartView;
@property (weak, nonatomic) IBOutlet CombinedChartView *barChartView;
@property (weak, nonatomic) IBOutlet CombinedChartView *rsiCombinedChartView;
@property (nonatomic,   copy)NSArray<KlineModel *> *klineList;

/**十字光标*/
@property (nonatomic, strong) CAShapeLayer *crossLayer;
/**十字光标时间轴文字*/
@property (nonatomic, strong) CATextLayer *crossTimeLayer;
/**十字光标价格轴文字*/
@property (nonatomic, strong) CATextLayer *crossPriceLayer;

@end

@implementation KCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UITapGestureRecognizer* singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    singleTap.delegate = self;
    [self addGestureRecognizer:singleTap];

    // Initialization code
    [self setupCombinedChartView];
    [self setupBarChartsView];
    [self setupRSICombinedChartView];
}
- (CAShapeLayer *)crossLayer {
    if (!_crossLayer) {
        _crossLayer =[[CAShapeLayer alloc]init];
        _crossLayer.lineDashPattern = @[@1, @2];//画虚线
    }
    return _crossLayer;
}
- (CATextLayer *)crossPriceLayer {
    if (!_crossPriceLayer) {
        _crossPriceLayer = [[CATextLayer alloc]init];
    }
    return _crossPriceLayer;
}
- (CATextLayer *)crossTimeLayer {
    if (!_crossTimeLayer) {
        _crossTimeLayer = [[CATextLayer alloc]init];
    }
    return _crossTimeLayer;
}
- (void)setupCombinedChartView{
    
    self.combinedChartView.delegate = self;
    self.combinedChartView.chartDescription.enabled = NO;
    self.combinedChartView.drawGridBackgroundEnabled = NO;
    self.combinedChartView.pinchZoomEnabled = NO;
    self.combinedChartView.scaleXEnabled = YES;
    self.combinedChartView.scaleYEnabled = YES;
    self.combinedChartView.maxVisibleCount = 0;
    self.combinedChartView.chartDescription.enabled = NO;
    self.combinedChartView.legend.enabled = NO;
    self.combinedChartView.legend.textColor = RGBCOLOR(135, 135, 135);
    self.combinedChartView.backgroundColor = RGBCOLOR(39, 39, 39);
    [self.combinedChartView setAutoScaleMinMaxEnabled:YES];
    self.combinedChartView.doubleTapToZoomEnabled = NO;
    self.combinedChartView.noDataText = @"";
    
    ChartXAxis *xAxis = self.combinedChartView.xAxis;
    xAxis.enabled = NO;


    ChartYAxis *leftAxis = self.combinedChartView.leftAxis;
//    leftAxis.axisMaximum = 0.77;
//    leftAxis.axisMinimum = 0.76;
    leftAxis.drawGridLinesEnabled = YES;
    leftAxis.forceLabelsEnabled = YES;//不强制绘制指定数量的label
    leftAxis.drawAxisLineEnabled = NO;
    leftAxis.gridLineDashLengths = @[@5.f, @5.f];
    leftAxis.axisLineWidth = 1.1/[UIScreen mainScreen].scale;
    leftAxis.axisLineColor = [UIColor grayColor];
    leftAxis.labelTextColor = RGBCOLOR(135, 135, 135);
    leftAxis.labelCount = 3;
    leftAxis.labelPosition = YAxisLabelPositionInsideChart;
    //    leftAxis.valueFormatter = [[BTCKlineYAxisFormatter alloc] init];
    
    ChartYAxis *rightAxis = self.combinedChartView.rightAxis;
    rightAxis.enabled = NO;
}
- (void)setupRSICombinedChartView{
    
    self.rsiCombinedChartView.delegate = self;
    self.rsiCombinedChartView.chartDescription.enabled = NO;
    self.rsiCombinedChartView.drawGridBackgroundEnabled = NO;
    self.rsiCombinedChartView.pinchZoomEnabled = NO;
    self.rsiCombinedChartView.scaleXEnabled = YES;
    self.rsiCombinedChartView.scaleYEnabled = YES;
    self.rsiCombinedChartView.maxVisibleCount = 0;
    self.rsiCombinedChartView.chartDescription.enabled = NO;
    self.rsiCombinedChartView.legend.enabled = NO;
    self.rsiCombinedChartView.legend.textColor = RGBCOLOR(135, 135, 135);
    self.rsiCombinedChartView.backgroundColor = RGBCOLOR(39, 39, 39);
    [self.rsiCombinedChartView setAutoScaleMinMaxEnabled:YES];
    self.rsiCombinedChartView.doubleTapToZoomEnabled = NO;
    self.rsiCombinedChartView.noDataText = @"";
    
    ChartXAxis *xAxis = self.rsiCombinedChartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.drawGridLinesEnabled = NO;
    xAxis.drawAxisLineEnabled = YES;
    xAxis.valueFormatter = [[KlineXAxisFormatter alloc] init];
    xAxis.labelCount = 5;
    xAxis.axisLineWidth = 1.1/[UIScreen mainScreen].scale;
    xAxis.labelTextColor = RGBCOLOR(135, 135, 135);
    ChartYAxis *leftAxis = self.rsiCombinedChartView.leftAxis;
    leftAxis.drawGridLinesEnabled = YES;
    leftAxis.drawAxisLineEnabled = NO;
    leftAxis.gridLineDashLengths = @[@5.f, @5.f];
    leftAxis.axisLineWidth = 1.1/[UIScreen mainScreen].scale;
    leftAxis.axisLineColor = [UIColor grayColor];
    leftAxis.labelTextColor = RGBCOLOR(135, 135, 135);
    leftAxis.labelCount = 3;
    leftAxis.labelPosition = YAxisLabelPositionInsideChart;
    //    leftAxis.valueFormatter = [[BTCKlineYAxisFormatter alloc] init];
    
    ChartYAxis *rightAxis = self.rsiCombinedChartView.rightAxis;
    rightAxis.enabled = NO;
}
- (void)setupBarChartsView{
    
    self.barChartView.delegate = self;
    self.barChartView.chartDescription.enabled = NO;
    self.barChartView.drawGridBackgroundEnabled = NO;
    self.barChartView.pinchZoomEnabled = NO;
    self.barChartView.scaleXEnabled = YES;
    self.barChartView.scaleYEnabled = YES;
    self.barChartView.maxVisibleCount = 0;
    self.barChartView.chartDescription.enabled = NO;
    self.barChartView.legend.enabled = NO;
    self.barChartView.legend.textColor = RGBCOLOR(135, 135, 135);
    self.barChartView.backgroundColor = RGBCOLOR(39, 39, 39);
    [self.barChartView setAutoScaleMinMaxEnabled:YES];
    self.barChartView.doubleTapToZoomEnabled = NO;
    self.barChartView.noDataText = @"";
    
    ChartXAxis *xAxis = self.barChartView.xAxis;
    xAxis.enabled = NO;

    ChartYAxis *leftAxis = self.barChartView.leftAxis;
    leftAxis.drawGridLinesEnabled = YES;
    leftAxis.drawAxisLineEnabled = NO;
    leftAxis.gridLineDashLengths = @[@5.f, @5.f];
    leftAxis.axisLineWidth = 1.1/[UIScreen mainScreen].scale;
    leftAxis.axisLineColor = [UIColor grayColor];
    leftAxis.labelTextColor = RGBCOLOR(135, 135, 135);
    leftAxis.labelCount = 3;
    leftAxis.labelPosition = YAxisLabelPositionInsideChart;
    //    leftAxis.valueFormatter = [[BTCKlineYAxisFormatter alloc] init];
    
    ChartYAxis *rightAxis = self.barChartView.rightAxis;
    rightAxis.enabled = NO;
}
- (void)reloadKLineData:(NSArray<KlineModel *> *)klineList{
    
    if(klineList.count > 0){
        
        self.klineList = klineList;
        //        [self fillKlineView];
        //        [self fillBarView];
        //        [self fillRSIKlineView];
        [self getMA];
        [self getVol];
        [self getMACD];
    }
}
- (void)getMA{
    CombinedChartData *combinedData = [[CombinedChartData alloc] init];
    combinedData.lineData = [self getMALineData];
    combinedData.candleData = [self getMACandleData];
    self.combinedChartView.data = combinedData;
    [self.combinedChartView setVisibleXRangeMaximum:10.0f];
    [self.combinedChartView moveViewToX:self.klineList.count-1];
    
    CGAffineTransform srcMatrix = self.barChartView.viewPortHandler.touchMatrix;
    [self.combinedChartView.viewPortHandler refreshWithNewMatrix:srcMatrix
                                                           chart:self.combinedChartView
                                                      invalidate:YES];
}
- (LineChartData *)getMALineData{
    LineChartDataSet *set_M5 = [self getLineChartViewSetDay:5 name:@"MA5"];
    LineChartDataSet *set_M10 = [self getLineChartViewSetDay:10 name:@"MA10"];
    LineChartDataSet *set_M20 = [self getLineChartViewSetDay:20 name:@"MA20"];
    LineChartData *data = [[LineChartData alloc] initWithDataSets:@[set_M5, set_M10, set_M20]];
    return data;
}
- (NSArray *)getClosePriceList:(NSInteger)days{
    
    NSInteger dayValue = days - 1;
    NSMutableArray *closePriceList = [[NSMutableArray alloc] init];
    for(NSInteger i = self.klineList.count - 1; i >= 0; i--){
        if(i - dayValue < 0) break;
        double allClosePriceDouble = 0.0;
        for(NSInteger j = i-dayValue; j<= i; j++){
            KlineModel *candleItem = [self.klineList objectAtIndex:j];
            allClosePriceDouble += candleItem.closePrice;
        }
        NSString *closePriceStr = [NSString stringWithFormat:@"%f", allClosePriceDouble/days];
        [closePriceList insertObject:closePriceStr atIndex:0];
    }
    return closePriceList;
}
- (CandleChartData *)getMACandleData{
    
    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
    for(int i = 0; i < self.klineList.count; i++){
        
        KlineModel *candleItem = [self.klineList objectAtIndex:i];
        NSLog(@"%@", candleItem.timeStr);
        [yVals1 addObject:[[CandleChartDataEntry alloc] initWithX:i shadowH:candleItem.highestPrice shadowL:candleItem.lowestPrice open:candleItem.openPrice close:candleItem.closePrice]];
    }
    CandleChartDataSet *set1 = [[CandleChartDataSet alloc] initWithValues:yVals1 label:@"k line"];
    [set1 setShadowColorSameAsCandle:YES];
    set1.axisDependency = AxisDependencyLeft;
    [set1 setColor:[UIColor colorWithWhite:80/255.f alpha:1.f]];
    //    set1.drawIconsEnabled = NO;
    set1.shadowWidth = 0.7;
    set1.highlightEnabled = YES;
//    [set1 setDrawValuesEnabled:YES];
//    set1.valueFont = [UIFont systemFontOfSize:10.f];
//    set1.valueTextColor = [UIColor redColor];
    set1.decreasingColor = RGBCOLOR(221, 7, 113);
    set1.decreasingFilled = YES;
    set1.increasingColor = RGBCOLOR(118, 167, 0);
    set1.increasingFilled = YES;
    set1.neutralColor = RGBCOLOR(118, 167, 0);
    CandleChartData *data = [[CandleChartData alloc] initWithDataSet:set1];
    return data;
}
- (void)getEMA{
    CombinedChartData *combinedData = [[CombinedChartData alloc] init];
    combinedData.lineData = [self getEMALineData];
    combinedData.candleData = [self getMACandleData];
    self.combinedChartView.data = combinedData;
    [self.combinedChartView setVisibleXRangeMaximum:10.0f];
    [self.combinedChartView moveViewToX:self.klineList.count-1];
    
    CGAffineTransform srcMatrix = self.barChartView.viewPortHandler.touchMatrix;
    [self.combinedChartView.viewPortHandler refreshWithNewMatrix:srcMatrix
                                                           chart:self.combinedChartView
                                                      invalidate:YES];
}
- (LineChartData *)getEMALineData{
    LineChartDataSet *set_M5 = [self getEMALineChartViewSetDay:5 name:@"MA5"];
    LineChartDataSet *set_M10 = [self getEMALineChartViewSetDay:10 name:@"MA10"];
    LineChartDataSet *set_M20 = [self getEMALineChartViewSetDay:20 name:@"MA20"];
    LineChartData *data = [[LineChartData alloc] initWithDataSets:@[set_M5, set_M10, set_M20]];
    return data;
}
- (LineChartDataSet *)getEMALineChartViewSetDay:(NSInteger)day name:(NSString *)name{
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    for(int index = 0; index < self.klineList.count; index++){
        KlineModel *model = self.klineList[index];
        double ema = 0.0 ;
        if (day == 5) {
            ema = model.EMA5.doubleValue;
        }else if (day == 10){
            ema = model.EMA10.doubleValue;
        }else if (day == 30){
            ema = model.EMA30.doubleValue;
        }
        [yVals addObject:[[ChartDataEntry alloc] initWithX:index y:ema]];
    }
    
    LineChartDataSet *set = [[LineChartDataSet alloc] initWithValues:yVals label:name];
    [self configLineSets:set];
    if (day == 5) {
        [set setColor:[UIColor colorWithRed:119/255.f green:206/255.f blue:254/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:119/255.f green:206/255.f blue:254/255.f alpha:1.f];
    }
    if (day == 10) {
        [set setColor:[UIColor colorWithRed:132/255.f green:80/255.f blue:240/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:132/255.f green:80/255.f blue:240/255.f alpha:1.f];
    }
    if (day == 20) {
        [set setColor:[UIColor colorWithRed:249/255.f green:237/255.f blue:81/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:249/255.f green:237/255.f blue:81/255.f alpha:1.f];
    }
    
    return set;
}
- (void)getBOLL{
    CombinedChartData *combinedData = [[CombinedChartData alloc] init];
    combinedData.lineData = [self getBOOLLineData];
    combinedData.candleData = [self getMACandleData];
    self.combinedChartView.data = combinedData;
    [self.combinedChartView setVisibleXRangeMaximum:10.0f];
    [self.combinedChartView moveViewToX:self.klineList.count-1];
    
    CGAffineTransform srcMatrix = self.barChartView.viewPortHandler.touchMatrix;
    [self.combinedChartView.viewPortHandler refreshWithNewMatrix:srcMatrix
                                                           chart:self.combinedChartView
                                                      invalidate:YES];
}
- (LineChartData *)getBOOLLineData{
    LineChartDataSet *set_M5 = [self getBOOLLineChartViewSetDay:5 name:@"MA5"];
    LineChartDataSet *set_M10 = [self getBOOLLineChartViewSetDay:10 name:@"MA10"];
    LineChartDataSet *set_M20 = [self getBOOLLineChartViewSetDay:20 name:@"MA20"];
    LineChartData *data = [[LineChartData alloc] initWithDataSets:@[set_M5, set_M10, set_M20]];
    return data;
}
- (LineChartDataSet *)getBOOLLineChartViewSetDay:(NSInteger)day name:(NSString *)name{
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    for(int index = 0; index < self.klineList.count; index++){
        KlineModel *model = self.klineList[index];
        double ema = 0.0 ;
        if (day == 5) {
            ema = model.BOLL_MA.doubleValue;
        }else if (day == 10){
            ema = model.BOLL_UP.doubleValue;
        }else if (day == 30){
            ema = model.BOLL_DN.doubleValue;
        }
        [yVals addObject:[[ChartDataEntry alloc] initWithX:index y:ema]];
    }
    
    LineChartDataSet *set = [[LineChartDataSet alloc] initWithValues:yVals label:name];
    [self configLineSets:set];
    if (day == 5) {
        [set setColor:[UIColor colorWithRed:119/255.f green:206/255.f blue:254/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:119/255.f green:206/255.f blue:254/255.f alpha:1.f];
    }
    if (day == 10) {
        [set setColor:[UIColor colorWithRed:132/255.f green:80/255.f blue:240/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:132/255.f green:80/255.f blue:240/255.f alpha:1.f];
    }
    if (day == 20) {
        [set setColor:[UIColor colorWithRed:249/255.f green:237/255.f blue:81/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:249/255.f green:237/255.f blue:81/255.f alpha:1.f];
    }
    
    return set;
}
- (void)getVol{
    CombinedChartData *combinedData = [[CombinedChartData alloc] init];
    combinedData.barData = [self getVolBarData];
    self.barChartView.data = combinedData;
    [self.barChartView setVisibleXRangeMaximum:10.0f];
    [self.barChartView moveViewToX:self.klineList.count-1];
    
    CGAffineTransform srcMatrix = self.combinedChartView.viewPortHandler.touchMatrix;
    [self.barChartView.viewPortHandler refreshWithNewMatrix:srcMatrix
                                                              chart:self.barChartView
                                                         invalidate:YES];
    
    
    
}
- (void)getMACD{
    CombinedChartData *combinedData = [[CombinedChartData alloc] init];
    combinedData.barData = [self getMACDBarData];
    combinedData.lineData = [self getMACDLineData];
    self.rsiCombinedChartView.data = combinedData;
    [self.rsiCombinedChartView setVisibleXRangeMaximum:10.0f];
    [self.rsiCombinedChartView moveViewToX:self.klineList.count-1];
    
    KlineXAxisFormatter *formatter = (KlineXAxisFormatter *)self.rsiCombinedChartView.xAxis.valueFormatter;
    formatter.kLineList = self.klineList;
    
    
    CGAffineTransform srcMatrix = self.barChartView.viewPortHandler.touchMatrix;
    [self.rsiCombinedChartView.viewPortHandler refreshWithNewMatrix:srcMatrix
                                                              chart:self.rsiCombinedChartView
                                                         invalidate:YES];
}
- (BarChartData *)getVolBarData{
    //对应Y轴上面需要显示的数据
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.klineList.count; i++) {
        BarChartDataEntry *entry = [[BarChartDataEntry alloc] initWithX:i y:[self.klineList[i] volumn].doubleValue];
        [yVals addObject:entry];
    }
    
    //创建BarChartDataSet对象，其中包含有Y轴数据信息，以及可以设置柱形样式
    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithValues:yVals];
    //    set1.barBorderWidth = 0.0;//柱形之间的间隙占整个柱形(柱形+间隙)的比例
    set1.drawValuesEnabled = YES;//是否在柱形图上面显示数值
    set1.highlightEnabled = YES;//点击选中柱形图是否有高亮效果，（双击空白处取消选中）
    
    [set1 setColors:[self getBarColors]];//设置柱形图颜色
    //将BarChartDataSet对象放入数组中
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    //创建BarChartData对象, 此对象就是barChartView需要最终数据对象
    BarChartData *data = [[BarChartData alloc] initWithDataSet:set1];
    return data;
}
- (BarChartData *)getMACDBarData{
    
    //对应Y轴上面需要显示的数据
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.klineList.count; i++) {
        BarChartDataEntry *entry = [[BarChartDataEntry alloc] initWithX:i y:[self.klineList[i] MACD].doubleValue];
        [yVals addObject:entry];
    }
    
    
    //创建BarChartDataSet对象，其中包含有Y轴数据信息，以及可以设置柱形样式
    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithValues:yVals];
    //    set1.barBorderWidth = 0.0;//柱形之间的间隙占整个柱形(柱形+间隙)的比例
    set1.drawValuesEnabled = YES;//是否在柱形图上面显示数值
    set1.highlightEnabled = YES;//点击选中柱形图是否有高亮效果，（双击空白处取消选中）
    
    [set1 setColors:[self getBarColors]];//设置柱形图颜色
    //将BarChartDataSet对象放入数组中
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    //创建BarChartData对象, 此对象就是barChartView需要最终数据对象
    BarChartData *data = [[BarChartData alloc] initWithDataSet:set1];
    return data;
}
- (LineChartData *)getMACDLineData{
    LineChartDataSet *set_M5 = [self getMACDLineChartViewSetDay:5 name:@"MA5"];
    LineChartDataSet *set_M10 = [self getMACDLineChartViewSetDay:10 name:@"MA10"];
    LineChartData *data = [[LineChartData alloc] initWithDataSets:@[set_M5, set_M10]];
    return data;
}
- (LineChartDataSet *)getMACDLineChartViewSetDay:(NSInteger)day name:(NSString *)name{
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    for(int index = 0; index < self.klineList.count; index++){
        KlineModel *model = self.klineList[index];
        double ema = 0.0 ;
        if (day == 5) {
            ema = model.DIF.doubleValue;
        }else if (day == 10){
            ema = model.DEA.doubleValue;
        }else if (day == 30){
            ema = model.EMA30.doubleValue;
        }
        [yVals addObject:[[ChartDataEntry alloc] initWithX:index y:ema]];
    }
    
    LineChartDataSet *set = [[LineChartDataSet alloc] initWithValues:yVals label:name];
    [self configLineSets:set];
    if (day == 5) {
        [set setColor:[UIColor colorWithRed:119/255.f green:206/255.f blue:254/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:119/255.f green:206/255.f blue:254/255.f alpha:1.f];
    }
    if (day == 10) {
        [set setColor:[UIColor colorWithRed:132/255.f green:80/255.f blue:240/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:132/255.f green:80/255.f blue:240/255.f alpha:1.f];
    }
    if (day == 20) {
        [set setColor:[UIColor colorWithRed:249/255.f green:237/255.f blue:81/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:249/255.f green:237/255.f blue:81/255.f alpha:1.f];
    }
    
    return set;
}
- (void)getKDJ{
    CombinedChartData *combinedData = [[CombinedChartData alloc] init];
    combinedData.lineData = [self getKDJLineData];
    self.rsiCombinedChartView.data = combinedData;
    [self.rsiCombinedChartView setVisibleXRangeMaximum:10.0f];
    [self.rsiCombinedChartView moveViewToX:self.klineList.count-1];
    
    CGAffineTransform srcMatrix = self.barChartView.viewPortHandler.touchMatrix;
    [self.rsiCombinedChartView.viewPortHandler refreshWithNewMatrix:srcMatrix
                                                              chart:self.rsiCombinedChartView
                                                         invalidate:YES];
}
- (LineChartData *)getKDJLineData{
    LineChartDataSet *set_M5 = [self getKDJLineChartViewSetDay:5 name:@"MA5"];
    LineChartDataSet *set_M10 = [self getKDJLineChartViewSetDay:10 name:@"MA10"];
    LineChartDataSet *set_M20 = [self getKDJLineChartViewSetDay:30 name:@"MA20"];
    LineChartData *data = [[LineChartData alloc] initWithDataSets:@[set_M5, set_M10,set_M20]];
    return data;
}
- (LineChartDataSet *)getKDJLineChartViewSetDay:(NSInteger)day name:(NSString *)name{
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    for(int index = 0; index < self.klineList.count; index++){
        KlineModel *model = self.klineList[index];
        double ema = 0.0 ;
        if (day == 5) {
            ema = model.KDJ_K.doubleValue;
        }else if (day == 10){
            ema = model.KDJ_D.doubleValue;
        }else if (day == 30){
            ema = model.KDJ_J.doubleValue;
        }
        [yVals addObject:[[ChartDataEntry alloc] initWithX:index y:ema]];
    }
    
    LineChartDataSet *set = [[LineChartDataSet alloc] initWithValues:yVals label:name];
    [self configLineSets:set];
    if (day == 5) {
        [set setColor:[UIColor colorWithRed:119/255.f green:206/255.f blue:254/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:119/255.f green:206/255.f blue:254/255.f alpha:1.f];
    }
    if (day == 10) {
        [set setColor:[UIColor colorWithRed:132/255.f green:80/255.f blue:240/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:132/255.f green:80/255.f blue:240/255.f alpha:1.f];
    }
    if (day == 20) {
        [set setColor:[UIColor colorWithRed:249/255.f green:237/255.f blue:81/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:249/255.f green:237/255.f blue:81/255.f alpha:1.f];
    }
    
    return set;
}
- (void)getRSI{
    CombinedChartData *combinedData = [[CombinedChartData alloc] init];
    combinedData.lineData = [self getRSILineData];
    self.rsiCombinedChartView.data = combinedData;
    [self.rsiCombinedChartView setVisibleXRangeMaximum:10.0f];
    [self.rsiCombinedChartView moveViewToX:self.klineList.count-1];
    
    CGAffineTransform srcMatrix = self.barChartView.viewPortHandler.touchMatrix;
    [self.rsiCombinedChartView.viewPortHandler refreshWithNewMatrix:srcMatrix
                                                              chart:self.rsiCombinedChartView
                                                         invalidate:YES];
}
- (LineChartData *)getRSILineData{
    LineChartDataSet *set_M5 = [self getRSILineChartViewSetDay:5 name:@"MA5"];
    LineChartDataSet *set_M10 = [self getRSILineChartViewSetDay:10 name:@"MA10"];
    LineChartDataSet *set_M20 = [self getRSILineChartViewSetDay:30 name:@"MA20"];
    LineChartData *data = [[LineChartData alloc] initWithDataSets:@[set_M5, set_M10,set_M20]];
    return data;
}
- (LineChartDataSet *)getRSILineChartViewSetDay:(NSInteger)day name:(NSString *)name{
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    for(int index = 0; index < self.klineList.count; index++){
        KlineModel *model = self.klineList[index];
        double ema = 0.0 ;
        if (day == 5) {
            ema = model.RSI_6.doubleValue;
        }else if (day == 10){
            ema = model.RSI_12.doubleValue;
        }else if (day == 30){
            ema = model.RSI_24.doubleValue;
        }
        [yVals addObject:[[ChartDataEntry alloc] initWithX:index y:ema]];
    }
    
    LineChartDataSet *set = [[LineChartDataSet alloc] initWithValues:yVals label:name];
    [self configLineSets:set];
    if (day == 5) {
        [set setColor:[UIColor colorWithRed:119/255.f green:206/255.f blue:254/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:119/255.f green:206/255.f blue:254/255.f alpha:1.f];
    }
    if (day == 10) {
        [set setColor:[UIColor colorWithRed:132/255.f green:80/255.f blue:240/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:132/255.f green:80/255.f blue:240/255.f alpha:1.f];
    }
    if (day == 20) {
        [set setColor:[UIColor colorWithRed:249/255.f green:237/255.f blue:81/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:249/255.f green:237/255.f blue:81/255.f alpha:1.f];
    }
    
    return set;
}

- (LineChartDataSet *)getLineChartViewSetDay:(NSInteger)day name:(NSString *)name{
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    NSArray *closePriceM5List = [self getClosePriceList:day];
    for(int index = 0; index < closePriceM5List.count; index++){
        [yVals addObject:[[ChartDataEntry alloc] initWithX:index + day - 1 y:[[closePriceM5List objectAtIndex:index] doubleValue]]];
    }
    
    LineChartDataSet *set = [[LineChartDataSet alloc] initWithValues:yVals label:name];
    [self configLineSets:set];
    if (day == 5) {
        [set setColor:[UIColor colorWithRed:119/255.f green:206/255.f blue:254/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:119/255.f green:206/255.f blue:254/255.f alpha:1.f];
    }
    if (day == 10) {
        [set setColor:[UIColor colorWithRed:132/255.f green:80/255.f blue:240/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:132/255.f green:80/255.f blue:240/255.f alpha:1.f];
    }
    if (day == 20) {
        [set setColor:[UIColor colorWithRed:249/255.f green:237/255.f blue:81/255.f alpha:1.f]];
        set.fillColor = [UIColor colorWithRed:249/255.f green:237/255.f blue:81/255.f alpha:1.f];
    }
    
    return set;
}


- (void)configLineSets:(LineChartDataSet *)set{
    
    set.lineWidth = 1.0f;
    set.mode = LineChartModeCubicBezier;
    set.drawValuesEnabled = NO;
    set.highlightEnabled = YES;
    set.axisDependency = AxisDependencyLeft;
    set.drawCirclesEnabled = NO;
    

}


- (NSArray *)getBarColors{
    
    NSMutableArray *colorList = [[NSMutableArray alloc] init];
    for(int i = 0; i < self.klineList.count; i++){
        KlineModel *candleItem = [self.klineList objectAtIndex:i];
        if(candleItem.closePrice - candleItem.openPrice >= 0){
            [colorList addObject:RGBCOLOR(118, 167, 1)];
        }else{
            [colorList addObject:RGBCOLOR(221, 7, 113)];
        }
    }
    return colorList;
}
/**画文字,指定CATextLayer*/
- (void)drawCrossLabelWithTextLayer:(CATextLayer*)textLayer AtRect:(CGRect)rect textStr:(NSString*)textStr {
    textLayer.frame = rect;
    textLayer.backgroundColor = [UIColor grayColor].CGColor;
    [self.layer addSublayer:textLayer];
    //set text attributes
    textLayer.foregroundColor = [UIColor redColor].CGColor;
    textLayer.alignmentMode = kCAAlignmentCenter;
    textLayer.wrapped = YES;
    //choose a font
    UIFont *font = [UIFont systemFontOfSize:10];
    //set layer font
    CFStringRef fontName = (__bridge CFStringRef)font.fontName;
    CGFontRef fontRef = CGFontCreateWithFontName(fontName);
    textLayer.font = fontRef;
    textLayer.fontSize = font.pointSize;
    CGFontRelease(fontRef);
    textLayer.contentsScale = [UIScreen mainScreen].scale;
    //choose some text
    //set layer text
    textLayer.string = textStr;
}
#pragma mark -
#pragma mark ChartView Delegate
- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry highlight:(ChartHighlight * __nonnull)highlight {
    NSLog(@"");

}

- (void)chartScaled:(ChartViewBase *)chartView scaleX:(CGFloat)scaleX scaleY:(CGFloat)scaleY {
    
    CGAffineTransform srcMatrix = chartView.viewPortHandler.touchMatrix;
    [self.combinedChartView.viewPortHandler refreshWithNewMatrix:srcMatrix
                                                           chart:self.combinedChartView
                                                      invalidate:YES];
    [self.barChartView.viewPortHandler refreshWithNewMatrix:srcMatrix
                                                      chart:self.barChartView
                                                 invalidate:YES];
    [self.rsiCombinedChartView.viewPortHandler refreshWithNewMatrix:srcMatrix
                                                              chart:self.rsiCombinedChartView
                                                         invalidate:YES];
}

- (void)chartTranslated:(ChartViewBase *)chartView dX:(CGFloat)dX dY:(CGFloat)dY {
    
    CGAffineTransform srcMatrix = chartView.viewPortHandler.touchMatrix;
    [self.combinedChartView.viewPortHandler refreshWithNewMatrix:srcMatrix
                                                           chart:self.combinedChartView
                                                      invalidate:YES];
    [self.barChartView.viewPortHandler refreshWithNewMatrix:srcMatrix
                                                      chart:self.barChartView
                                                 invalidate:YES];
    [self.rsiCombinedChartView.viewPortHandler refreshWithNewMatrix:srcMatrix
                                                              chart:self.rsiCombinedChartView
                                                         invalidate:YES];
}

- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView {
    self.crossLayer.path = nil;
    [self.crossPriceLayer removeFromSuperlayer];
    [self.crossTimeLayer removeFromSuperlayer];
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    
    CGPoint point = [sender locationInView:self];
    CombinedChartView *chat;
    
    if ([_combinedChartView.layer containsPoint:point]) {
        NSLog(@"sss");
        chat = _combinedChartView;
    }else if ([_barChartView.layer containsPoint:point]){
        NSLog(@"sss");
        chat = _barChartView;
    }else if ([_rsiCombinedChartView.layer containsPoint:point]){
        NSLog(@"sss");
        chat = _rsiCombinedChartView;
    }
    ChartHighlight *hight = [chat.highlighter getHighlightWithX:point.x y:point.y];
    [self shizixian:hight ChartView:chat];
}
- (void)shizixian:(ChartHighlight *)highlight ChartView:(ChartViewBase *)chartView{
    CGPoint point =[chartView convertPoint:CGPointMake(highlight.xPx, highlight.yPx) toView:self];
    UIBezierPath * path = [[UIBezierPath alloc]init];
    [path moveToPoint:CGPointMake(point.x, 0)];
    [path addLineToPoint:CGPointMake(point.x, 300)];
    [path moveToPoint:CGPointMake(0, point.y)];
    [path addLineToPoint:CGPointMake(size_width, point.y)];
    self.crossLayer.strokeColor = [UIColor redColor].CGColor;
    self.crossLayer.path = path.CGPath;
    self.crossLayer.lineJoin = kCALineCapRound;
    [self.layer addSublayer: self.crossLayer];
    
    [self drawCrossLabelWithTextLayer:self.crossTimeLayer AtRect:CGRectMake(point.x - 10, 300, 30, 20) textStr:@"eee"];
    [self drawCrossLabelWithTextLayer:self.crossPriceLayer AtRect:CGRectMake(0, point.y - 10, 30, 20) textStr:@"aaa"];
}
@end
