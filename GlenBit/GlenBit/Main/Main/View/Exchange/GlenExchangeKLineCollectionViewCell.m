//
//  GlenExchangeKLineCollectionViewCell.m
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenExchangeKLineCollectionViewCell.h"
#import <Charts/Charts-Swift.h>
#import "DepthXAxisFormatter.h"
#import "DepthYAxisFormatter.h"


@interface GlenExchangeKLineCollectionViewCell ()<ChartViewDelegate>
@property (weak, nonatomic) IBOutlet LineChartView *depthChartView;



@end

@implementation GlenExchangeKLineCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setupDepthChartsView];
}
- (void)setModel:(ExchangeModel *)model{
    _model = model;
    [self fillDepthView];
}
- (void)setupDepthChartsView{
    //    self.depthChartView.delegate = self;
    self.depthChartView.chartDescription.enabled = NO;
    self.depthChartView.drawGridBackgroundEnabled = NO;
    self.depthChartView.pinchZoomEnabled = NO;
    self.depthChartView.scaleXEnabled = YES;
    self.depthChartView.scaleYEnabled = NO;
    self.depthChartView.maxVisibleCount = 5;
    self.depthChartView.legend.enabled = NO;
//    self.depthChartView.legend.textColor = RGBCOLOR(135, 135, 135);
    self.depthChartView.backgroundColor = [UIColor colorWithHexString:@"#182027"];
//    [self.depthChartView setAutoScaleMinMaxEnabled:YES];
    self.depthChartView.doubleTapToZoomEnabled = NO;
    self.depthChartView.noDataText = @"";
    
    ChartXAxis *xAxis = self.depthChartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.drawGridLinesEnabled = YES;
    xAxis.drawAxisLineEnabled = NO;
    xAxis.gridLineDashLengths = @[@5.f, @5.f];
    xAxis.axisLineWidth = 1.1/[UIScreen mainScreen].scale;
    xAxis.axisLineColor = [UIColor grayColor];
    xAxis.valueFormatter = [[DepthXAxisFormatter alloc] init];
    xAxis.labelCount = 5;
    xAxis.axisLineWidth = 1.1/[UIScreen mainScreen].scale;
    xAxis.labelTextColor = [UIColor whiteColor];
    
    
    ChartYAxis *leftAxis = self.depthChartView.leftAxis;
    leftAxis.drawGridLinesEnabled = YES;
    leftAxis.drawAxisLineEnabled = YES;
    leftAxis.gridLineDashLengths = @[@5.f, @5.f];
    leftAxis.axisLineDashLengths = @[@5.f, @5.f];
    leftAxis.axisLineWidth = 1.1/[UIScreen mainScreen].scale;
    leftAxis.axisLineColor = [UIColor grayColor];
    leftAxis.labelTextColor = [UIColor whiteColor];
    leftAxis.valueFormatter = [[DepthYAxisFormatter alloc]init];
    leftAxis.labelCount = 3;
    leftAxis.labelPosition = YAxisLabelPositionInsideChart;
    ChartYAxis *rightAxis = self.depthChartView.rightAxis;
    rightAxis.drawGridLinesEnabled = YES;
    rightAxis.drawAxisLineEnabled = YES;
    rightAxis.gridLineDashLengths = @[@5.f, @5.f];
    rightAxis.axisLineDashLengths = @[@5.f, @5.f];
    rightAxis.axisLineWidth = 1.1/[UIScreen mainScreen].scale;
    rightAxis.axisLineColor = [UIColor grayColor];
    rightAxis.labelTextColor = [UIColor whiteColor];
    rightAxis.valueFormatter = [[DepthYAxisFormatter alloc]init];
    rightAxis.labelCount = 3;
    rightAxis.labelPosition = YAxisLabelPositionInsideChart;
    //    self.depthChartView.rightAxis.enabled = NO;
}

- (void)fillDepthView{
    
    LineChartData *data = [[LineChartData alloc] init];
    BOOL needNotify = NO;
    NSArray<exchangeDetail *> *bidlist = self.model.sortbuys;
    NSArray<exchangeDetail *> *asklist = self.model.sortsells;
    
    if(bidlist.count || asklist.count){
        DepthXAxisFormatter *depthXAxisFormatter = (DepthXAxisFormatter *)self.depthChartView.xAxis.valueFormatter;
        depthXAxisFormatter.depthDataItem = _model;
        DepthYAxisFormatter *depthLeftYAxisFormatter = (DepthYAxisFormatter *)self.depthChartView.leftAxis.valueFormatter;
        depthLeftYAxisFormatter.kLineList = asklist;
        DepthYAxisFormatter *depthRightYAxisFormatter = (DepthYAxisFormatter *)self.depthChartView.rightAxis.valueFormatter;
        depthRightYAxisFormatter.kLineList = bidlist;
    }
    if(bidlist.count > 0){
        
        NSArray* reversedArray = [[[[NSMutableArray alloc] initWithArray:bidlist] reverseObjectEnumerator] allObjects];
        
        BOOL bidNeedNotify = [self needNotifyDataWithDepthViewWithBidList:bidlist andAskList:asklist atIndex:0 withData:data];
        needNotify = bidNeedNotify||needNotify;
    }
    if(asklist.count > 0){
        BOOL askNeedNotify = [self needNotifyDataWithDepthViewWithBidList:bidlist andAskList:asklist atIndex:1 withData:data];
        needNotify = askNeedNotify||needNotify;
    }
    [data setValueFont:[UIFont systemFontOfSize:8.f]];//文字字体
    [data setValueTextColor:[UIColor whiteColor]];//文字颜色
    self.depthChartView.data = data;
    //    [self.depthChartView animateWithYAxisDuration:1.0f];
    if(needNotify)[self.depthChartView notifyDataSetChanged];
}
//卖
- (BOOL)needNotifyDataWithDepthViewWithBidList:(NSArray<exchangeDetail *> *)bids andAskList:(NSArray<exchangeDetail *> *)asks atIndex:(NSInteger)index withData:(LineChartData *)chartData{
    
    NSArray<exchangeDetail *> *depthList = [[NSArray<exchangeDetail *> alloc] init];
    if(index == 0){
        depthList = bids;
    }else{
        depthList = asks;
    }
    //对应Y轴上面需要显示的数据
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < depthList.count; i++) {
        
        ChartDataEntry *entry = [[ChartDataEntry alloc] initWithX:index?(i + bids.count):i y:[[depthList[i] amount] doubleValue]];
        [yVals addObject:entry];
    }
    
    LineChartDataSet *set1 = nil;
    // 待会加回来
    //    if (self.depthChartView.data.dataSetCount ==  2) {
    //
    //        set1 = (LineChartDataSet *)self.depthChartView.data.dataSets[index];
    //        set1.values = yVals;
    //        [self.depthChartView.data notifyDataChanged];
    //        return YES;
    //
    //    }else{
    
    //创建LineChartDataSet对象
    set1 = [[LineChartDataSet alloc] initWithValues:yVals label:index?@"asks":@"bids"];
    //设置折线的样式
    set1.drawIconsEnabled = NO;
    set1.formLineWidth = 1.1;//折线宽度
    set1.formSize = 13.0;
    set1.drawValuesEnabled = YES;//是否在拐点处显示数据
    //        set1.valueColors = @[[UIColor whiteColor]];//折线拐点处显示数据的颜色
    [set1 setColor:index?[UIColor colorWithHexString:@"#00ACDC"]:[UIColor colorWithHexString:@"#F04A5D"]];//折线颜色
    //折线拐点样式
    set1.drawCirclesEnabled = NO;//是否绘制拐点
    //第二种填充样式:渐变填充
    set1.drawFilledEnabled = YES;//是否填充颜色
    set1.fillAlpha = 0.2f;//透明度
    set1.fillColor = index?[UIColor colorWithHexString:@"#00ACDCCC"]:[UIColor colorWithHexString:@"#F04A5DCC"];
    //点击选中拐点的交互样式
    set1.highlightEnabled = NO;//选中拐点,是否开启高亮效果(显示十字线)
//    set1.highlightColor = RGBCOLOR(125, 125, 125);//点击选中拐点的十字线的颜色
    set1.highlightLineWidth = 1.1/[UIScreen mainScreen].scale;//十字线宽度
    set1.highlightLineDashLengths = @[@5, @5];//十字线的虚线样式
    
    //将 LineChartDataSet 对象放入数组中
    //        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    //        [dataSets addObject:set1];
    
    //创建 LineChartData 对象, 此对象就是lineChartView需要最终数据对象
    //        LineChartData *data = [[LineChartData alloc] initWithDataSets:dataSets];
    [chartData addDataSet:set1];
    return NO;
    //    }
}
@end
