//
//  GlenExchangHeadInfoCollectionReusableView.h
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlenExchangHeadInfoCollectionReusableView : UICollectionReusableView

@property (nonatomic, copy) void(^select)(Markets *markes);
@property (nonatomic, strong) Markets *selectModel;

@end
