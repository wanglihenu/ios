//
//  GlenBuyCollectionViewCell.m
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBuyCollectionViewCell.h"
#import "GlenSilder.h"
#import "ExchangeModel.h"
#import "ASValueTrackingSlider.h"

@interface GlenBuyCollectionViewCell ()<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIView *priceView;
@property (weak, nonatomic) IBOutlet UIView *amountView;
@property (weak, nonatomic) IBOutlet ASValueTrackingSlider *silder;
@property (weak, nonatomic) IBOutlet UILabel *exType;
@property (weak, nonatomic) IBOutlet UILabel *avaiType;
@property (weak, nonatomic) IBOutlet UILabel *avaiValue;
@property (weak, nonatomic) IBOutlet UITextField *price;
@property (weak, nonatomic) IBOutlet UITextField *amount;
@property (weak, nonatomic) IBOutlet UILabel *best;
@property (weak, nonatomic) IBOutlet UILabel *max;
@property (weak, nonatomic) IBOutlet UILabel *fee;
@property (weak, nonatomic) IBOutlet UILabel *volume;


@property (strong, nonatomic) Markets *marketModel;
@property (strong, nonatomic) NSString *balance;
@property (strong, nonatomic) NSString *maxValue;
@property (assign, nonatomic) NSInteger type;

@property (weak, nonatomic) IBOutlet UILabel *ratioStr;

@end

@implementation GlenBuyCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _priceView.layer.borderColor = _amountView.layer.borderColor = [UIColor colorWithHexString:@"#353B41"].CGColor;
    _priceView.layer.borderWidth = _amountView.layer.borderWidth = 1;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterPercentStyle];
    [_silder setNumberFormatter:formatter];
    _silder.font = [UIFont systemFontOfSize:12];
    [_silder setThumbImage:[UIImage imageNamed:@"icon_ellipse"] forState:UIControlStateNormal];
    [_silder setThumbImage:[UIImage imageNamed:@"icon_ellipse"] forState:UIControlStateSelected];
    [_silder setThumbImage:[UIImage imageNamed:@"icon_ellipse"] forState:UIControlStateHighlighted];
    _silder.maximumTrackTintColor = [UIColor colorWithHexString:@"#353B41"];
    _silder.minimumTrackTintColor = [UIColor colorWithHexString:@"#ffffff"];
    [_silder addTarget:self action:@selector(sliderValueChanged:)forControlEvents:UIControlEventValueChanged];
    UITapGestureRecognizer *_tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionTapGesture:)];
    _tapGesture.delegate = self;
    [_silder addGestureRecognizer:_tapGesture];

    [_price addTarget:self action:@selector(changedTextField:) forControlEvents:UIControlEventEditingChanged];
    [_amount addTarget:self action:@selector(changedTextField:) forControlEvents:UIControlEventEditingChanged];
    _ratioStr.text = kLocalizedString(@"RATIO", @"比例");
}
- (void)sliderValueChanged:(UISlider *)sende{
    CGFloat aa = sende.value * 4;
    NSInteger finalValue = round(aa);
    CGFloat aaa = finalValue / 4.00;
    [_silder setValue:aaa];
    [_silder showPopUpViewAnimated:YES];
    [self changeAmount:aaa];
}
- (void)actionTapGesture:(UITapGestureRecognizer *)sender{
    CGPoint touchPoint = [sender locationInView:_silder];
    CGFloat value = (_silder.maximumValue - _silder.minimumValue) * (touchPoint.x / _silder.frame.size.width );
    CGFloat aa = value * 4;
    NSInteger finalValue = round(aa);
    CGFloat aaa = finalValue / 4.00;
    [_silder setValue:aaa];
    [_silder showPopUpViewAnimated:YES];
    [self changeAmount:aaa];
}
- (void)changeAmount:(CGFloat)scale{
    _amount.text = [GlenTools total:_maxValue amount:[NSString stringWithFormat:@"%f",scale] scale:6];
    NSString *temVom = [GlenTools total:_price.text amount:_amount.text scale:6];
    _volume.text = [NSString stringWithFormat:@"%@ %@ %@",kLocalizedString(@"VOLUME", @"交易量"),temVom,_marketModel.marketCoin];
    _maxValue = [_maxValue stringByReplacingOccurrencesOfString:@"," withString:@""];
    if (self.upData) {
        self.upData(_price.text, _amount.text, _maxValue);
    }
}
-(void)changedTextField:(UITextField *)textField
{
    if (textField == _price) {
        if (textField.text.floatValue == 0) {
            _max.text = [NSString stringWithFormat:@" %@%@ %@",kLocalizedString(@"MAX", @"MAX"),@"0",_marketModel.targetCoin];
            _maxValue = @"0";
            if (_maxValue.floatValue == 0) {
                _silder.enabled = NO;
            }
        }else{
            NSString *temMax = [GlenTools amout:textField.text total:_balance scale:6];
            _maxValue = temMax;
            if (_maxValue.floatValue == 0) {
                _silder.enabled = NO;
            }
            _max.text = [NSString stringWithFormat:@" %@%@ %@",kLocalizedString(@"MAX", @"MAX"),@"0",_marketModel.targetCoin];
        }
    }else if (textField == _amount){
        
    }
    NSString *temVom = [GlenTools total:_price.text amount:_amount.text scale:6];
    _volume.text = [NSString stringWithFormat:@"%@ %@ %@",kLocalizedString(@"VOLUME", @"交易量"),temVom,_marketModel.marketCoin];
    if (self.upData) {
        self.upData(_price.text, _amount.text, _maxValue);
    }
}
- (void)setType:(NSInteger)type market:(Markets *)marketModel bestPrice:(exchangeDetail *)best select:(exchangeDetail *)select{
    _marketModel = marketModel;
    _type = type;
    if (best) {
        _best.text = [NSString stringWithFormat:@"Best Price %@",best.price];
    }
    if (select) {
        _price.text = select.price;
    }
    _fee.text = [NSString stringWithFormat:@"%@ %@",kLocalizedString(@"FEE", @"手续费"),marketModel.fee];
    if (type == 0) {
        _exType.text = [NSString stringWithFormat:@"%@ %@",kLocalizedString(@"BUY", @"交易量"),marketModel.targetCoin];
        _avaiType.text = [NSString stringWithFormat:@"%@ %@",kLocalizedString(@"AVAILABLE", @""),marketModel.marketCoin];
        _balance = [self getBalanece:marketModel.marketCoin];
        if (_price.text.floatValue > 0) {
            NSString *temMax = [GlenTools amout:_price.text total:_balance scale:6];
            _max.text = [NSString stringWithFormat:@" %@%@ %@",kLocalizedString(@"MAX", @"MAX"),temMax,_marketModel.targetCoin];
            _maxValue = temMax;
            if (_maxValue.floatValue == 0) {
                _silder.enabled = NO;
            }
        }
        
    }else if (type == 1) {
        _exType.text = [NSString stringWithFormat:@"%@ %@",kLocalizedString(@"SELL", @"交易量"),marketModel.targetCoin];
        _avaiType.text = [NSString stringWithFormat:@"%@ %@",kLocalizedString(@"AVAILABLE", @"当前可用"),marketModel.targetCoin];
        _balance = [self getBalanece:marketModel.targetCoin];
        _max.text = [NSString stringWithFormat:@" %@%@ %@",kLocalizedString(@"MAX", @"MAX"),_balance,_marketModel.targetCoin];
        _maxValue = _balance;
        if (_maxValue.floatValue == 0) {
            _silder.enabled = NO;
        }
    }
    NSString *temVom = [GlenTools total:_price.text amount:_amount.text scale:6];
    _volume.text = [NSString stringWithFormat:@"%@ %@ %@",kLocalizedString(@"VOLUME", @"交易量"),temVom,_marketModel.marketCoin];
    _avaiValue.text = _balance;

    if (self.upData) {
        self.upData(_price.text, _amount.text, _maxValue);
    }
    
}
- (NSString *)getBalanece:(NSString *)type{
    for (Coins *coin in [UerManger shareUserManger].coins) {
        if ([coin._id isEqualToString:type]) {
            if (coin.balance) {
                return [GlenTools formatString:coin.balance.free scale:4];
            }else{
                return [GlenTools formatString:@"0" scale:4];
            }
        }
    }
    return @"0.00";
}
@end
