//
//  GlenBuyCollectionViewCell.h
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExchangeModel.h"

@interface GlenBuyCollectionViewCell : UICollectionViewCell

@property (nonatomic, copy) void(^upData)(NSString *price, NSString *amount,NSString *max);

- (void)setType:(NSInteger)type market:(Markets *)marketModel bestPrice:(exchangeDetail *)best select:(exchangeDetail *)select;

@end
