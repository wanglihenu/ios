//
//  GlenExchangeRataCollectionReusableView.m
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenExchangeRataCollectionReusableView.h"

@interface GlenExchangeRataCollectionReusableView ()
@property (weak, nonatomic) IBOutlet UILabel *title;

@end

@implementation GlenExchangeRataCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setModel:(HomeModel *)model{
    
    _model = model;
    if (model == nil) {
        _title.text = @"0.00";
    }else{
        _title.text = model.close;
    }
    
}

@end
