//
//  GlenExchangeKlineSelectReusableView.h
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlenExchangeKlineSelectReusableView : UICollectionReusableView

@property (nonatomic, copy) void(^openAsset)(NSInteger index);
@property (nonatomic, assign) NSInteger type;///<1:首页 2:买 3:卖

@end
