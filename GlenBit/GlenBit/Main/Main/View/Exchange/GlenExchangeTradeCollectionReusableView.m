//
//  GlenExchangeTradeCollectionReusableView.m
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenExchangeTradeCollectionReusableView.h"

@interface GlenExchangeTradeCollectionReusableView ()
@property (weak, nonatomic) IBOutlet UILabel *titltelab;

@end

@implementation GlenExchangeTradeCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setT:(NSString *)t{
    _titltelab.text = t;
}
@end
