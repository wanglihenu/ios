//
//  GlenExchangeCollectionViewCell.h
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExchangeModel.h"

@interface GlenExchangeCollectionViewCell : UICollectionViewCell

@property (nonatomic, assign) NSInteger type;
@property (nonatomic, strong) exchangeDetail *detail;

@end
