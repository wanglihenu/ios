//
//  EWebCollectionViewCell.h
//  JBT
//
//  Created by Lee on 2018/4/3.
//  Copyright © 2018年 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EWebCollectionViewCell : UICollectionViewCell

- (void)setWebUrl:(NSString *)url allCor:(BOOL)allCor;
- (void)setFunctionName:(NSString *)functionName js:(NSString *)js;
@end
