//
//  GlenExchangeKlineSelectReusableView.m
//  GlenBit
//
//  Created by Lee on 2019/1/9.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenExchangeKlineSelectReusableView.h"

@interface GlenExchangeKlineSelectReusableView ()
@property (weak, nonatomic) IBOutlet UIButton *tab1;
@property (weak, nonatomic) IBOutlet UIButton *tab2;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineCenter;
@property (weak, nonatomic) IBOutlet UIView *line;
@property (strong, nonatomic)  UIButton *selectBtn;

@end

@implementation GlenExchangeKlineSelectReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}
- (void)setType:(NSInteger)type{
    _type = type;
    if (type == 2) {
        [_tab1 setTitle:@"Depth" forState:UIControlStateNormal];
        [_tab2 setTitle:@"Order book" forState:UIControlStateNormal];
    }
}
- (IBAction)select:(UIButton *)sender {
    if (_selectBtn == sender) {
        return;
    }
    _selectBtn.selected = NO;
    sender.selected = YES;
    _selectBtn = sender;
    _line.centerX = sender.centerX;
    if (self.openAsset) {
        self.openAsset(sender.tag - 10);
    }
}

@end
