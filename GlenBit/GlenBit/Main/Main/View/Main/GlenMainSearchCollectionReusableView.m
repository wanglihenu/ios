//
//  GlenMainSearchCollectionReusableView.m
//  GlenBit
//
//  Created by Lee on 2019/1/4.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenMainSearchCollectionReusableView.h"
#import <YBPopupMenu/YBPopupMenu.h>

@interface GlenMainSearchCollectionReusableView ()
@property (weak, nonatomic) IBOutlet SkyRadiusView *selectView;
@property (weak, nonatomic) IBOutlet UIView *searchBack;

@property (weak, nonatomic) IBOutlet UITextField *searchText;
@property (weak, nonatomic) IBOutlet UILabel *type;

@end

@implementation GlenMainSearchCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self config];
    [_searchText addTarget:self action:@selector(changedTextField:) forControlEvents:UIControlEventEditingChanged];
}
- (void)setCoin:(Coins *)coin select:(NSIndexPath *)index{
    _searchText.placeholder = kLocalizedString(@"SEARCH", @"");
    if (index.row == 0) {
        _type.text = kLocalizedString(@"ALL", @"所有");
        
    }else{
        _type.text = coin._id;
    }
}
-(void)changedTextField:(UITextField *)textField
{
    if (self.search) {
        self.search(textField.text);
    }
}
- (IBAction)tap:(id)sender {
    NSLog(@"ssss");
    if (self.select) {
        self.select(self);
    }
}

- (void)config{
    self.selectView.layer.borderColor = [UIColor colorWithHexString:@"#BDBFC2"].CGColor;
    self.selectView.layer.borderWidth = 1;
    self.selectView.layer.cornerRadius = 20;
    self.searchBack.layer.cornerRadius = 20;
    
    UIButton *search = [UIButton buttonWithType:UIButtonTypeCustom];
    search.frame = CGRectMake(0, 0, 18, 18);
    [search setImage:[UIImage imageNamed:@"home_search"] forState:UIControlStateNormal];
    self.searchText.rightView = search;
    self.searchText.rightViewMode = UITextFieldViewModeAlways;
    
    UITapGestureRecognizer *selectTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
    [self.selectView addGestureRecognizer:selectTap];
    
}
@end
