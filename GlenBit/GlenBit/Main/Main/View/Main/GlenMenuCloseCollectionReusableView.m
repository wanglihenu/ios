//
//  GlenMenuCloseCollectionReusableView.m
//  GlenBit
//
//  Created by Lee on 2019/1/4.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenMenuCloseCollectionReusableView.h"

@interface GlenMenuCloseCollectionReusableView ()

@end

@implementation GlenMenuCloseCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (IBAction)close:(id)sender {
    if (self.closeRight) {
        self.closeRight();
    }
}

@end
