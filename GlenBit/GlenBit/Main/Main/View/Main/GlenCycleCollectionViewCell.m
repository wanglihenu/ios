//
//  HomeCycleCollectionViewCell.m
//  JBT
//
//  Created by Lee on 2017/11/15.
//  Copyright © 2017年 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenCycleCollectionViewCell.h"
#import <SDCycleScrollView/SDCycleScrollView.h>


@interface GlenCycleCollectionViewCell ()<SDCycleScrollViewDelegate>

@property (nonatomic, strong) SDCycleScrollView *SDCycleScrollView;

@end

@implementation GlenCycleCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self interFaceLayOut];
    }
    return self;
}
- (void)interFaceLayOut {
    
    [self.contentView addSubview:self.SDCycleScrollView];
    
}
- (SDCycleScrollView *)SDCycleScrollView{
    if (!_SDCycleScrollView) {
        NSArray *imageArr = [NSArray arrayWithObjects:@"banner1", nil];
        _SDCycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, self.width, self.height) shouldInfiniteLoop:YES imageNamesGroup:imageArr];
        _SDCycleScrollView.delegate = self;
        _SDCycleScrollView.bannerImageViewContentMode = UIViewContentModeScaleToFill;
//        _SDCycleScrollView.autoScrollTimeInterval = 3;
        _SDCycleScrollView.autoScroll = NO;
        _SDCycleScrollView.backgroundColor=[UIColor whiteColor];
        _SDCycleScrollView.placeholderImage = [UIImage imageNamed:@"banner1"];
        _SDCycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        _SDCycleScrollView.currentPageDotColor = [UIColor colorWithHexString:@"#F8D900"];
        _SDCycleScrollView.pageDotColor = [UIColor colorWithHexString:@"#353C41"];
        _SDCycleScrollView.pageControlDotSize = CGSizeMake(8, 8);
        _SDCycleScrollView.pageControlBottomOffset = 36;
    }
    return _SDCycleScrollView;
}
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{

}
- (void)setupImageUrls:(NSMutableArray *)arr{
//    [_dataArr removeAllObjects];
//    MJWeakSelf;
//    __block NSMutableArray *arr1 = [NSMutableArray new];
//    [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        Banners *model = (Banners *)obj;
//        [weakSelf.dataArr addObject:model];
//        [arr1 addObject:model.imageUrl];
//    }];
//    self.SDCycleScrollView.imageURLStringsGroup = arr1;
}
- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray new];
    }
    return _dataArr;
}

@end
