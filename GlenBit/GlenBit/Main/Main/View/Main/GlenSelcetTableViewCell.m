//
//  GlenSelcetTableViewCell.m
//  GlenBit
//
//  Created by Lee on 2019/1/4.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenSelcetTableViewCell.h"

@interface GlenSelcetTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIView *line;
@property (weak, nonatomic) IBOutlet UIImageView *icon;

@end

@implementation GlenSelcetTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setTitle:(Coins *)string selectIndex:(NSIndexPath *)index current:(NSIndexPath *)current{
    if (current.row == 0) {
        if (index) {
            _title.text = string._id;
        }else{
            _title.text = kLocalizedString(@"ALL", @"所有");
        }
        if (current.row == index.row) {
            _line.hidden = NO;
        }else{
            _line.hidden = YES;
        }

    }else{
        _title.text = string._id;
        _icon.hidden = YES;
        if (current.row == index.row) {
            _line.hidden = NO;
        }else{
            _line.hidden = YES;
        }
        if (!index) {
            _line.hidden = YES;
        }
    }
        
    
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
