//
//  GlenMainCollectionViewCell.m
//  GlenBit
//
//  Created by Lee on 2019/1/4.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenMainCollectionViewCell.h"

@interface GlenMainCollectionViewCell ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *width;
@property (weak, nonatomic) IBOutlet UIButton *arrow;
@property (weak, nonatomic) IBOutlet UILabel *pair;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *amount;
@property (weak, nonatomic) IBOutlet UILabel *pesent;
@property (weak, nonatomic) IBOutlet UILabel *usdt;
@property (weak, nonatomic) IBOutlet UIView *pesentView;


@end

@implementation GlenMainCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setType:(NSInteger)type{
    _type = type;
    if (type == 0) {
        _width.constant = 0;
        _arrow.hidden = YES;
        _pesentView.hidden = NO;
    }else if (type == 1){
        _width.constant = 40;
        _arrow.hidden = NO;
        _pesentView.hidden = YES;
    }
}
- (void)setBalanceModel:(Coins *)balanceModel{
    _balanceModel = balanceModel;
    _pair.text = [NSString stringWithFormat:@"%@(%@)",balanceModel._id,balanceModel.name];
    _amount.text = [GlenTools addtotal:balanceModel.balance.locked?:@"0" amount:balanceModel.balance.free?:@"0" scale:6];
    _price.text = [GlenTools formatString:balanceModel.balance.free?:@"0" scale:6];
    _usdt.text = [GlenTools formatString:balanceModel.balance.locked?:@"0" scale:6];
}
- (void)setHomeModel:(HomeModel *)homeModel{
    _homeModel = homeModel;
    _pair.text = homeModel.pair;
    _price.text = [GlenTools formatString:homeModel.close scale:6];
    _amount.text = [GlenTools formatString:homeModel.volume scale:6];
    _pesent.text = [GlenTools formatPesent:homeModel.change scale:2];
    if (homeModel.change.doubleValue>=0) {
        _pesentView.backgroundColor = [UIColor colorWithHexString:@"#00ACDC"];
    }else{
        _pesentView.backgroundColor = [UIColor colorWithHexString:@"#F04A5D"];
    }
}
- (IBAction)openAction:(UIButton *)sender {
    if (self.openAsset) {
        self.openAsset(sender.tag - 10);
    }
}


@end
