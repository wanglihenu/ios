//
//  GlenSelcetTableViewCell.h
//  GlenBit
//
//  Created by Lee on 2019/1/4.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlenSelcetTableViewCell : UITableViewCell

- (void)setTitle:(Coins *)string selectIndex:(NSIndexPath *)index current:(NSIndexPath *)current;

@end
