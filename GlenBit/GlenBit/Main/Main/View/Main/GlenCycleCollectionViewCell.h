//
//  HomeCycleCollectionViewCell.h
//  JBT
//
//  Created by Lee on 2017/11/15.
//  Copyright © 2017年 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlenCycleCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) NSMutableArray *dataArr;

- (void)setupImageUrls:(NSMutableArray *)arr;

@end
