//
//  GlenSelectView.m
//  GlenBit
//
//  Created by Lee on 2019/1/4.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenSelectView.h"
#import "GlenSelcetTableViewCell.h"

#define YBMainWindow  [UIApplication sharedApplication].keyWindow

@interface GlenSelectView ()<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic, strong) UIView      * menuBackView;

@end

@implementation GlenSelectView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
        _menuBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size_width, size_height)];
        _menuBackView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
        _menuBackView.alpha = 0;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(touchOutSide)];
        [_menuBackView addGestureRecognizer: tap];
        self.alpha = 0;
        self.backgroundColor = [UIColor clearColor];
        [self addSubview:self.tableView];
    }
    return self;
}
- (void)show
{
    GlenWeakSelf;
    
    [YBMainWindow addSubview:_menuBackView];
    [YBMainWindow addSubview:self];

    self.layer.affineTransform = CGAffineTransformMakeScale(0.1, 0.1);
    [UIView animateWithDuration: 0.25 animations:^{
        self.layer.affineTransform = CGAffineTransformMakeScale(1.0, 1.0);
        self.alpha = 1;
        weakSelf.menuBackView.alpha = 1;
    } completion:^(BOOL finished) {

    }];
}
- (void)touchOutSide
{
    [self dismiss];
}
- (void)dismiss
{
    GlenWeakSelf;
    [UIView animateWithDuration: 0.25 animations:^{
        self.layer.affineTransform = CGAffineTransformMakeScale(0.1, 0.1);
        self.alpha = 0;
        weakSelf.menuBackView.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [weakSelf.menuBackView removeFromSuperview];
    }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 64;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GlenSelcetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GlenSelcetTableViewCell" forIndexPath:indexPath];
    return cell;
}
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, size_width, size_height - StatusBar_height  - 44) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"GlenSelcetTableViewCell" bundle:nil] forCellReuseIdentifier:@"GlenSelcetTableViewCell"];
    }
    return _tableView;
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
