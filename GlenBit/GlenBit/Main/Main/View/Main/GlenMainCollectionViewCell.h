//
//  GlenMainCollectionViewCell.h
//  GlenBit
//
//  Created by Lee on 2019/1/4.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeModel.h"

@interface GlenMainCollectionViewCell : UICollectionViewCell

@property (nonatomic, assign) NSInteger type;
@property (nonatomic, copy) void(^openAsset)(NSInteger index);
@property (nonatomic, strong) HomeModel *homeModel;

@property (nonatomic, strong) Coins *balanceModel;


- (void)setSelectIndex:(NSIndexPath *)index;

@end
