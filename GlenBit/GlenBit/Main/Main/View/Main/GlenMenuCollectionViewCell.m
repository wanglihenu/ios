//
//  GlenMenuCollectionViewCell.m
//  GlenBit
//
//  Created by Lee on 2019/1/4.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenMenuCollectionViewCell.h"

@interface GlenMenuCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *title;

@end

@implementation GlenMenuCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setStr:(NSString *)str{
    _title.text = str;
}
@end
