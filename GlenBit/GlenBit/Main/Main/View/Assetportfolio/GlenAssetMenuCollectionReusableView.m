//
//  GlenAssetMenuCollectionReusableView.m
//  GlenBit
//
//  Created by Lee on 2019/1/5.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenAssetMenuCollectionReusableView.h"

@interface GlenAssetMenuCollectionReusableView ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet SkyRadiusView *selectView;
@property (weak, nonatomic) IBOutlet UIView *searchBack;

@property (weak, nonatomic) IBOutlet UITextField *searchText;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *selectViews;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *selectLab;
@property (weak, nonatomic) IBOutlet UILabel *type;

@property (weak, nonatomic) IBOutlet UILabel *labStr;
@property (weak, nonatomic) IBOutlet UILabel *lab2Str;
@property (weak, nonatomic) IBOutlet UILabel *lab3Str;


@end

@implementation GlenAssetMenuCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self config];
    [_searchText addTarget:self action:@selector(changedTextField:) forControlEvents:UIControlEventEditingChanged];
    _searchText.delegate = self;
}
- (void)setCoin:(Coins *)coin select:(NSIndexPath *)index seacrh:(NSString *)search{
    _searchText.placeholder = kLocalizedString(@"SEARCH", @"");
    _labStr.text = kLocalizedString(@"MY_PORTFOLIO", @"");
    _lab2Str.text = [NSString stringWithFormat:@"%@%@",kLocalizedString(@"DEPOSIT", @""),kLocalizedString(@"RECORDS", @"")];
    _lab3Str.text = [NSString stringWithFormat:@"%@%@",kLocalizedString(@"WITHDRAW", @""),kLocalizedString(@"RECORDS", @"")];
    if (index.row == 0) {
        _type.text = kLocalizedString(@"ALL", @"所有");
        
    }else{
        _type.text = coin._id;
    }
    _searchText.text = search;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (self.search) {
        self.search(textField.text);
    }
    [textField resignFirstResponder];
    return YES;
}
-(void)changedTextField:(UITextField *)textField
{
    if (self.search && textField.text.length == 0) {
        self.search(textField.text);
    }
}
- (IBAction)tap:(id)sender {
    NSLog(@"ssss");
    if (self.select) {
        self.select(self);
    }
}
- (void)setCurrentIndex:(NSInteger)currentIndex{
    _currentIndex = currentIndex;
    for (int i = 0; i < _selectLab.count; i++) {
        UILabel *tempLab = _selectLab[i];
        UIView *tempView = _selectViews[i];
        if (i == _currentIndex) {
            tempLab.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
            tempView.backgroundColor = [UIColor colorWithHexString:@"#353B41"];
            tempView.layer.borderWidth = 0;
        }else{
            tempLab.textColor = [UIColor colorWithHexString:@"#353B41"];
            tempView.backgroundColor = [UIColor colorWithHexString:@"#E8E8E8"];
            tempView.layer.borderColor = [UIColor colorWithHexString:@"#B4B7BB"].CGColor;
            tempView.layer.borderWidth = 1;
        }
    }
    
}
- (void)seletap:(UITapGestureRecognizer *)sender{
    self.currentIndex = [sender view].tag - 10;
    if (self.changeCurrent) {
        self.changeCurrent(_currentIndex);
    }
}
- (void)config{
    self.selectView.layer.borderColor = [UIColor colorWithHexString:@"#BDBFC2"].CGColor;
    self.selectView.layer.borderWidth = 1;
    self.selectView.layer.cornerRadius = 20;
    self.searchBack.layer.cornerRadius = 20;
    
    UIButton *search = [UIButton buttonWithType:UIButtonTypeCustom];
    search.frame = CGRectMake(0, 0, 18, 18);
    [search setImage:[UIImage imageNamed:@"home_search"] forState:UIControlStateNormal];
    self.searchText.rightView = search;
    self.searchText.rightViewMode = UITextFieldViewModeAlways;
    
    UITapGestureRecognizer *selectTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
    [self.selectView addGestureRecognizer:selectTap];
    for (int i = 0; i < _selectViews.count; i++) {
        UIView *tempView = _selectViews[i];
        UITapGestureRecognizer *Tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(seletap:)];
        [tempView addGestureRecognizer:Tap];
    }
}

@end
