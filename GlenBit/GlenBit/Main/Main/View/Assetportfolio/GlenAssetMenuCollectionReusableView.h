//
//  GlenAssetMenuCollectionReusableView.h
//  GlenBit
//
//  Created by Lee on 2019/1/5.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlenAssetMenuCollectionReusableView : UICollectionReusableView

@property (nonatomic, copy) void(^changeCurrent)(NSInteger index);
@property (nonatomic, copy) void(^select)(UIView *view);
@property (nonatomic, copy) void(^search)(NSString *search);
@property (nonatomic, assign) NSInteger currentIndex;
- (void)setCoin:(Coins *)coin select:(NSIndexPath *)index seacrh:(NSString *)search;

@end
