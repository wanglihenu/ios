//
//  GlenAssetDepositView.m
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenAssetDepositView.h"
#import "HMScanner.h"

@interface GlenAssetDepositView ()
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UIImageView *address;
@property (weak, nonatomic) IBOutlet UILabel *tips;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UILabel *titl;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@property (weak, nonatomic) IBOutlet UILabel *addressStrLab;
@property (strong, nonatomic)  NSString *min;

@end

@implementation GlenAssetDepositView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"GlenAssetDepositView" owner:self options:nil] firstObject];
        self.frame = frame;
        self.addressView.layer.borderColor =  [UIColor colorWithHexString:@"#353B41"].CGColor;
        self.addressView.layer.borderWidth = 1;
        
        _titl.text = _coin._id;
        _titleLab.text = kLocalizedString(@"DEPOSIT", @"");
        _addressStrLab.text = kLocalizedString(@"ADDRESS", @"");
    }
    return self;
}
- (IBAction)tap:(id)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = _addressStr;
    [[Toast makeUpText:@"复制成功"] showWithType:ShortTime];
}
- (void)setAddressStr:(NSString *)addressStr{
    NSString *tem = [addressStr stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    tem = [tem stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    _addressStr = tem;
    GlenWeakSelf;
    [HMScanner qrImageWithString:_addressStr avatar:nil completion:^(UIImage *image) {
        weakSelf.address.image = image;
    }];
    _addressLab.text = _addressStr;
}
- (void)setCoin:(Coins *)coin{
    
    
    if ([coin._id.lowercaseString isEqualToString:@"usdt"]) {
        _min = @"1.0";
    }else if ([coin._id.lowercaseString isEqualToString:@"btc"]){
        _min = @"0.001";
    }else if ([coin._id.lowercaseString isEqualToString:@"eth"]){
        _min = @"0.01";
    }else if ([coin._id.lowercaseString isEqualToString:@"xrp"]){
        _min = @"0.1";
    }else if ([coin._id.lowercaseString isEqualToString:@"ltc"]){
        _min = @"0.001";
    }else{
        _min = @"1.0";
    }
    
    
    
    
    
    
    _coin = coin;
    _titl.text = _coin._id;
    _tips.text = [NSString stringWithFormat:@"%@%@%@%@ %@%@",kLocalizedString(@"TIP1", @"TIP1"),_coin._id,kLocalizedString(@"TIP2", @"TIP2"),_min,_coin._id,kLocalizedString(@"TIP3", @"TIP3")];
}
- (IBAction)close:(id)sender {
    if (self.closeSelf) {
        self.closeSelf();
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
