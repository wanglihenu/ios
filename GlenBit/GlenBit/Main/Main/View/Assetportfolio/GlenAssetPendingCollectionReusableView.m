//
//  GlenAssetPendingCollectionReusableView.m
//  GlenBit
//
//  Created by Lee on 2019/1/5.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenAssetPendingCollectionReusableView.h"

@interface GlenAssetPendingCollectionReusableView ()

@property (weak, nonatomic) IBOutlet SkyRadiusView *selectView;
@property (weak, nonatomic) IBOutlet UILabel *type;
@property (weak, nonatomic) IBOutlet UILabel *pendingStr;

@end

@implementation GlenAssetPendingCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self config];
}
- (void)setCoin:(Coins *)coin select:(NSIndexPath *)index{
    _pendingStr.text = kLocalizedString(@"PENDING_DEPOSIT", @"待确认充值");
    if (index.row == 0) {
        _type.text = kLocalizedString(@"ALL", @"所有");
        
    }else{
        _type.text = coin._id;
    }
}
- (void)config{
    self.selectView.layer.borderColor = [UIColor colorWithHexString:@"#BDBFC2"].CGColor;
    self.selectView.layer.borderWidth = 1;
    self.selectView.layer.cornerRadius = 20;
    UITapGestureRecognizer *selectTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
    [self.selectView addGestureRecognizer:selectTap];
    
}
- (IBAction)tap:(id)sender {
    NSLog(@"ssss");
    if (self.select) {
        self.select(self);
    }
}
@end
