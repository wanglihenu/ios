//
//  GlenWithDrawView.m
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenWithDrawView.h"
#import "MBProgressHUD.h"

@interface GlenWithDrawView ()
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UIView *amountView;
@property (weak, nonatomic) IBOutlet UIView *codeView;

@property (weak, nonatomic) IBOutlet UILabel *impotant;

@property (weak, nonatomic) IBOutlet UITextField *address;
@property (weak, nonatomic) IBOutlet UITextField *amont;
@property (weak, nonatomic) IBOutlet UILabel *detail;
@property (weak, nonatomic) IBOutlet UITextField *googleCodel;
@property (weak, nonatomic) IBOutlet UILabel *coinLab;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *addresTr;
@property (weak, nonatomic) IBOutlet UILabel *amountStr;
@property (weak, nonatomic) IBOutlet UILabel *googleStr;


@end

@implementation GlenWithDrawView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"GlenWithDrawView" owner:self options:nil] firstObject];
        self.frame = frame;
        
        self.addressView.layer.borderColor = self.amountView.layer.borderColor = self.codeView.layer.borderColor = [UIColor colorWithHexString:@"#353B41"].CGColor;
        self.addressView.layer.borderWidth = self.amountView.layer.borderWidth = self.codeView.layer.borderWidth =1;
        _titleLab.text = kLocalizedString(@"WITHDRAW", @"");
        
        _addresTr.text = kLocalizedString(@"ADDRESS", @"");
        _amountStr.text = kLocalizedString(@"AMOUNT", @"");
        _googleStr.text = kLocalizedString(@"ENTERGOOGLECODE", @"ENTERGOOGLECODE");
        _googleCodel.placeholder = kLocalizedString(@"ENTERGOOGLECODE", @"ENTERGOOGLECODE");
        
    }
    return self;
}
- (void)setCoin:(NSString *)coin{
    _coin = coin;
    _coinLab.text = coin;
}
- (void)setModel:(TixianInfoModel *)model{
    _model = model;
    
    _impotant.text = [NSString stringWithFormat:@"%@%@ %@。%@",kLocalizedString(@"TIP4", @"TIP4"),model.minWithdraw,_coin,kLocalizedString(@"TIP5", @"TIP5")];
    _detail.text = [NSString stringWithFormat:@"%@ %@ %@\r%@ %@ %@",kLocalizedString(@"AVAILABLE", @""),model.available,_coin,model.fee,_coin,kLocalizedString(@"FEE", @"FEE")];
    
    
}
- (IBAction)close:(id)sender {
    if (self.closeWithDraw) {
        self.closeWithDraw();
    }
}
- (IBAction)tijiao:(id)sender {
    
    if (NULLString(_address.text)) {
        [[Toast makeUpText:@"提现地址不能为空"] showWithType:ShortTime];
        return;
    }
    if (_amont.text.floatValue < _model.minWithdraw.floatValue) {
        [[Toast makeUpText:@"提现数额不能小于最小提现额度!"] showWithType:ShortTime];
        return;
    }
//    if (_amont.text.floatValue > _model.available.floatValue) {
//        [[Toast makeUpText:@"提现数额超出了可用余额!"] showWithType:ShortTime];
//        return;
//    }
    if (NULLString(_googleCodel.text)) {
        [[Toast makeUpText:@"谷歌二次验证不能为空"] showWithType:ShortTime];
        return;
    }
    GlenWeakSelf;
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    [[GlenHttpTool shareInstance] withdrawAddress:_address.text coin:_coin amount:_amont.text token:_googleCodel.text Completion:^(NSString *succes) {
        [MBProgressHUD hideHUDForView:weakSelf animated:YES];
        if (weakSelf.closeWithDraw) {
            weakSelf.closeWithDraw();
        }
    } fail:^(NSString *error) {
        [MBProgressHUD hideHUDForView:weakSelf animated:YES];
        [[Toast makeUpText:error] showWithType:ShortTime];
    }];
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
