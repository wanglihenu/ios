//
//  GlenAssetPendingCollectionViewCell.m
//  GlenBit
//
//  Created by Lee on 2019/1/5.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenAssetPendingCollectionViewCell.h"

@interface GlenAssetPendingCollectionViewCell ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *width;
@property (weak, nonatomic) IBOutlet UIButton *arrow;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *amout;
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *TXIS;
@property (weak, nonatomic) IBOutlet UILabel *bitType;


@end

@implementation GlenAssetPendingCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setType:(NSInteger)type{
    _type = type;
    if (type == 0) {
        _width.constant = 0;
        _arrow.hidden = YES;
    }else if (type == 1){
        _width.constant = 40;
        _arrow.hidden = NO;
    }
}
- (void)setTixian:(TixianModel *)tixian{
    _tixian = tixian;
    
    if (NULLString(tixian.updatedAt) && NULLString(tixian.txid)) {
        _status.text = kLocalizedString(@"审核中", @"审核中");
        _time.text = [NSString StringFromTimestamp:tixian.createdAt];
    }
    if(!NULLString(tixian.updatedAt) && NULLString(tixian.txid)){
        _status.text = kLocalizedString(@"处理中", @"处理中");
        _time.text = [NSString StringFromTimestamp:tixian.updatedAt];
    }
    if(!NULLString(tixian.txid)){
        _status.text = kLocalizedString(@"已汇出", @"已汇出");
        _time.text = [NSString StringFromTimestamp:tixian.updatedAt];
    }
    _amout.text = tixian.amount;
    _bitType.text = tixian.coin;
    
}
- (void)setChongzhi:(ChongzhiModel *)chongzhi{
    _chongzhi = chongzhi;
    if (NULLString(chongzhi.updatedAt)) {
        _status.text = kLocalizedString(@"待确认", @"待确认");
        _time.text = [NSString StringFromTimestamp:chongzhi.createdAt];
    }else{
        _status.text = kLocalizedString(@"充值成功", @"充值成功");
        _time.text = [NSString StringFromTimestamp:chongzhi.updatedAt];
    }
    _amout.text = chongzhi.amount;
    _address.text = chongzhi.DepositAddress;
    _TXIS.text = chongzhi.txid;
    _bitType.text = chongzhi.coin;
}
@end
