//
//  GlenAssetTotalCollectionViewCell.m
//  GlenBit
//
//  Created by Lee on 2019/1/5.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenAssetTotalCollectionViewCell.h"

@interface GlenAssetTotalCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *total;
@property (weak, nonatomic) IBOutlet UILabel *btcValue;
@property (weak, nonatomic) IBOutlet UILabel *scale;
@property (weak, nonatomic) IBOutlet UILabel *totalStr;
@property (weak, nonatomic) IBOutlet UILabel *tateStr;

@end

@implementation GlenAssetTotalCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

}
- (void)setBalanece:(NSArray *)balanece{
    _totalStr.text = kLocalizedString(@"ASSET_VALUE", @"我的资产");
    _tateStr.text = kLocalizedString(@"CURRENT_FX", @"");
    _balanece = balanece;
    NSString *usd = [GlenTools totalBalance:[UerManger shareUserManger].marke_price balanse:_balanece coin:@"USD"scale:2];
    _total.text = [NSString stringWithFormat:@"%@ USD",usd];
    NSString *btc = [GlenTools totalBalance:[UerManger shareUserManger].marke_price balanse:_balanece coin:@"BTC"scale:8];
    _btcValue.text = [NSString stringWithFormat:@"≈%@",btc];

    _scale.text = [NSString stringWithFormat:@"BTC/USD = %@",[GlenTools formatString:[GlenTools rateFrom:@"BTC" to:@"USD" lastRate:nil] scale:2]];
}
@end
