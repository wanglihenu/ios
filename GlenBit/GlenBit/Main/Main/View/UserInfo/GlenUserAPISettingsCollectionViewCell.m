//
//  GlenUserAPISettingsCollectionViewCell.m
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenUserAPISettingsCollectionViewCell.h"

@implementation GlenUserAPISettingsCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (IBAction)openAction:(UIButton *)sender {
    if (self.openAsset) {
        self.openAsset(sender.tag - 10);
    }
}
@end
