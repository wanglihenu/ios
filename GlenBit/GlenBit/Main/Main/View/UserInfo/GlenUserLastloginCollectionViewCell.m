//
//  GlenUserLastloginCollectionViewCell.m
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenUserLastloginCollectionViewCell.h"

@interface GlenUserLastloginCollectionViewCell()
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *ip;

@end

@implementation GlenUserLastloginCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setModel:(recentLogins *)model{
    _model = model;
    _time.text = [NSString StringFromTimestamp:_model.createdAt];
    NSArray *tem = [_model.ip componentsSeparatedByString:@","];
    _ip.text = tem[0];
    
}
@end
