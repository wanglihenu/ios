//
//  GlenUserSecretDeletView.m
//  GlenBit
//
//  Created by Lee on 2019/1/8.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenUserSecretDeletView.h"


@implementation GlenUserSecretDeletView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"GlenUserSecretDeletView" owner:self options:nil] firstObject];
        self.frame = frame;
    }
    return self;
}
- (IBAction)clos:(UIButton *)sender {
    if (self.closeSelf) {
        self.closeSelf(sender.tag - 10);
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
