//
//  GlenUserSecurityCollectionViewCell.m
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenUserSecurityCollectionViewCell.h"

@interface GlenUserSecurityCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *detaiLab;

@end

@implementation GlenUserSecurityCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setIndex:(NSIndexPath *)index{
    switch (index.section) {
        case 3:
        {
            switch (index.row) {
                case 0:
                {
                    _titleLab.text = kLocalizedString(@"SMS_VERIFY", @"短信认证");
                    _detaiLab.text = [UerManger shareUserManger].userModel.isSmsVerified?kLocalizedString(@"ENABLED", @"已启用"):kLocalizedString(@"DISABLED", @"未启用");
                }
                    break;
                case 1:
                {
                    _titleLab.text = kLocalizedString(@"GOOGLE_TWO_STEP", @"谷歌二次认证");
                    _detaiLab.text = [UerManger shareUserManger].userModel.isTwoStepEnabled?kLocalizedString(@"ENABLED", @"已启用"):kLocalizedString(@"DISABLED", @"未启用");;
                }
                    break;
            }
        }
            break;
        default:
            break;
    }
}
@end
