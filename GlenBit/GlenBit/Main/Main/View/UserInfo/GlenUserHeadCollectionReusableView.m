//
//  GlenUserHeadCollectionReusableView.m
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenUserHeadCollectionReusableView.h"

@interface GlenUserHeadCollectionReusableView ()
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *ip;
@property (weak, nonatomic) IBOutlet UILabel *atStr;

@property (weak, nonatomic) IBOutlet UILabel *ipStr;


@end

@implementation GlenUserHeadCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _atStr.text = kLocalizedString(@"LAST_LOGIN_AT", @"最后登录于");
    _ipStr.text = kLocalizedString(@"LAST_LOGIN_IP", @"最后登录时IP地址");
}
- (void)updataInfo{
    _atStr.text = kLocalizedString(@"LAST_LOGIN_AT", @"最后登录于");
    _ipStr.text = kLocalizedString(@"LAST_LOGIN_IP", @"最后登录时IP地址");
    UserModel *model = [UerManger shareUserManger].userModel;
    if ([UerManger shareUserManger].loginType == 0) {
        _name.text = model.email;
    }else{
        _name.text = model.phone;
    }
    recentLogins *login = model.sortRecenLogin[0];
    _time.text = [NSString StringFromTimestamp:login.createdAt];
    NSArray *tem = [login.ip componentsSeparatedByString:@","];
    _ip.text = tem[0];
    
}
@end
