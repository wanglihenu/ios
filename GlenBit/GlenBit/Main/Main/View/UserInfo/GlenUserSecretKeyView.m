//
//  GlenUserSecretKeyView.m
//  GlenBit
//
//  Created by Lee on 2019/1/8.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenUserSecretKeyView.h"

@interface GlenUserSecretKeyView ()
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UIView *amountView;
@end

@implementation GlenUserSecretKeyView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"GlenUserSecretKeyView" owner:self options:nil] firstObject];
        self.frame = frame;
        self.addressView.layer.borderColor = self.amountView.layer.borderColor = [UIColor colorWithHexString:@"#353B41"].CGColor;
        self.addressView.layer.borderWidth = self.amountView.layer.borderWidth = 1;
    }
    return self;
}
- (IBAction)close:(id)sender {
    if (self.closeSelf) {
        self.closeSelf();
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
