//
//  GlenAletView.m
//  GlenBit
//
//  Created by Lee on 2019/1/26.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenAletView.h"

@interface GlenAletView ()
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (copy, nonatomic)  confirm confirm;
@property (copy, nonatomic)  cancel cancel;
@property (weak, nonatomic) IBOutlet UIButton *canBtn;
@property (weak, nonatomic) IBOutlet UIButton *confiBtn;

@end

@implementation GlenAletView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"GlenAletView" owner:self options:nil] firstObject];
        self.frame = frame;
        [_confiBtn setTitle:kLocalizedString(@"Determine", @"确定") forState:UIControlStateNormal];
        [_canBtn setTitle:kLocalizedString(@"quxiao", @"取消") forState:UIControlStateNormal];
    }
    return self;
}
- (void)showTitle:(NSString *)title confirm:(confirm)confirm cancel:(cancel)cancel{
    _titleLab.text = title;
    _confirm = confirm;
    _cancel = cancel;
}
- (IBAction)cancleBtn:(id)sender {
    if (self.cancel) {
        self.cancel();
    }
}
- (IBAction)confirmBtn:(id)sender {
    if (self.confirm) {
        self.confirm();
    }
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
