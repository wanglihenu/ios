//
//  GlenUserSecretDeletView.h
//  GlenBit
//
//  Created by Lee on 2019/1/8.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlenUserSecretDeletView : UIView

@property (nonatomic, copy) void(^closeSelf)(NSInteger index);

@end
