//
//  GlenUserSectionCollectionReusableView.m
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenUserSectionCollectionReusableView.h"


@interface GlenUserSectionCollectionReusableView ()
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIButton *createBtn;

@end

@implementation GlenUserSectionCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setIndex:(NSIndexPath *)index{
    _createBtn.hidden = YES;
    if (index.section == 3) {
        _title.text = kLocalizedString(@"SECURITY", @"安全");
    }else if (index.section == 4){
        _title.text = kLocalizedString(@"LOGIN_HISTORY", @"登录历史");
    }else if (index.section == 5){
        _title.text = kLocalizedString(@"API_Settings", @"安全");
        _createBtn.hidden = NO;
        [_createBtn setTitle:kLocalizedString(@"Create_API_key", @"") forState:UIControlStateNormal];
    }
}
- (IBAction)creat:(id)sender {
    if (self.creatApi) {
        self.creatApi();
    }
}

@end
