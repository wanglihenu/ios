//
//  GlenUserInfoCollectionViewCell.m
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenUserInfoCollectionViewCell.h"

@interface GlenUserInfoCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *detaiLab;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;
@property (weak, nonatomic) IBOutlet UISwitch *switche;

@end

@implementation GlenUserInfoCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setIndex:(NSIndexPath *)index{
    _switche.hidden = YES;
    _arrowImage.hidden = NO;
    _detaiLab.hidden = NO;
    UserModel *model = [UerManger shareUserManger].userModel;
    switch (index.section) {
        case 0:
        {
            _titleLab.text = @"Sign-in Record";
            _detaiLab.hidden = YES;
        }
            break;
        case 1:
        {
            switch (index.row) {
                case 0:
                    {
                        if ([UerManger shareUserManger].loginType == 0) {
                            _titleLab.text = kLocalizedString(@"EMAIL", @"邮箱");
                            _detaiLab.text = model.email;
                        }else{
                            _titleLab.text = kLocalizedString(@"Phone", @"手机号");
                            _detaiLab.text = model.phone;
                        }
                        
                    }
                    break;
                case 1:
                {
                    _titleLab.text = kLocalizedString(@"PASSWORD", @"登录密码");
                    _detaiLab.text = @"**************";
                }
                    break;
                case 3:
                {
                    _titleLab.text = @"ID Verification";
                    _detaiLab.text = @"Not verified";
                }
                    break;
                case 2:
                {
                    _titleLab.text = kLocalizedString(@"LOGOUT", @"登出");
                    _detaiLab.text = @"";
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
        case 2:
            switch (index.row) {
                case 0:
                {
                    _titleLab.text = kLocalizedString(@"24H_WITHDRAW_LIMIT", @"24h提现额度");
                    _detaiLab.text = @"2.0 BTC";
                    _arrowImage.hidden = YES;
                    _switche.hidden = NO;
                }
                    break;
                case 1:
                {
                    _titleLab.text = kLocalizedString(@"Language", @"登录密码");
                    _detaiLab.text = @"";
                }
                    break;

                    
                default:
                    break;
            }
            break;
        case 4:
        {
            _titleLab.text = @"Trading Authentication";
            _detaiLab.text = @"Never\nLast change\n2018-08-18 16:35:38";
        }
            break;
        default:
            break;
    }
    
    
    
    
    
}




@end
