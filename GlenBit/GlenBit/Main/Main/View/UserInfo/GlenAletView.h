//
//  GlenAletView.h
//  GlenBit
//
//  Created by Lee on 2019/1/26.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^cancel)(void);

typedef void(^confirm)(void);

@interface GlenAletView : UIView

- (void)showTitle:(NSString *)title confirm:(confirm)confirm cancel:(cancel)cancel;


@end
