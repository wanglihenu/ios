//
//  GlenOrderCollectionViewCell.h
//  GlenBit
//
//  Created by Lee on 2019/2/14.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlenOrderCollectionViewCell : UICollectionViewCell

@property (nonatomic, copy) void(^openAsset)(NSInteger index);
@property (nonatomic, strong) orderModel *mode;

@end
