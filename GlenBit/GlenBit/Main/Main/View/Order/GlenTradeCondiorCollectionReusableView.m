//
//  GlenTradeCondiorCollectionReusableView.m
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenTradeCondiorCollectionReusableView.h"

@interface GlenTradeCondiorCollectionReusableView ()
@property (weak, nonatomic) IBOutlet UIButton *refine;

@property (weak, nonatomic) IBOutlet UILabel *coinFirst;
@property (weak, nonatomic) IBOutlet UILabel *coinSecont;
@property (weak, nonatomic) IBOutlet UILabel *typeFirst;
@property (weak, nonatomic) IBOutlet UILabel *dateFirst;
@property (weak, nonatomic) IBOutlet UILabel *datesecond;

@end

@implementation GlenTradeCondiorCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _refine.layer.borderWidth = 1;
    _refine.layer.borderColor = [UIColor colorWithHexString:@"#BDBFC2"].CGColor;
    _refine.layer.cornerRadius = 20;
}
- (IBAction)selct:(id)sender {
    if (self.selectContior) {
        self.selectContior();
    }
}
- (void)setModel:(GlenContionModel *)model{
    _model = model;
    _coinFirst.text = model.marke?model.marke.targetCoin:kLocalizedString(@"ALL", @"所有");
    _coinSecont.text = model.marke?model.marke.marketCoin:kLocalizedString(@"ALL", @"所有");
    _dateFirst.text = model.beginTime?[NSString StringFromTimestamp:model.beginTime]:kLocalizedString(@"ALL", @"所有");
    _datesecond.text = model.endTime?[NSString StringFromTimestamp:model.endTime]:kLocalizedString(@"ALL", @"所有");
}
@end
