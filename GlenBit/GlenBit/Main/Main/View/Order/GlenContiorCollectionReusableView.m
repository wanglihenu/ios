//
//  GlenContiorCollectionReusableView.m
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenContiorCollectionReusableView.h"


@interface GlenContiorCollectionReusableView ()

@property (weak, nonatomic) IBOutlet UIButton *refine;
@property (weak, nonatomic) IBOutlet UILabel *coinFirst;
@property (weak, nonatomic) IBOutlet UILabel *coinSecont;
@property (weak, nonatomic) IBOutlet UILabel *typeFirst;
@property (weak, nonatomic) IBOutlet UILabel *coinStr;
@property (weak, nonatomic) IBOutlet UILabel *typeStr;


@end

@implementation GlenContiorCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _refine.layer.borderWidth = 1;
    _refine.layer.borderColor = [UIColor colorWithHexString:@"#BDBFC2"].CGColor;
    _refine.layer.cornerRadius = 20;
}
- (void)setModel:(GlenContionModel *)model{
    
    _coinStr.text = kLocalizedString(@"PAIR", @"");
    _typeStr.text = kLocalizedString(@"BUY_OR_SELL", @"");
    [_refine setTitle:kLocalizedString(@"Refine", @"") forState:UIControlStateNormal];
    
    
    _model = model;
    _coinFirst.text = model.marke?model.marke.targetCoin:kLocalizedString(@"ALL", @"所有");
    _coinSecont.text = model.marke?model.marke.marketCoin:kLocalizedString(@"ALL", @"所有");
    _typeFirst.text = model.typeStr?model.typeStr:kLocalizedString(@"ALL", @"所有");
}
- (IBAction)selct:(id)sender {
    if (self.selectContior) {
        self.selectContior();
    }
}

@end
