//
//  GlenOrderHeadCollectionReusableView.m
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenOrderHeadCollectionReusableView.h"

@interface GlenOrderHeadCollectionReusableView ()

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *selectViews;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *selectLab;
@property (weak, nonatomic) IBOutlet UILabel *orderSre;
@property (weak, nonatomic) IBOutlet UILabel *openStr;
@property (weak, nonatomic) IBOutlet UILabel *hisStr;

@end

@implementation GlenOrderHeadCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
     [self config];
}
- (void)setCurrentIndex:(NSInteger)currentIndex{
    
    _orderSre.text = kLocalizedString(@"ORDERS", @"");
    _openStr.text = kLocalizedString(@"OPEN_ORDERS", @"");
    _hisStr.text = kLocalizedString(@"DEALT_ORDERS", @"");
    _currentIndex = currentIndex;
    for (int i = 0; i < _selectLab.count; i++) {
        UILabel *tempLab = _selectLab[i];
        UIView *tempView = _selectViews[i];
        if (i == _currentIndex) {
            tempLab.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
            tempView.backgroundColor = [UIColor colorWithHexString:@"#353B41"];
            tempView.layer.borderWidth = 0;
        }else{
            tempLab.textColor = [UIColor colorWithHexString:@"#353B41"];
            tempView.backgroundColor = [UIColor colorWithHexString:@"#E8E8E8"];
            tempView.layer.borderColor = [UIColor colorWithHexString:@"#B4B7BB"].CGColor;
            tempView.layer.borderWidth = 1;
        }
    }
    
}
- (void)seletap:(UITapGestureRecognizer *)sender{
    self.currentIndex = [sender view].tag - 10;
    if (self.changeCurrent) {
        self.changeCurrent(_currentIndex);
    }
}
- (void)config{
    for (int i = 0; i < _selectViews.count; i++) {
        UIView *tempView = _selectViews[i];
        UITapGestureRecognizer *Tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(seletap:)];
        [tempView addGestureRecognizer:Tap];
    }
}
@end
