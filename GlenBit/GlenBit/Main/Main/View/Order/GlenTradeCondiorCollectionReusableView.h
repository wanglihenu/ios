//
//  GlenTradeCondiorCollectionReusableView.h
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlenTradeCondiorCollectionReusableView : UICollectionReusableView

@property (nonatomic, copy) void(^selectContior)(void);
@property (nonatomic, strong) GlenContionModel *model;

@end
