//
//  GlenOrderHeadCollectionReusableView.h
//  GlenBit
//
//  Created by Lee on 2019/1/7.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlenOrderHeadCollectionReusableView : UICollectionReusableView

@property (nonatomic, copy) void(^changeCurrent)(NSInteger index);
@property (nonatomic, assign) NSInteger currentIndex;

@end
