//
//  GlenOrderCollectionViewCell.m
//  GlenBit
//
//  Created by Lee on 2019/2/14.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenOrderCollectionViewCell.h"

@interface GlenOrderCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *pair;
@property (weak, nonatomic) IBOutlet UILabel *amount;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *type;
@property (weak, nonatomic) IBOutlet UIButton *canBtn;


@end

@implementation GlenOrderCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setMode:(orderModel *)mode{
    [_canBtn setTitle:kLocalizedString(@"CANCEL", @"") forState:UIControlStateNormal];
    _mode = mode;
    _pair.text = mode.pair;
    _price.text = mode.price;
    _amount.text = mode.amount;
    _time.text = [NSString StringFromTimestamp:mode.createdAt];
    _type.text = [mode.type isEqualToString:@"buy"]?kLocalizedString(@"BUY", @""):kLocalizedString(@"SELL", @"");
}
- (IBAction)openAction:(UIButton *)sender {
    if (self.openAsset) {
        self.openAsset(sender.tag - 10);
    }
}
@end
