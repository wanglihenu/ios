//
//  DepthYAxisFormatter.h
//  GlenBit
//
//  Created by Lee on 2019/1/24.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Charts/Charts-Swift.h>
#import "ExchangeModel.h"


@interface DepthYAxisFormatter : NSObject<IChartAxisValueFormatter>

@property (nonatomic, copy)NSArray *kLineList;

@end
