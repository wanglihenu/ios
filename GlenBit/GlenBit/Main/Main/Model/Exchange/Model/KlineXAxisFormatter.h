//
//  KlineXAxisFormatter.h
//  GlenBit
//
//  Created by Lee on 2019/2/22.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Charts/Charts-Swift.h>

@interface KlineXAxisFormatter : NSObject<IChartAxisValueFormatter>

@property (nonatomic, copy)NSArray *kLineList;

@end
