//
//  KlineXAxisFormatter.m
//  GlenBit
//
//  Created by Lee on 2019/2/22.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "KlineXAxisFormatter.h"
#import <Charts/Charts-Swift.h>

@implementation KlineXAxisFormatter

-(NSString *)stringForValue:(double)value axis:(ChartAxisBase *)axis{
    
    if(value >= self.kLineList.count) return @"";
    KlineModel *candleItem = [self.kLineList objectAtIndex:(int)value];
    return [NSString StringfENFromTimestamp:candleItem.timestamp];
    
}

@end
