//
//  DepthYAxisFormatter.m
//  GlenBit
//
//  Created by Lee on 2019/1/24.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "DepthYAxisFormatter.h"

@implementation DepthYAxisFormatter

-(NSString *)stringForValue:(double)value axis:(ChartAxisBase *)axis{
    
    
    return [NSString stringWithFormat:@"%.2f",value];
    if(value >= self.kLineList.count) return @"";
    exchangeDetail *candleItem = [self.kLineList objectAtIndex:(int)value];
    return candleItem.amount;
}

@end
