//
//  TixianInfoModel.h
//  GlenBit
//
//  Created by Lee on 2019/2/12.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TixianInfoModel : NSObject

@property (nonatomic , copy) NSString              * fee;
@property (nonatomic , copy) NSString              * minWithdraw;
@property (nonatomic , copy) NSString              * dailyAvailable;
@property (nonatomic , copy) NSString              * available;
@property (nonatomic , copy) NSString              * balance;

@end
