//
//  Markets.h
//  GlenBit
//
//  Created by Lee on 2019/1/21.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Markets : NSObject

@property (nonatomic , copy) NSString              * minVolume;
@property (nonatomic , copy) NSString              * marketCoin;
@property (nonatomic , assign) BOOL               isAvailable;
@property (nonatomic , copy) NSString              * targetCoin;
@property (nonatomic , copy) NSString              * precision;
@property (nonatomic , copy) NSString              * fee;

@end

@interface SortMarkets : NSObject

@property (nonatomic , copy) NSMutableArray              * markes;
@property (nonatomic , copy) NSMutableArray              * targetCoins;
@end

@interface SortSecondMarkets : NSObject

@property (nonatomic , copy) NSArray              * markes;
@property (nonatomic , copy) NSMutableArray              * marketCoins;

@end
