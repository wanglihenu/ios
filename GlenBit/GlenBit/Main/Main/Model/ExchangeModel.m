//
//  ExchangeModel.m
//  GlenBit
//
//  Created by Lee on 2019/1/23.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "ExchangeModel.h"

@implementation exchangeDetail

@end

@implementation ExchangeModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"sells" : [exchangeDetail class],
             @"histories" : [exchangeDetail class],
             @"buys" : [exchangeDetail class]
             };
}
- (NSMutableArray *)sortbuys{
    if (!_sortbuys){
        _sortbuys = [NSMutableArray new];
    NSSortDescriptor *ageSD = [NSSortDescriptor sortDescriptorWithKey:@"price" ascending:NO];//ascending:YES
    [_sortbuys addObjectsFromArray:[_buys sortedArrayUsingDescriptors:@[ageSD]]];
    }
    return _sortbuys;
}
- (NSMutableArray *)sortall{
    if (!_sortall){
        _sortall = [NSMutableArray new];
        NSMutableArray *tem = [NSMutableArray new];
        [tem addObjectsFromArray:_buys];
        [tem addObjectsFromArray:_sells];
        NSSortDescriptor *ageSD = [NSSortDescriptor sortDescriptorWithKey:@"price" ascending:NO];//ascending:YES
        [_sortall addObjectsFromArray:[tem sortedArrayUsingDescriptors:@[ageSD]]];
    }
    return _sortall;
}
- (NSMutableArray *)sortsells{
    if (!_sortsells){
        _sortsells = [NSMutableArray new];
        NSSortDescriptor *ageSD = [NSSortDescriptor sortDescriptorWithKey:@"price" ascending:NO];//ascending:YES
        [_sortsells addObjectsFromArray:[[_sells subarrayWithRange:NSMakeRange(0, _sells.count>12?12:_sells.count)] sortedArrayUsingDescriptors:@[ageSD]]];
    }
    return _sortsells;
}
- (NSMutableArray *)sorthistories{
    if (!_sorthistories) {
        _sorthistories = [NSMutableArray new];
    NSSortDescriptor *ageSD = [NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:NO];//ascending:YES
    [_sorthistories addObjectsFromArray:[ _histories sortedArrayUsingDescriptors:@[ageSD]]];
    for (NSInteger i = 0; i < _sorthistories.count ; i++) {
        exchangeDetail *model1 = _sorthistories[i];
        if (i == _sorthistories.count - 1) {
            model1.isUp = YES;
            break;
        }
        exchangeDetail *model2 = _sorthistories[i + 1];
        if (model1.price.floatValue >= model2.price.floatValue) {
            model1.isUp = YES;
        }else{
            model1.isUp = NO;
        }
    }
    }
    return _sorthistories;
}
@end
