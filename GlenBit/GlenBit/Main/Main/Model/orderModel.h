//
//  orderModel.h
//  GlenBit
//
//  Created by Lee on 2019/1/21.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface orderModel : NSObject

@property (nonatomic , copy) NSString              * orderId;
@property (nonatomic , copy) NSString              * pair;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * price;
@property (nonatomic , copy) NSString              * amount;
@property (nonatomic , copy) NSString              * filled;
@property (nonatomic , copy) NSString              * createdAt;
@end
@interface hisModel : NSObject

@property (nonatomic , copy) NSArray              * docs;
@end
