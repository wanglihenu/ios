//
//  ExchangeModel.h
//  GlenBit
//
//  Created by Lee on 2019/1/23.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface exchangeDetail :NSObject
@property (nonatomic , strong) NSString              * amount;
@property (nonatomic , strong) NSString              * _id;
@property (nonatomic , strong) NSString              * price;
@property (nonatomic , strong) NSString              * createdAt;
@property (nonatomic , assign) BOOL              isUp;
@end

@interface ExchangeModel :NSObject
@property (nonatomic , strong) NSArray              * sells;
@property (nonatomic , copy) NSString              * timestamp;
@property (nonatomic , strong) NSArray              * histories;
@property (nonatomic , strong) NSMutableArray              * sorthistories;
@property (nonatomic , strong) NSMutableArray              * sortsells;
@property (nonatomic , strong) NSMutableArray              * sortbuys;
@property (nonatomic , strong) NSMutableArray              * sortall;
@property (nonatomic , copy) NSString              * pair;
@property (nonatomic , strong) NSArray              * buys;

@end

