//
//  Markets.m
//  GlenBit
//
//  Created by Lee on 2019/1/21.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "Markets.h"

@implementation Markets

@end

@implementation SortMarkets
- (NSMutableArray *)markes{
    if (!_markes) {
        _markes = [NSMutableArray new];
    }
    return _markes;
}
- (NSMutableArray *)targetCoins{
    if (!_targetCoins) {
        _targetCoins = [NSMutableArray new];
    }
    return _targetCoins;
}

@end

@implementation SortSecondMarkets

- (NSArray *)markes{
    if (!_markes) {
        _markes = [NSArray new];
    }
    return _markes;
}
- (NSMutableArray *)marketCoins{
    if (!_marketCoins) {
        _marketCoins = [NSMutableArray new];
    }
    return _marketCoins;
}
@end
