//
//  orderModel.m
//  GlenBit
//
//  Created by Lee on 2019/1/21.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "orderModel.h"

@implementation orderModel

@end

@implementation hisModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"docs" : [orderModel class]};
}

@end
