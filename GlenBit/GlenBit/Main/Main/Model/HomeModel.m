//
//  HomeModel.m
//  GlenBit
//
//  Created by Lee on 2019/1/23.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "HomeModel.h"

@implementation HomeModel



-(BOOL)isEqualToLocModel:(HomeModel *)model{
    if (!model) {
        return NO;
    }
    
    BOOL haveEqual = (!self.pair && !model.pair) || [self.pair isEqualToString: model.pair];

    
    return haveEqual;
}
- (NSString *)targetCoin{
    if (!_targetCoin) {
        NSArray *tem = [_pair componentsSeparatedByString:@"-"];
        _targetCoin = tem[0];
    }
    return _targetCoin;
}
- (NSString *)marketCoin{
    if (!_marketCoin) {
        NSArray *tem = [_pair componentsSeparatedByString:@"-"];
        _marketCoin = tem[1];
    }
    return _marketCoin;
}
- (BOOL)isEqual:(id)object {
    if (self == object) {//对象本身
        return YES;
    }
    
    if (![object isKindOfClass:[HomeModel class]]) {//不是本类
        return NO;
    }
    
    return [self isEqualToLocModel:(HomeModel *)object];//必须全部属性相同 但是在实际开发中关键属性相同就可以
}

- (NSUInteger)hash {//hash比较 如果对象的属性是NSString等的对象 则需要用 ^运算逻辑异或
    return [self hash];
}


@end
