//
//  GlenContionModel.h
//  GlenBit
//
//  Created by Lee on 2019/1/26.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlenContionModel : NSObject

@property (nonatomic , strong) Markets               * marke;
@property (nonatomic , copy) NSString              * typeStr;
@property (nonatomic , assign) NSInteger              type;
@property (nonatomic , copy) NSString              * beginTime;
@property (nonatomic , copy) NSString              * endTime;

@end
