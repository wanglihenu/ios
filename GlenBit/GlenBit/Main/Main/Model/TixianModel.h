//
//  TixianModel.h
//  GlenBit
//
//  Created by Lee on 2019/2/12.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

///txid与updateAt为空审核中，只有txid为空的时候为处理中，txid不为空的时候为已汇出。
@interface TixianModel : NSObject

@property (nonatomic , copy) NSString              * txid;
@property (nonatomic , copy) NSString              * coin;
@property (nonatomic , copy) NSString              * amount;
@property (nonatomic , copy) NSString              * createdAt;
@property (nonatomic , copy) NSString              * updatedAt;
@property (nonatomic , copy) NSString              * address;

@end
