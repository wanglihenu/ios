//
//  GoogleModel.h
//  GlenBit
//
//  Created by Lee on 2019/1/25.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoogleModel : NSObject

@property (nonatomic , copy) NSString              * key;
@property (nonatomic , copy) NSString              * url;

@end
