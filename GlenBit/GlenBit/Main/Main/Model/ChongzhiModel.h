//
//  ChongzhiModel.h
//  GlenBit
//
//  Created by Lee on 2019/2/12.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChongzhiModel : NSObject

@property (nonatomic , copy) NSString              * txid;
@property (nonatomic , copy) NSString              * coin;
@property (nonatomic , copy) NSString              * amount;
@property (nonatomic , copy) NSString              * createdAt;
@property (nonatomic , copy) NSString              * updatedAt;//null话为待确认，否则显示在充值记录中
@property (nonatomic , copy) NSString              * DepositAddress;
@end
