//
//  marketPrices.h
//  GlenBit
//
//  Created by Lee on 2019/1/21.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface marketPrices : NSObject

@property (nonatomic , copy) NSString              * to;
@property (nonatomic , assign) BOOL               isLocal;
@property (nonatomic , copy) NSString              * updatedAt;
@property (nonatomic , copy) NSString              * price;
@property (nonatomic , copy) NSString              * from;

@end
