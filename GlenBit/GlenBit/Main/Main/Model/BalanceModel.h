//
//  BalanceModel.h
//  GlenBit
//
//  Created by Lee on 2019/1/31.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BalanceModel : NSObject

@property (nonatomic , copy) NSString              * free;
@property (nonatomic , copy) NSString              * coin;
@property (nonatomic , copy) NSString              * locked;

@end
