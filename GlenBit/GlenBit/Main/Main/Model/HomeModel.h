//
//  HomeModel.h
//  GlenBit
//
//  Created by Lee on 2019/1/23.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeModel : NSObject

@property (nonatomic , copy) NSString              * change;
@property (nonatomic , copy) NSString              * close;
@property (nonatomic , copy) NSString              * high;
@property (nonatomic , copy) NSString              * low;
@property (nonatomic , strong) NSString              * pair;
@property (nonatomic , strong) NSString              * targetCoin;
@property (nonatomic , strong) NSString              * marketCoin;
@property (nonatomic , copy) NSString              * updatedAt;
@property (nonatomic , copy) NSString              * volume;
@property (nonatomic , copy) NSString              * volumeDiff;



-(BOOL)isEqualToLocModel:(HomeModel *)model;

@end
