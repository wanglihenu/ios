//
//  FGLanguageTool.h
//  LanguageSwitching
//
//  Created by apple on 16/12/5.
//  Copyright © 2016年 YJS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define ChangeLanguageNotificationName @"changeLanguage"
#define kLocalizedString(key, comment) [kLanguageManager localizedStringForKey:key value:comment]
#define CHINESE @"zh_CN"
#define ENGLISH @"en"
#define JAPANESE @"Japanese"
#define KOREAN @"Korean"
#define KHMER @"Cambodia"

@interface HXLanguageManager : NSObject

@property (nonatomic,copy) void (^completion)(NSString *currentLanguage);

- (NSInteger)deflaut;

- (NSArray *)getAllLanguage;

- (void)setLanguageIndex:(NSInteger)index;
- (NSString *)currentLanguage; //当前语言
- (NSString *)serviceLanguage;//给服务器提交的
- (NSString *)languageFormat:(NSString*)language;
- (void)setUserlanguage:(NSString *)language;//设置当前语言

- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)value;

- (UIImage *)ittemInternationalImageWithName:(NSString *)name;

+ (instancetype)shareInstance;

#define kLanguageManager [HXLanguageManager shareInstance]

@end
