//
//  GlenBaseViewController.m
//  Glenbit
//
//  Created by Lee on 2019/1/3.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBaseViewController.h"
#import "GlenNavView.h"
#import "GlenLoginViewController.h"
#import "GlenMainViewController.h"
#import "GlenUserInfoViewController.h"
#import "MBManager.h"

@interface GlenBaseViewController ()

@property (strong, nonatomic) GlenNavView *navigationView;

@end

@implementation GlenBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(upDataLoca)
     name:@"laugu"
     object:nil];
    if (_hideNav) {
        
    }else{
        GlenWeakSelf;
        self.navigationView = [[GlenNavView alloc] initWithFrame:CGRectMake(0, StatusBar_height, size_width, 60)];
        self.navigationView.openRight = ^{
            [weakSelf openRight];
        };
        if ([UerManger shareUserManger].isLogin) {
            [self.navigationView updataUserInfo];
        }
        
        self.navigationView.openUser = ^{
            if ([weakSelf isKindOfClass:[GlenLoginViewController class]]) {
                return ;
            }
            if ([UerManger shareUserManger].isLogin) {
                GlenUserInfoViewController *login = [GlenUserInfoViewController new];
                [weakSelf.navigationController pushViewController:login animated:YES];
            }else{
                GlenLoginViewController *login = [GlenLoginViewController new];
                [weakSelf.navigationController pushViewController:login animated:YES];
            }
            
            
            
            
        };
        [self.view addSubview:self.navigationView];
    }
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self upDataLoca];
}
- (void)upDataLoca{
    
}
- (void)getMarket{
    [self showHUBWith:@""];
    GlenWeakSelf;
    [[GlenHttpTool shareInstance] marketsCompletion:^(NSArray *coins) {
        
        NSMutableSet *set = [NSMutableSet new];
        for (Markets *market in coins) {
            [set addObject:market.targetCoin];
        }
        SortMarkets *mark = [SortMarkets new];
        for (NSString *key in set) {
            SortSecondMarkets *second = [SortSecondMarkets new];
            NSPredicate  *predicate = [NSPredicate predicateWithFormat:@"targetCoin CONTAINS %@",key];
            second.markes = [coins filteredArrayUsingPredicate:predicate];
            for (Markets *market in second.markes) {
                [second.marketCoins addObject:market.marketCoin];
            }
            [mark.targetCoins addObject:key];;
            [mark.markes addObject:second];
        }
        [UerManger shareUserManger].markes = mark;
        [weakSelf getMarkPrice];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
    }];
}
- (void)getMarkPrice{
    GlenWeakSelf;
    [[GlenHttpTool shareInstance] marketPricesCompletion:^(NSArray *coins) {
        NSMutableArray *a = [NSMutableArray new];
        [a addObjectsFromArray:coins];
        [a enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            marketPrices *p = obj;
            
            if (p.price.floatValue == 1) {
                *stop = YES;
                
                if (*stop == YES) {
                    [a removeObject:p];
                }
                *stop = NO;
            }
        }];
        [UerManger shareUserManger].marke_price = a;
        
        [[GlenHttpTool shareInstance]coinsCompletion:^(NSArray *coins) {
            [weakSelf hiddenHUB];
            
            [UerManger shareUserManger].coins = coins;
            NSSortDescriptor *ageSD = [NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES];//ascending:YES
            
            [UerManger shareUserManger].coins = [[[UerManger shareUserManger].coins sortedArrayUsingDescriptors:@[ageSD]] mutableCopy];
        } fail:^(NSString *error) {
            [weakSelf hiddenHUB];
        }];
    } fail:^(NSString *error) {
        [weakSelf hiddenHUB];
    }];
}
- (void)updata{
    [_navigationView updataUserInfo];
}
// 显示加载圈, titleWie加载圈上显示的内容
- (void)showHUBWith: (NSString *)title{
    [MBManager showGiF];
}
//- (void)viewDidAppear:(BOOL)animated{
//    [super viewDidAppear:animated];
//    [self  hiddenHUB];
//}
// 隐藏加载圈
- (void)hiddenHUB{
    [MBManager hide];
}
- (void)openRight{
    //     [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}
- (BOOL)isPresent {
    
    BOOL isPresent = false;
    
    NSArray *viewcontrollers = self.navigationController.viewControllers;
    
    if (viewcontrollers.count > 1) {
        {
            
            isPresent = NO; //push方式
        }
    }
    else{
        isPresent = YES;  // modal方式
    }
    
    return isPresent;
}
- (void)setHideNav:(BOOL)hideNav{
    _hideNav = hideNav;
    if (self.navigationView) {
        self.navigationView.hidden = YES;
    }
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter]
     removeObserver:self];
}

-(void)contentSizeDidChangeNotification:(NSNotification*)notification{
    [self updata];
}

-(void)contentSizeDidChange:(NSString *)size{
    //Implement in subclass
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
