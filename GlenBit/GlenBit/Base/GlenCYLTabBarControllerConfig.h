//
//  CYLTabBarControllerConfig.h
//  CYLTabBarController
//
//  v1.14.0 Created by 微博@iOS程序犭袁 ( http://weibo.com/luohanchenyilong/ ) on 15/11/3.
//  Copyright © 2015年 https://github.com/ChenYilong .All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GlenMainViewController.h"
#import "GlenExchangeViewController.h"
#import "GlenAssetViewController.h"
#import "GlenOrderViewController.h"
#import "GlenUserInfoViewController.h"

@interface CYLBaseNavigationController : UINavigationController
<UINavigationControllerDelegate>

// 记录push标志
@property (nonatomic, getter=isPushing) BOOL pushing;

@property (nonatomic, strong) UIViewController *lastViewController;

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated isNeedLogin:(BOOL)isNeedLogin;
@end


@interface GlenCYLTabBarControllerConfig : NSObject

@property (nonatomic, readonly, strong) CYLTabBarController *tabBarController;

@end

