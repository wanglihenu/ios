//
//  GlenBaseViewController.h
//  Glenbit
//
//  Created by Lee on 2019/1/3.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlenBaseViewController : UIViewController

@property (nonatomic, assign) BOOL hideNav;


// 显示加载圈, titleWie加载圈上显示的内容
- (void)showHUBWith: (NSString *)title;
// 隐藏加载圈
- (void)hiddenHUB;

-(void)contentSizeDidChange:(NSString*)size;
- (void)openRight;
- (void)updata;
- (BOOL)isPresent;
- (void)upDataLoca;
@end
