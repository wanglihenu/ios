//
//  GlenNavView.m
//  GlenBit
//
//  Created by Lee on 2019/1/4.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenNavView.h"

@interface GlenNavView ()
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIImageView *userHead;
@property (weak, nonatomic) IBOutlet UIButton *more;


@end

@implementation GlenNavView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"GlenNavView" owner:self options:nil] firstObject];
        self.frame = frame;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userDetail)];
        [_userHead addGestureRecognizer:tap];
        [_userName addGestureRecognizer:tap];
        _userName.hidden = YES;
        _userHead.hidden = YES;
    }
    return self;
}
- (void)userDetail{
//    if (self.openUser) {
//        self.openUser();
//    }
}
- (void)updataUserInfo{
    if ([UerManger shareUserManger].isLogin) {
        _userName.hidden = NO;
        _userHead.hidden = NO;
        _userName.text = [UerManger shareUserManger].loginType == 0?[UerManger shareUserManger].userModel.email:[UerManger shareUserManger].userModel.phone;
    }else{
        _userName.hidden = YES;
        _userHead.hidden = YES;
    }
    
}
- (IBAction)openRightMenu:(UIButton *)sender {
    if (self.openRight) {
        self.openRight();
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
