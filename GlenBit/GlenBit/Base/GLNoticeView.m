//
//  GLNoticeView.m
//  GlenBit
//
//  Created by Lee on 2019/2/23.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GLNoticeView.h"
#import "FLAnimatedImageView.h"
#import "FLAnimatedImage.h"
#import "UIImage+GIF.h"

@implementation GLNoticeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (CGSize)intrinsicContentSize {
    CGFloat contentViewH = 50;
    CGFloat contentViewW = 50;
    return CGSizeMake(contentViewW, contentViewH);
}


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];// 先调用父类的initWithFrame方法
    
    if (self) {
    self.translatesAutoresizingMaskIntoConstraints = NO;
    self.layer.cornerRadius = 25;
    self.layer.masksToBounds = YES;
        [self image];
    }
    return self;
}
- (void)image{
    FLAnimatedImageView *images = [[FLAnimatedImageView alloc]initWithFrame:CGRectMake(1, 1, 48, 48)];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"GlenBit" ofType:@"gif"];
    NSData * running=[NSData dataWithContentsOfFile:filePath];
    FLAnimatedImage *gifImage = [FLAnimatedImage animatedImageWithGIFData:running];
    images.animatedImage = gifImage;
    [self addSubview:images];
}
@end
