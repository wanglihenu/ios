//
//  EWebViewController.m
//  EWallet
//
//  Created by Lee on 2018/9/3.
//  Copyright © 2018年 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "EWebViewController.h"
#import <WebKit/WebKit.h>


@interface EWebViewController ()<WKNavigationDelegate>
@property (nonatomic, strong) WKWebView *wk_webView;
@property (nonatomic, assign) BOOL isSussces;
@end

@implementation EWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    if (_isXieyi) {
        self.title = @"服务条款";
        if (!_isSussces) {
            [self showHUBWith:@""];
        }
                self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_return"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
        _wk_webView=[self createSharableWKWebView];
        
        _wk_webView.frame=CGRectMake(0, 0, size_width, size_height - StatusBar_height);
        _wk_webView.backgroundColor = [UIColor whiteColor];
        _wk_webView.navigationDelegate = self;
        if ([[[HXLanguageManager shareInstance]currentLanguage] isEqualToString:CHINESE]) {
            [self setWebUrl:[[NSBundle mainBundle] pathForResource:@"userAgreement" ofType:@"html"]];
        }else{
           [self setWebUrl:[[NSBundle mainBundle] pathForResource:@"userAgreementEn" ofType:@"html"]];
        }
        
    }else{
        self.title = _titleStr;
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_return"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
        if (!_isSussces) {
            [self showHUBWith:@""];
        }
        _wk_webView=[self createSharableWKWebView];
        
        _wk_webView.frame=CGRectMake(0, 0, size_width, size_height - StatusBar_height);
        _wk_webView.backgroundColor = [UIColor whiteColor];
        _wk_webView.navigationDelegate = self;
        [_wk_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_url.length>0?_url:@"http://beta.betatime.net/wap/informations_list.aspx"]]];
    }
    [self.view addSubview:_wk_webView];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO; // 隐藏导航栏
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = YES; // 隐藏导航栏
}
- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (WKWebView *)createSharableWKWebView {
    
    WKUserContentController* userContentController = [WKUserContentController new];
    
    NSMutableString *cookies = [NSMutableString string];
    WKUserScript * cookieScript = [[WKUserScript alloc] initWithSource:[cookies copy]
                                                         injectionTime:WKUserScriptInjectionTimeAtDocumentStart
                                                      forMainFrameOnly:NO];
    [userContentController addUserScript:cookieScript];
    
    WKWebViewConfiguration *configuration = [WKWebViewConfiguration new];
    // 以下两个属性是允许H5视屏自动播放,并且全屏,可忽略
    configuration.allowsInlineMediaPlayback = YES;
    if (@available(iOS 10.0, *)) {
        configuration.mediaTypesRequiringUserActionForPlayback = NO;
    } else {
        // Fallback on earlier versions
    }
    // 全局使用同一个processPool
    configuration.processPool = [WKProcessPool new];
    configuration.userContentController = userContentController;
    WKWebView *wk_webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:configuration];
    return wk_webView;
}

- (NSURL *)fileURLForBuggyWKWebView8:(NSURL *)fileURL {
    NSError *error = nil;
    if (!fileURL.fileURL || ![fileURL checkResourceIsReachableAndReturnError:&error]) {
        return nil;
    }
    // Create "/temp/www" directory
    NSFileManager *fileManager= [NSFileManager defaultManager];
    NSURL *temDirURL = [[NSURL fileURLWithPath:NSTemporaryDirectory()] URLByAppendingPathComponent:@"www"];
    [fileManager createDirectoryAtURL:temDirURL withIntermediateDirectories:YES attributes:nil error:&error];
    
    NSURL *dstURL = [temDirURL URLByAppendingPathComponent:fileURL.lastPathComponent];
    // Now copy given file to the temp directory
    [fileManager removeItemAtURL:dstURL error:&error];
    [fileManager copyItemAtURL:fileURL toURL:dstURL error:&error];
    // Files in "/temp/www" load flawlesly :)
    return dstURL;
}
- (void)setWebUrl:(NSString *)url{
    if(url){
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 9.0) {
            NSURL *fileURL = [NSURL fileURLWithPath:url];
            if (@available(iOS 9.0, *)) {
                [_wk_webView loadFileURL:fileURL allowingReadAccessToURL:fileURL];
            } else {
                // Fallback on earlier versions
            }
        } else {
            NSURL *fileURL = [self fileURLForBuggyWKWebView8:[NSURL fileURLWithPath:url]];
            NSURLRequest *request = [NSURLRequest requestWithURL:fileURL];
            [_wk_webView loadRequest:request];
        }
    }
}
- (void)setTitleStr:(NSString *)titleStr{
    _titleStr = titleStr;
}
- (void)setUrl:(NSString *)url{
    _url = url;
}
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    if (!_isSussces) {
        
        [self hiddenHUB];
        _isSussces = YES;
    }
}
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {

    WKNavigationActionPolicy actionPolicy = WKNavigationActionPolicyAllow;
    if (navigationAction.accessibilityNavigationStyle == WKNavigationTypeBackForward ) {
        if (webView.backForwardList.backList.count == 0) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    decisionHandler(actionPolicy);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
