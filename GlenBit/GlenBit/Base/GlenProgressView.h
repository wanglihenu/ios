//
//  GlenProgressView.h
//  GlenBit
//
//  Created by Lee on 2019/1/8.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,ProgressType) {
    ProgressTypePassNormal,
    ProgressTypeCurrentNormal,
    ProgressTypeNext,
};

@interface GlenProgressView : UIView

- (void)showProgressViewWithTitleArr:(NSArray *)titleArr curentIndex:(NSInteger)curentIndex;

@end
