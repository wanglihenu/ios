//
//  EWebViewController.h
//  EWallet
//
//  Created by Lee on 2018/9/3.
//  Copyright © 2018年 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenBaseViewController.h"

@interface EWebViewController : GlenBaseViewController

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *titleStr;
@property (nonatomic, assign) BOOL isXieyi;
@property (nonatomic, copy) void(^xieyiAgree)(BOOL xieyiAgree);

@end
