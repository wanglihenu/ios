//
//  CYLTabBarControllerConfig.m
//  CYLTabBarController
//
//  v1.14.0 Created by 微博@iOS程序犭袁 ( http://weibo.com/luohanchenyilong/ ) on 10/20/15.
//  Copyright © 2015 https://github.com/ChenYilong . All rights reserved.
//
#import "GlenCYLTabBarControllerConfig.h"
//static CGFloat const CYLTabBarControllerHeight = 44.f;

#import "GlenMainViewController.h"
#import "GlenExchangeViewController.h"
#import "GlenAssetViewController.h"
#import "GlenOrderViewController.h"
#import "GlenUserInfoViewController.h"


@implementation CYLBaseNavigationController

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_X (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 812.0f)
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.translucent = YES;
    self.delegate = self;
    self.navigationBarHidden = NO; // 使右滑返回手势可用
    self.navigationBar.hidden = YES; // 隐藏导航栏
}
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (self.viewControllers.count > 0) {

        //        if (!IS_IPHONE_X) {
        viewController.hidesBottomBarWhenPushed = YES;
        //        }

    }
    [super pushViewController:viewController animated:animated];
}
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated isNeedLogin:(BOOL)isNeedLogin{
    
    [self pushViewController:viewController animated:YES];
}
@end

@interface GlenCYLTabBarControllerConfig ()<UITabBarControllerDelegate>

@property (nonatomic, readwrite, strong) CYLTabBarController *tabBarController;

@end

@implementation GlenCYLTabBarControllerConfig

/**
 *  lazy load tabBarController
 *
 *  @return CYLTabBarController
 */
- (CYLTabBarController *)tabBarController {
    if (_tabBarController == nil) {
        /**
         * 以下两行代码目的在于手动设置让TabBarItem只显示图标，不显示文字，并让图标垂直居中。
         * 等效于在 `-tabBarItemsAttributesForController` 方法中不传 `CYLTabBarItemTitle` 字段。
         * 更推荐后一种做法。
         */
        UIEdgeInsets imageInsets = UIEdgeInsetsZero;//UIEdgeInsetsMake(4.5, 0, -4.5, 0);
        UIOffset titlePositionAdjustment = UIOffsetZero;//UIOffsetMake(0, MAXFLOAT);
        
        CYLTabBarController *tabBarController = [[CYLTabBarController alloc]initWithViewControllers:self.viewControllers
                                                                              tabBarItemsAttributes:self.tabBarItemsAttributesForController imageInsets:imageInsets titlePositionAdjustment:titlePositionAdjustment];
        
        [self customizeTabBarAppearance:tabBarController];
        _tabBarController = tabBarController;
    }
    return _tabBarController;
}

- (NSArray *)viewControllers {
    GlenMainViewController *main = [[GlenMainViewController alloc]init];
    UIViewController *mainNav = [[CYLBaseNavigationController alloc]
                                initWithRootViewController:main];
    
    GlenExchangeViewController *exchange = [[GlenExchangeViewController alloc]init];
    UIViewController *exchangeNav = [[CYLBaseNavigationController alloc]
                                    initWithRootViewController:exchange];
    
    GlenAssetViewController *asset = [[GlenAssetViewController alloc]init];
    UIViewController *assetNav = [[CYLBaseNavigationController alloc]
                                 initWithRootViewController:asset];
    
    GlenOrderViewController *order = [[GlenOrderViewController alloc]init];
    UIViewController *orderNav = [[CYLBaseNavigationController alloc]
                                 initWithRootViewController:order];
    GlenUserInfoViewController *user = [[GlenUserInfoViewController alloc]init];
    UIViewController *userNav = [[CYLBaseNavigationController alloc]
                                  initWithRootViewController:user];
    
    
    NSArray *viewControllers = @[
                                 mainNav,
                                 exchangeNav,
                                 assetNav,
                                 orderNav,
                                 userNav
                                 ];
    return viewControllers;
}

- (NSArray *)tabBarItemsAttributesForController {
    NSDictionary *firstTabBarItemsAttributes = @{
                                                 CYLTabBarItemTitle : kLocalizedString(@"HOME", @"主页"),
                                                 CYLTabBarItemImage : @"menu_home",
                                                 CYLTabBarItemSelectedImage : @"menu_home_current",
                                                 };
    NSDictionary *secondTabBarItemsAttributes = @{
                                                  CYLTabBarItemTitle :  kLocalizedString(@"TRADE", @"交易"),
                                                  CYLTabBarItemImage : @"menu_exchange",
                                                  CYLTabBarItemSelectedImage : @"menu_exchange_current",
                                                  };
    NSDictionary *thirdTabBarItemsAttributes = @{
                                                 CYLTabBarItemTitle : kLocalizedString(@"WALLET", @"钱包"),
                                                 CYLTabBarItemImage : @"menu_wallet",
                                                 CYLTabBarItemSelectedImage : @"menu_wallet_current",
                                                 };
    NSDictionary *fourthTabBarItemsAttributes = @{
                                                  CYLTabBarItemTitle : kLocalizedString(@"ORDERS", @"委托记录"),
                                                  CYLTabBarItemImage : @"menu_orders",
                                                  CYLTabBarItemSelectedImage : @"menu_orders_current"
                                                  };
    NSDictionary *fiveTabBarItemsAttributes = @{
                                                  CYLTabBarItemTitle : kLocalizedString(@"ACCOUNT", @"账号"),
                                                  CYLTabBarItemImage : @"menu_account",
                                                  CYLTabBarItemSelectedImage : @"menu_account_current"
                                                  };
    NSArray *tabBarItemsAttributes = @[
                                       firstTabBarItemsAttributes,
                                       secondTabBarItemsAttributes,
                                       thirdTabBarItemsAttributes,
                                       fourthTabBarItemsAttributes,
                                       fiveTabBarItemsAttributes
                                       ];
    return tabBarItemsAttributes;
}

/**
 *  更多TabBar自定义设置：比如：tabBarItem 的选中和不选中文字和背景图片属性、tabbar 背景图片属性等等
 */
- (void)customizeTabBarAppearance:(CYLTabBarController *)tabBarController {
    
    tabBarController.tabBarHeight = CYL_IS_IPHONE_X ? 94 : 60;
    
    tabBarController.tabBar.barTintColor = [UIColor colorWithHexString:@"#000913"];
    
    NSMutableDictionary *normalAttrs = [NSMutableDictionary dictionary];
    normalAttrs[NSForegroundColorAttributeName] = [UIColor colorWithHexString:@"#ffffff7f"];
    
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSForegroundColorAttributeName] = [UIColor colorWithHexString:@"#ffffff"];
    
    UITabBarItem *tabBar = [UITabBarItem appearance];
    [tabBar setTitleTextAttributes:normalAttrs forState:UIControlStateNormal];
    [tabBar setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
    
    [[UITabBar appearance] setBackgroundColor:[UIColor colorWithHexString:@"#000913"]];
//    [UITabBar appearance].translucent = NO;
//    [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];;
}

- (void)updateTabBarCustomizationWhenTabBarItemWidthDidUpdate {
    void (^deviceOrientationDidChangeBlock)(NSNotification *) = ^(NSNotification *notification) {
        UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
        if ((orientation == UIDeviceOrientationLandscapeLeft) || (orientation == UIDeviceOrientationLandscapeRight)) {
            NSLog(@"Landscape Left or Right !");
        } else if (orientation == UIDeviceOrientationPortrait) {
            NSLog(@"Landscape portrait!");
        }
        [self customizeTabBarSelectionIndicatorImage];
    };
    [[NSNotificationCenter defaultCenter] addObserverForName:CYLTabBarItemWidthDidChangeNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:deviceOrientationDidChangeBlock];
}

- (void)customizeTabBarSelectionIndicatorImage {
    ///Get initialized TabBar Height if exists, otherwise get Default TabBar Height.
    CGFloat tabBarHeight = CYL_IS_IPHONE_X ? 97 : 60;;
    CGSize selectionIndicatorImageSize = CGSizeMake(CYLTabBarItemWidth, tabBarHeight);
    //Get initialized TabBar if exists.
    UITabBar *tabBar = [self cyl_tabBarController].tabBar ?: [UITabBar appearance];
    [tabBar setSelectionIndicatorImage:
     [[self class] imageWithColor:[UIColor yellowColor]
                             size:selectionIndicatorImageSize]];
}

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
    if (!color || size.width <= 0 || size.height <= 0) return nil;
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width + 1, size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
