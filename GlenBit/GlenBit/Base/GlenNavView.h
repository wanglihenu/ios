//
//  GlenNavView.h
//  GlenBit
//
//  Created by Lee on 2019/1/4.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlenNavView : UIView

@property (nonatomic, copy) void(^openRight)(void);
@property (nonatomic, copy) void(^openUser)(void);


- (void)updataUserInfo;

@end
