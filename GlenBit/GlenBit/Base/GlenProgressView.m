//
//  GlenProgressView.m
//  GlenBit
//
//  Created by Lee on 2019/1/8.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenProgressView.h"

static CGFloat const leftMargin     = 14.0f;
static CGFloat const middMargin    = 10.0f;


@interface GlenProgressView ()


@property (nonatomic, assign) NSInteger titleCount; //记录 title 数组的个数

@property (nonatomic, assign) CGFloat x; //x坐标
@property (nonatomic, strong) UIView *backView; //x坐标
@end

@implementation GlenProgressView

- (void)showProgressViewWithTitleArr:(NSArray *)titleArr curentIndex:(NSInteger)curentIndex{
    self.titleCount = titleArr.count;
    CGFloat singleWidth = (size_width - (leftMargin * 2 + 13) * (self.titleCount - 1)) / self.titleCount;
    CGFloat singleHeight = self.bounds.size.height;
    _x = 0;
    NSString *titleStr = @"";
    _backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 200, singleHeight)];
    [self addSubview:_backView];
    for (int i = 0; i < self.titleCount; i++) {
        if (titleArr[i]) {
            titleStr = titleArr[i];
        }
        bool isFirst = i==0?YES:NO;
        bool isEnd = i==self.titleCount-1?YES:NO;
        
        if (i == curentIndex) {
            
            [self singleViewWithX:_x maxWidth:singleWidth height:singleHeight titleStr:titleStr model:ProgressTypeCurrentNormal isFirst:isFirst isEnd:isEnd];
        }else if (i < curentIndex){
            [self singleViewWithX:_x maxWidth:singleWidth height:singleHeight titleStr:titleStr model:ProgressTypePassNormal isFirst:isFirst isEnd:isEnd];
        }else if (i > curentIndex){
            [self singleViewWithX:_x maxWidth:singleWidth height:singleHeight titleStr:titleStr model:ProgressTypeNext isFirst:isFirst isEnd:isEnd];
        }
    }
    _backView.centerX = size_width / 2;
}
- (void)singleViewWithX:(CGFloat)x maxWidth:(CGFloat)maxWidth height:(CGFloat)height titleStr:(NSString *)titleStr model:(ProgressType)type isFirst:(BOOL)isFirst isEnd:(BOOL)isEnd{
    UIView *singleView = [[UIView alloc] initWithFrame:CGRectMake(x, 0, maxWidth, height)];
    
    UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(0,(height - 20)/2, 20, 20)];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfSize:14];
    titleLabel.text = titleStr;
    titleLabel.numberOfLines = 0;
    
    UILabel *detalLabel = [[UILabel alloc] init];
    detalLabel.font = [UIFont systemFontOfSize:14];
    detalLabel.text = @"...";
    detalLabel.numberOfLines = 1;
    detalLabel.textColor = [UIColor colorWithHexString:@"#BDBFC2"];
    switch (type) {
        case ProgressTypeCurrentNormal:
        {
            iconView.image = [UIImage imageNamed:@"step_current"];
            titleLabel.textColor = [UIColor colorWithHexString:@"#F8D900"];
        }
            break;
        case ProgressTypePassNormal:
        {
            iconView.image = [UIImage imageNamed:@"step_done"];
            titleLabel.textColor = [UIColor colorWithHexString:@"#101315"];
        }
            break;
        case ProgressTypeNext:
        {
            iconView.image = [UIImage imageNamed:@"step_yet"];
            titleLabel.textColor = [UIColor colorWithHexString:@"#BDBFC2"];
        }
            break;
            
        default:
            break;
    }
    
    CGSize titleSize = [self string:titleStr sizeWithFont:[UIFont systemFontOfSize:14] MaxSize:CGSizeMake(maxWidth - 20 - middMargin, height)];
    CGSize detailSize = [self string:@"..." sizeWithFont:[UIFont systemFontOfSize:14] MaxSize:CGSizeMake(100, height)];
    
    titleLabel.frame = CGRectMake(iconView.right + middMargin, (height - titleSize.height)/2, titleSize.width, titleSize.height);

    [singleView addSubview:iconView];
    [singleView addSubview:titleLabel];
    singleView.width = 20 + titleSize.width + middMargin;
    [_backView addSubview:singleView];
    _x += singleView.width;
    if (!isEnd) {
        detalLabel.frame = CGRectMake(_x + leftMargin,height / 2 - detailSize.height/2, detailSize.width, detailSize.height);
        [_backView addSubview:detalLabel];
        _x += detailSize.width + leftMargin * 2;
    }
    _backView.width = _x;
    [singleView bringSubviewToFront:iconView];
}
- (CGSize)string:(NSString *)str sizeWithFont:(UIFont *)font MaxSize:(CGSize)maxSize
{
    CGSize resultSize;
    NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    CGRect rect = [str boundingRectWithSize:maxSize
                                    options:(NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading)
                                 attributes:attrs
                                    context:nil];
    resultSize = rect.size;
    resultSize = CGSizeMake(ceil(resultSize.width), ceil(resultSize.height));
    
    return resultSize;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
