//
//  GlenNavBaseViewController.m
//  Glenbit
//
//  Created by Lee on 2019/1/3.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenNavBaseViewController.h"

@interface GlenNavBaseViewController ()

@end

@implementation GlenNavBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBarHidden = NO; // 使右滑返回手势可用
    self.navigationBar.hidden = YES; // 隐藏导航栏
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
