//
//  ENetWorkManage.m
//  JBT
//
//  Created by Lee on 2018/1/21.
//  Copyright © 2018年 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "ENetWorkManage.h"
#import <AFNetworking/AFNetworking.h>
#import <CommonCrypto/CommonDigest.h>
#import "PPNetworkHelper.h"
#import "GlenLoginViewController.h"
#import "GlenCYLTabBarControllerConfig.h"
#import "MBManager.h"

@implementation ENetWorkManage
+ (void)requestGETWithURLStr: (NSString *)urlStr paramDic: (NSDictionary *)paramDic finish: (void(^)(id responseObject))finish enError: (void(^)(NSError *error))enError{
    
    // 如果含中文转编码
    if ([ENetWorkManage isChinese:urlStr]) {
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    
    [PPNetworkHelper setResponseSerializer:PPResponseSerializerHTTP];
    [PPNetworkHelper setRequestSerializer:PPRequestSerializerHTTP];
    NSString *cookie = [[NSUserDefaults standardUserDefaults] valueForKey:@"cookie"];
    if (cookie.length > 0) {
        NSString *cookieStr = [NSString stringWithFormat:@"connect.sid=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"cookie"]];
        [PPNetworkHelper setValue:cookieStr forHTTPHeaderField:@"cookie"];
    }
    [PPNetworkHelper GET:urlStr parameters:paramDic success:^(id responseObject) {
        
        NSString * str  =[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            if (error.code == 4000) {
                [UerManger cleanLogin];
                UITabBarController  *tabbar = [self cyl_tabBarController];
                GlenLoginViewController *loginVC = [GlenLoginViewController new];
                CYLBaseNavigationController *NAV = [[CYLBaseNavigationController alloc] initWithRootViewController:loginVC];
                loginVC.loginSuccess = ^(BOOL loginSuccess) {
                    if (loginSuccess){
                        
                    }else{
                        UIViewController *vc = [ENetWorkManage getCurrentVC];
                        [vc.navigationController popToRootViewControllerAnimated:NO];
                        [tabbar setSelectedIndex:0];
                    }
                };
                
                [tabbar presentViewController:NAV animated:YES completion:nil];
                enError ([NSError errorWithDomain:@"登录过期" code:4000 userInfo:@{NSLocalizedDescriptionKey:@"登录过期"}]);
            }else{
                finish(str);
            }
            
        }else{
            finish(str);
        }
        
    } failure:^(NSError *error) {
        
        enError(error);
    }];
    
    
}
// POST请求方法
+ (void)requestPOSTWithURLStr:(NSString *)urlStr
                     paramDic: (NSDictionary *)paramDic
                  needGeetest: (BOOL)needGeetest
                       finish: (void(^)(id responseObject))finish
                      enError: (void (^)(NSError *error))enError{
    
    // 如果含中文转编码
    if ([ENetWorkManage isChinese:urlStr]) {
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    
    if (needGeetest) {
        [MBManager hide];
        
        [[Geetest shareInstance] showGeetestSuccse:^(NSString * _Nonnull challenge, NSString * _Nonnull validate, NSString * _Nonnull seccode) {
//            [MBManager showGiF];
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithDictionary:@{@"geetest_challenge":challenge,@"geetest_validate":validate,@"geetest_seccode":seccode}];
            if (paramDic) {
                [dic addEntriesFromDictionary:paramDic];
            }
            
            [PPNetworkHelper setResponseSerializer:PPResponseSerializerHTTP];
            [PPNetworkHelper setRequestSerializer:PPRequestSerializerHTTP];
            NSString *cookie = [[NSUserDefaults standardUserDefaults] valueForKey:@"cookie"];
            if (cookie.length > 0) {
                NSString *cookieStr = [NSString stringWithFormat:@"connect.sid=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"cookie"]];
                [PPNetworkHelper setValue:cookieStr forHTTPHeaderField:@"cookie"];
            }
            
            [PPNetworkHelper POST:urlStr parameters:dic success:^(id responseObject) {
                NSString * str  =[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
                if (model.errors.count) {
                    ErrorsModel *error = model.errors[0];
                    if (error.code == 4000) {
                        [UerManger cleanLogin];
                        UITabBarController  *tabbar = [self cyl_tabBarController];
                        GlenLoginViewController *loginVC = [GlenLoginViewController new];
                        CYLBaseNavigationController *NAV = [[CYLBaseNavigationController alloc] initWithRootViewController:loginVC];
                        loginVC.loginSuccess = ^(BOOL loginSuccess) {
                            if (loginSuccess){
                                
                            }else{
                                UIViewController *vc = [ENetWorkManage getCurrentVC];
                                [vc.navigationController popToRootViewControllerAnimated:NO];
                                [tabbar setSelectedIndex:0];
                            }
                        };
                        
                        [tabbar presentViewController:NAV animated:YES completion:nil];
                        enError ([NSError errorWithDomain:@"登录过期" code:4000 userInfo:@{NSLocalizedDescriptionKey:@"登录过期"}]);
                    }else{
                        finish(str);
                    }
                    
                }else{
                    finish(str);
                }
            } failure:^(NSError *error) {
                
                enError(error);
            }];
        } Cancel:^{
            NSError *erro = [NSError errorWithDomain:NSURLErrorDomain code:121 userInfo:@{NSLocalizedDescriptionKey:@"请求超时"}];
            enError(erro);
        }];
    }else{
        [PPNetworkHelper setResponseSerializer:PPResponseSerializerHTTP];
        [PPNetworkHelper setRequestSerializer:PPRequestSerializerHTTP];
        NSString *cookie = [[NSUserDefaults standardUserDefaults] valueForKey:@"cookie"];
        if (cookie.length > 0) {
            NSString *cookieStr = [NSString stringWithFormat:@"connect.sid=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"cookie"]];
            [PPNetworkHelper setValue:cookieStr forHTTPHeaderField:@"cookie"];
        }
        [PPNetworkHelper POST:urlStr parameters:paramDic success:^(id responseObject) {
            NSString * str  =[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
            if (model.errors.count) {
                ErrorsModel *error = model.errors[0];
                if (error.code == 4000) {
                    [UerManger cleanLogin];
                    UITabBarController  *tabbar = [self cyl_tabBarController];
                    GlenLoginViewController *loginVC = [GlenLoginViewController new];
                    CYLBaseNavigationController *NAV = [[CYLBaseNavigationController alloc] initWithRootViewController:loginVC];
                    loginVC.loginSuccess = ^(BOOL loginSuccess) {
                        if (loginSuccess){
                            
                        }else{
                            UIViewController *vc = [ENetWorkManage getCurrentVC];
                            [vc.navigationController popToRootViewControllerAnimated:NO];
                            [tabbar setSelectedIndex:0];
                        }
                    };
                    
                    [tabbar presentViewController:NAV animated:YES completion:nil];
                    enError ([NSError errorWithDomain:@"登录过期" code:4000 userInfo:@{NSLocalizedDescriptionKey:@"登录过期"}]);
                }else{
                  finish(str);
                }
                
            }else{
            finish(str);
            }
        } failure:^(NSError *error) {
            
            enError(error);
        }];
    }
    
}
// 上传文件请求方法
+ (void)requestUploadFileURLStr:(NSString *)urlStr
                       paramDic: (NSDictionary *)paramDic
                           name:(NSString *)name
                       filePath:(NSString *)filePath
                       progress: (void (^)(NSProgress *progress))uploadProgress
                         finish: (void (^)(id responseObject))finish
                        enError: (void (^)(NSError *error))enError{
    // 如果含中文转编码
    if ([ENetWorkManage isChinese:urlStr]) {
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    
    
    [PPNetworkHelper uploadFileWithURL:urlStr parameters:paramDic name:name filePath:filePath progress:^(NSProgress *progress) {
        uploadProgress (progress);
    } success:^(id responseObject) {
        
        finish(responseObject);
    } failure:^(NSError *error) {
        enError(error);
    }];
}
+(BOOL)isChinese:(NSString *)str {
    for(int i=0; i< [str length];i++){
        int a = [str characterAtIndex:i];
        if( a > 0x4e00 && a < 0x9fff){
            return YES;
        }
    }
    return NO;
}

//获取当前时间的时间戳
+(NSString*)getCurrentTimestamp{
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval a=[dat timeIntervalSince1970];
    NSString*timeString = [NSString stringWithFormat:@"%0.f", a];//转为字符型
    return timeString;
}
//获取MD5签名结果
+(NSString *)getMD5SignResultWithTimeStamp:(NSString *)timeStamp
                                   user_id:(NSString *)user_id
                                     nonce:(NSString *)nonce
                                   version:(NSString *)version
                                     token:(NSString *)token
                                 paramDict:(id)paramDict
                                      type:(NSInteger)type
{
    
    //    NSLog(@"----timeStamp-----%@",timeStamp);
    //    NSLog(@"----userId-----%@",user_id);
    //    NSLog(@"----nonce-----%@",nonce);
    //    NSLog(@"----version-----%@",version);
    //    NSLog(@"----paramDict-----%@",paramDict);
    NSString *paramDictStr;
    if ([paramDict isKindOfClass:[NSDictionary class]]) {
        paramDictStr=[paramDict yy_modelToJSONString];
    }else{
        paramDictStr = paramDict;
        paramDictStr=[paramDictStr stringByReplacingOccurrencesOfString:@"=" withString:@""];
        paramDictStr=[paramDictStr stringByReplacingOccurrencesOfString:@"&" withString:@""];
    }
    NSString *signStr=[NSString stringWithFormat:@"%@%@%@%@%@%@", timeStamp, nonce, user_id, version, token, paramDictStr];
    
    NSLog(@"--token---%@",token);
    NSLog(@"----排列前-----%@",signStr);
    NSString *theStr=[signStr stringByReplacingOccurrencesOfString:@" " withString:@""];
    theStr=[theStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    theStr=[theStr stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    theStr=[theStr stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    
    if (type == 0) {
        theStr=[theStr stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        theStr=[theStr stringByReplacingOccurrencesOfString:@":" withString:@""];
        theStr=[theStr stringByReplacingOccurrencesOfString:@"{" withString:@""];
        theStr=[theStr stringByReplacingOccurrencesOfString:@"}" withString:@""];
        theStr=[theStr stringByReplacingOccurrencesOfString:@"," withString:@""];
        
    }
    //    NSLog(@"----排列后-----%@",theStr);
    NSString *signResultStr=[ENetWorkManage md5:theStr];
    //    NSLog(@"----加密后-----%@",signResultStr);
    return signResultStr.uppercaseString;
}
+ (NSString *)md5:(NSString *)str{
    const char *cStr = [str UTF8String];
    //CC_LONG length = (CC_LONG)strlen(cStr);
    CC_LONG length = (unsigned int)strlen(cStr);
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, length, result); // This is the md5 call
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH];
    
    for(int i = 0; i<CC_MD5_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02X",result[i]];
    }
    return [ret uppercaseString];
}
+ (NSString*)convertToJSONString:(NSDictionary *)infoDict
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:infoDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    NSString *jsonString = @"";
    
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除掉首尾的空白字符和换行字符
    
    [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    return jsonString;
}
//获取当前屏幕显示的viewcontroller
+ (UIViewController *)getCurrentVC
{
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    UIViewController *currentVC = [ENetWorkManage getCurrentVCFrom:rootViewController];
    
    return currentVC;
}

+ (UIViewController *)getCurrentVCFrom:(UIViewController *)rootVC
{
    UIViewController *currentVC;
    
    if ([rootVC presentedViewController]) {
        // 视图是被presented出来的
        
        rootVC = [rootVC presentedViewController];
    }
    
    if ([rootVC isKindOfClass:[UITabBarController class]]) {
        // 根视图为UITabBarController
        
        currentVC = [self getCurrentVCFrom:[(UITabBarController *)rootVC selectedViewController]];
        
    } else if ([rootVC isKindOfClass:[UINavigationController class]]){
        // 根视图为UINavigationController
        
        currentVC = [self getCurrentVCFrom:[(UINavigationController *)rootVC visibleViewController]];
        
    } else {
        // 根视图为非导航类
        
        currentVC = rootVC;
    }
    
    return currentVC;
}
@end
