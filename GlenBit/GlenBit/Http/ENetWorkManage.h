//
//  ENetWorkManage.h
//  JBT
//
//  Created by Lee on 2018/1/21.
//  Copyright © 2018年 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ENetWorkManage : NSObject

+ (void)requestGETWithURLStr: (NSString *)urlStr
                    paramDic: (NSDictionary *)paramDic
                      finish: (void(^)(id responseObject))finish
                     enError: (void(^)(NSError *error))enError;

// POST请求方法
+ (void)requestPOSTWithURLStr:(NSString *)urlStr
                     paramDic: (NSDictionary *)paramDic
                  needGeetest: (BOOL)needGeetest
                       finish: (void(^)(id responseObject))finish
                      enError: (void (^)(NSError *error))enError;

// 上传文件请求方法
+ (void)requestUploadFileURLStr:(NSString *)urlStr
                       paramDic: (NSDictionary *)paramDic
                           name:(NSString *)name
                       filePath:(NSString *)filePath
                       progress: (void (^)(NSProgress *progress))uploadProgress
                         finish: (void (^)(id responseObject))finish
                        enError: (void (^)(NSError *error))enError;


@end
