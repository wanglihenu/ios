//
//  RequestModel.m
//  JBT
//
//  Created by Lee on 2017/10/24.
//  Copyright © 2017年 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "RequestModel.h"

@implementation ErrorsModel

@end

@implementation RequestModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"errors" : [ErrorsModel class]};
}

@end
