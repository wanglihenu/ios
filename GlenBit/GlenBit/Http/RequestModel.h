//
//  RequestModel.h
//  JBT
//
//  Created by Lee on 2017/10/24.
//  Copyright © 2017年 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YYModel.h"

typedef enum : NSUInteger {
    UNKNOWN = 9999,
    REACH_API_LIMIT = 9998,
    VERIFY_CODE_UNMATCHED = 9997,
    INVALID_PARAMETERS = 9996,
    
    // master data
    INVALID_COIN_PAIR = 3000,
    UNSUPPORT_COIN = 3001,
    
    // account related
    UNAUTHENTICATED = 4000,
    DUPLICATE_REGISTRATION = 4001,
    INVALID_USER_INFO = 4002,
    INACTIVATED_USER = 4003,
    GEETEST_FAILURE = 4004,
    ACTIVATION_FAILURE = 4005,
    TWO_STEP_FAILURE = 4006,
    
    // Verification related
    SEND_SMS_VERIFY_CODE_FAILURE = 4101,
    DUPLICATE_VERIFICATION = 4102,
    
    // order related
    MAKE_ORDER_FAILURE = 5000,
    INSUFFICIENT_BALANCE_FOR_ORDER = 5001,
    ILEGAL_ORDER_OPERATION = 5002,
    
    // wallet related
    UNFOUND_COIN = 6000,
    INVALID_ADDRESS = 6001,
    WITHDRAW_FALIURE = 6002,
    INVALID_WITHDRAW_AMOUNT = 6003,
}ErrorType;



@interface ErrorsModel : NSObject

@property (assign, nonatomic) ErrorType code;
@property (copy, nonatomic) NSString *msg;

@end

@interface RequestModel : NSObject

@property (copy, nonatomic) id success;
@property (copy, nonatomic) NSArray *errors;

@end
