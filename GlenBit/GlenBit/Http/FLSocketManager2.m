//
//  FLSocketManager22.m
//  GlenBit
//
//  Created by Lee on 2019/2/13.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "FLSocketManager2.h"
#import "SRWebSocket.h"
@interface FLSocketManager2 ()<SRWebSocketDelegate>
@property (nonatomic,strong)SRWebSocket *webSocket;

@property (nonatomic,assign)FLSocketStatus fl_socketStatus;

@property (nonatomic,weak)NSTimer *timer;
@property (nonatomic,strong)BYTimer *bytime;
@property (nonatomic,copy)NSString *urlString;

@end

@implementation FLSocketManager2{
    NSInteger _reconnectCounter;
}


+ (instancetype)shareManager{
    static FLSocketManager2 *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        instance.overtime = 1;
        instance.reconnectCount = 5;
    });
    return instance;
}

- (void)fl_open:(NSString *)urlStr connect:(FLSocketDidConnectBlock)connect receive:(FLSocketDidReceiveBlock)receive failure:(FLSocketDidFailBlock)failure{
    [FLSocketManager2 shareManager].connect = connect;
    [FLSocketManager2 shareManager].receive = receive;
    [FLSocketManager2 shareManager].failure = failure;
    [self fl_open:urlStr];
}

- (void)fl_close:(FLSocketDidCloseBlock)close{
    [FLSocketManager2 shareManager].close = close;
    [self fl_close];
}

// Send a UTF8 String or Data.
- (void)fl_send:(id)data{
    switch ([FLSocketManager2 shareManager].fl_socketStatus) {
        case FLSocketStatusConnected:
        case FLSocketStatusReceived:{
            NSLog(@"发送中。。。");
            [self.webSocket send:data];
            break;
        }
        case FLSocketStatusFailed:
            NSLog(@"发送失败");
            break;
        case FLSocketStatusClosedByServer:
            NSLog(@"已经关闭");
            break;
        case FLSocketStatusClosedByUser:
            NSLog(@"已经关闭");
            break;
    }
    
}

#pragma mark -- private method
- (void)fl_open:(id)params{
    //    NSLog(@"params = %@",params);
    NSString *urlStr = nil;
    if ([params isKindOfClass:[NSString class]]) {
        urlStr = (NSString *)params;
    }
    else if([params isKindOfClass:[NSTimer class]]){
        NSTimer *timer = (NSTimer *)params;
        urlStr = [timer userInfo];
    }
    _bytime = [BYTimer new];
    [FLSocketManager2 shareManager].urlString = urlStr;
    [self.webSocket close];
    self.webSocket.delegate = nil;
    
    self.webSocket = [[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]]];
    self.webSocket.delegate = self;
    
    [self.webSocket open];
}

- (void)fl_close{
    
    [self.webSocket close];
    self.webSocket = nil;
    [self.bytime stopGCDTimer];
    [self.timer invalidate];
    self.timer = nil;
}

- (void)fl_reconnect{
    // 计数+1
    if (_reconnectCounter < self.reconnectCount - 1) {
        _reconnectCounter ++;
        // 开启定时器
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:self.overtime target:self selector:@selector(fl_open:) userInfo:self.urlString repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        self.timer = timer;
    }
    else{
        NSLog(@"Websocket Reconnected Outnumber ReconnectCount");
        if (self.timer) {
            [self.timer invalidate];
            self.timer = nil;
        }
        return;
    }
    
}

#pragma mark -- SRWebSocketDelegate
- (void)webSocketDidOpen:(SRWebSocket *)webSocket{
    NSLog(@"Websocket Connected");
    
    [FLSocketManager2 shareManager].connect ? [FLSocketManager2 shareManager].connect() : nil;
    [FLSocketManager2 shareManager].fl_socketStatus = FLSocketStatusConnected;
    // 开启成功后重置重连计数器
    _reconnectCounter = 0;
    [self sende];
}
- (void)sende{
    GlenWeakSelf;
    [_bytime startGCDTimerOnMainQueueWithInterval:30 Blcok:^{
        [weakSelf fl_send:@"hb"];
 
    }];
}
- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error{
    NSLog(@":( Websocket Failed With Error %@", error);
    [self.bytime stopGCDTimer];
    [FLSocketManager2 shareManager].fl_socketStatus = FLSocketStatusFailed;
    [FLSocketManager2 shareManager].failure ? [FLSocketManager2 shareManager].failure(error) : nil;
    // 重连
    [self fl_reconnect];
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message{
    if ([message isEqualToString:@"k"]){
        NSLog(@"拿到的K");
        return;
    }else if ([message isEqualToString:@"ping"]){
        NSLog(@"拿到的PING");
        [self fl_send:@"pong"];
        return;
    }
    NSLog(@":( Websocket Receive With message %@", message);
    [FLSocketManager2 shareManager].fl_socketStatus = FLSocketStatusReceived;
    [FLSocketManager2 shareManager].receive ? [FLSocketManager2 shareManager].receive(message,FLSocketReceiveTypeForMessage) : nil;
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean{
    NSLog(@"Closed Reason:%@  code = %zd",reason,code);
    if (reason) {
        [FLSocketManager2 shareManager].fl_socketStatus = FLSocketStatusClosedByServer;
        // 重连
        [self fl_reconnect];
    }
    else{
        [FLSocketManager2 shareManager].fl_socketStatus = FLSocketStatusClosedByUser;
    }
    [FLSocketManager2 shareManager].close ? [FLSocketManager2 shareManager].close(code,reason,wasClean) : nil;
    self.webSocket = nil;
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload{
    [FLSocketManager2 shareManager].receive ? [FLSocketManager2 shareManager].receive(pongPayload,FLSocketReceiveTypeForPong) : nil;
}

- (void)dealloc{
    // Close WebSocket
    [self fl_close];
}
@end
