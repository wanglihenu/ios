//
//  GlenHttpTool.h
//  GlenBit
//
//  Created by Lee on 2019/1/21.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GlenInterface.h"
#import "ENetWorkManage.h"
#import "RequestModel.h"

#import "Coins.h"
#import "Markets.h"
#import "marketPrices.h"
#import "UserModel.h"
#import "orderModel.h"
#import "GlenCountryModel.h"
#import "GoogleModel.h"
#import "GlenContionModel.h"
#import "BalanceModel.h"
#import "ChongzhiModel.h"
#import "TixianModel.h"
#import "TixianInfoModel.h"
#import "KlineModel.h"

@interface GlenHttpTool : NSObject

+ (GlenHttpTool *)shareInstance;


/**
 货币
 
 @param completion 成功
 @param fail 失败
 */
- (void)coinsCompletion:(void (^)(NSArray *coins))completion
                   fail:(void (^)(NSString *error))fail;
/**
 Markets
 
 @param completion 成功
 @param fail 失败
 */
- (void)marketsCompletion:(void (^)(NSArray *coins))completion
                     fail:(void (^)(NSString *error))fail;
/**
 market_prices
 
 @param completion 成功
 @param fail 失败
 */
- (void)marketPricesCompletion:(void (^)(NSArray *coins))completion
                          fail:(void (^)(NSString *error))fail;

/**
 logout
 
 @param completion 成功
 @param fail 失败
 */
- (void)logoutCompletion:(void (^)(NSString *succes))completion
                    fail:(void (^)(NSString *error))fail;

/**
 邮箱注册
 
 @param email 邮箱
 @param password 密码
 @param completion 成功
 @param fail 失败
 */
- (void)signUpWithEmail:(NSString *)email
               password:(NSString *)password
             Completion:(void (^)(NSString *succes))completion
                   fail:(void (^)(NSString *error))fail;
- (void)signUpWithPhone:(NSString *)Phone
               password:(NSString *)password
            countryCode:(NSString *)countryCode
                   code:(NSString *)code
             Completion:(void (^)(NSString *succes))completion
                   fail:(void (^)(NSString *error))fail;
- (void)profileCompletion:(void (^)(UserModel *user))completion
                     fail:(void (^)(NSString *error))fail;

/**
 登录
 
 @param email 邮箱
 @param password 密码
 @param completion 成功
 @param fail 失败
 */
- (void)loginWithEmail:(NSString *)email
              password:(NSString *)password
            Completion:(void (^)(UserModel *user))completion
                  fail:(void (^)(NSString *error))fail;

/**
 登录谷歌两步验证
 
 @param email 邮箱
 @param password 密码
 @param token Google Two-Step Authentication code
 @param completion 成功
 @param fail 失败
 */
- (void)login_gaWithEmail:(NSString *)email
                 password:(NSString *)password
                    token:(NSString *)token
               Completion:(void (^)(UserModel *users))completion
                     fail:(void (^)(NSString *error))fail;

/**
 Send SMS verify code
 
 @param CountryCode 国家编码
 @param phone 手机号
 @param completion 成功
 @param fail 失败
 */
- (void)sendSmsWithCountryCode:(NSString *)CountryCode
                         phone:(NSString *)phone
                    Completion:(void (^)(NSString *succes))completion
                          fail:(void (^)(NSString *error))fail;

/**
 Verify phone number
 
 @param CountryCode 国家编码
 @param phone 手机号
 @param code 验证码
 @param completion 成功
 @param fail 失败
 */
- (void)VerifySmsWithCountryCode:(NSString *)CountryCode
                           phone:(NSString *)phone
                            code:(NSString *)code
                      Completion:(void (^)(NSString *succes))completion
                            fail:(void (^)(NSString *error))fail;

/**
 Try to reset password by Email
 
 @param email 邮箱
 @param completion 成功
 @param fail 失败
 */
- (void)tryRestPassWordWithEmai:(NSString *)email
                     Completion:(void (^)(NSString *succes))completion
                           fail:(void (^)(NSString *error))fail;

/**
 Actually reset password by Email
 
 @param email 邮箱
 @param passWord 密码
 @param code 验证码
 */
- (void)resetPassWordWithEmail:(NSString *)email
                      passWord:(NSString *)passWord
                          code:(NSString *)code
                    Completion:(void (^)(NSString *succes))completion
                          fail:(void (^)(NSString *error))fail;

/**
 Try to reset password by Phone
 
 @param phone 手机号
 @param CountryCode 城市编码
 @param completion 成功
 @param fail 失败
 */
- (void)tryRestPassWordWithPhone:(NSString *)phone
                     CountryCode:(NSString *)CountryCode
                      Completion:(void (^)(NSString *succes))completion
                            fail:(void (^)(NSString *error))fail;

/**
 Actually reset password by Phone
 
 @param phone 手机号
 @param CountryCode 城市编码
 @param passWord 密码
 @param code 验证码
 @param completion 成功
 @param fail 失败
 */
- (void)resetPassWordWithPhone:(NSString *)phone
                   CountryCode:(NSString *)CountryCode
                      passWord:(NSString *)passWord
                          code:(NSString *)code
                    Completion:(void (^)(NSString *succes))completion
                          fail:(void (^)(NSString *error))fail;

/**
 Modify password by Email (for logged in users)
 
 @param passWord 密码
 @param code 验证码
 @param completion 成功
 @param fail 失败
 */
- (void)modif_passWordWithEmailPassWord:(NSString *)passWord
                                   code:(NSString *)code
                             Completion:(void (^)(NSString *succes))completion
                                   fail:(void (^)(NSString *error))fail;
/**
 Modify password by Phone (for logged in users)
 
 @param passWord 密码
 @param code 验证码
 @param completion 成功
 @param fail 失败
 */
- (void)modif_passWordWithPhonePassWord:(NSString *)passWord
                                   code:(NSString *)code
                             Completion:(void (^)(NSString *succes))completion
                                   fail:(void (^)(NSString *error))fail;

/**
 Reset Google Two-step Authentication
 
 @param completion 成功
 @param fail 失败
 */
- (void)resetTwoStepCompletion:(void (^)(NSString *succes))completion
                          fail:(void (^)(NSString *error))fail;

/**
 Verify SMS code for Google Two-step Authentication
 
 @param code 验证码
 @param completion 成功
 @param fail 失败
 */
- (void)twpStepVerifyCode:(NSString *)code
               Completion:(void (^)(GoogleModel *model))completion
                     fail:(void (^)(NSString *error))fail;

/**
 Enable Google Two-step Authentication
 
 @param token Google Two-Step Authentication code
 @param completion 成功
 @param fail 失败
 */
- (void)enableTwpStepVerifyToken:(NSString *)token
                      Completion:(void (^)(NSString *succes))completion
                            fail:(void (^)(NSString *error))fail;
/**
 Disable Google Two-step Authentication
 
 @param token Google Two-Step Authentication code
 @param completion 成功
 @param fail 失败
 */
- (void)disableTwpStepVerifyToken:(NSString *)token
                       Completion:(void (^)(NSString *succes))completion
                             fail:(void (^)(NSString *error))fail;


/**
 Make orde
 
 @param pair 货币类型
 @param type 买卖 0:buy 1:sell
 @param price 价格
 @param amount 数量
 @param completion 成功
 @param fail 失败
 */
- (void)orderMakePair:(NSString *)pair
                 type:(NSInteger)type
                price:(NSString *)price
               amount:(NSString *)amount
           Completion:(void (^)(NSString *succes))completion
                 fail:(void (^)(NSString *error))fail;

/**
 Cancel order
 
 @param pair 货币类型
 @param type 买卖 0:buy 1:sell
 @param orderId 订单id
 @param completion 成功
 @param fail 失败
 */
- (void)orderCancelPair:(NSString *)pair
                   type:(NSString *)type
                orderId:(NSString *)orderId
             Completion:(void (^)(NSString *succes))completion
                   fail:(void (^)(NSString *error))fail;

/**
 All open orders
 
 @param completion 成功
 @param fail 失败
 */
- (void)orderOpenPair:(Markets *)pair
           Completion:(void (^)(NSArray *orders))completion
                 fail:(void (^)(NSString *error))fail;
/**
 Open orders for a pair
 
 @param completion 成功
 @param fail 失败
 */
- (void)orderOpenPairCompletion:(void (^)(NSArray *orders))completion
                           fail:(void (^)(NSString *error))fail;
/**
 Dealt orders for a pair
 
 @param completion 成功
 @param fail 失败
 */
- (void)orderDealtPair:(GlenContionModel *)contion
                  page:(NSInteger)page
                 limit:(NSInteger)limit
            Completion:(void (^)(NSArray *orders))completion
                  fail:(void (^)(NSString *error))fail;

/**
 Balances
 
 @param completion 成功
 @param fail 失败
 */
- (void)walletBalanceCompletion:(void (^)(NSArray *result))completion
                           fail:(void (^)(NSString *error))fail;

/**
 Deposit history
 
 @param completion 成功
 @param fail 失败
 */
- (void)depositHistoryCompletion:(void (^)(NSArray *result))completion
                            fail:(void (^)(NSString *error))fail;
/**
 Withdraw history
 
 @param completion 成功
 @param fail 失败
 */
- (void)withdrawHistoryCompletion:(void (^)(NSArray *result))completion
                            fail:(void (^)(NSString *error))fail;

/**
 Get Deposit address for a coin
 
 @param completion 成功
 @param fail 失败
 */
- (void)addressCoin:(NSString *)coin
         Completion:(void (^)(NSString *succes))completion
               fail:(void (^)(NSString *error))fail;

/**
 Try to withdraw
 
 @param coin 货币
 @param completion 成功
 @param fail 失败
 */
- (void)tryWithdrawCoin:(NSString *)coin
             Completion:(void (^)(TixianInfoModel  *succes))completion
                   fail:(void (^)(NSString *error))fail;

/**
 Actually withdraw
 
 @param address <#address description#>
 @param coin <#coin description#>
 @param amount <#amount description#>
 @param token <#token description#>
 @param completion 成功
 @param fail 失败
 */
- (void)withdrawAddress:(NSString *)address
                   coin:(NSString *)coin
                 amount:(NSString *)amount
                  token:(NSString *)token
             Completion:(void (^)(NSString *succes))completion
                   fail:(void (^)(NSString *error))fail;


/**
 My referral info
 
 @param completion 成功
 @param fail 失败
 */
- (void)referralInfoCompletion:(void (^)(NSString *succes))completion
                          fail:(void (^)(NSString *error))fail;

/**
 My Invitees
 
 @param completion 失败
 @param fail 成功
 */
- (void)referralinvitationsCompletion:(void (^)(NSString *succes))completion
                                 fail:(void (^)(NSString *error))fail;

/**
 My Commissions
 
 @param completion 失败
 @param fail 成功
 */
- (void)referralcommissionsCompletion:(void (^)(NSString *succes))completion
                                 fail:(void (^)(NSString *error))fail;

- (void)kLineType:(NSInteger)type
             pair:(NSString *)pair
       Completion:(void (^)(NSArray *result))completion
                             fail:(void (^)(NSString *error))fail;
@end
