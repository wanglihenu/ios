//
//  GlenHttpTool.m
//  GlenBit
//
//  Created by Lee on 2019/1/21.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenHttpTool.h"
#import "YYModel.h"


@implementation GlenHttpTool

+ (GlenHttpTool *)shareInstance {
    static GlenHttpTool *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[[self class] alloc] init];
    });
    return instance;
}


- (void)coinsCompletion:(void (^)(NSArray *coins))completion
                   fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestGETWithURLStr:url_coins paramDic:nil finish:^(id responseObject) {
        NSArray *result = [NSArray yy_modelArrayWithClass:[Coins class] json:responseObject];
        completion (result);
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
    
}
- (void)marketsCompletion:(void (^)(NSArray *coins))completion
                     fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestGETWithURLStr:url_markets paramDic:nil finish:^(id responseObject) {
        NSArray *result = [NSArray yy_modelArrayWithClass:[Markets class] json:responseObject];
        completion (result);
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)marketPricesCompletion:(void (^)(NSArray *coins))completion
                          fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestGETWithURLStr:url_market_prices paramDic:nil finish:^(id responseObject) {
        NSArray *result = [NSArray yy_modelArrayWithClass:[marketPrices class] json:responseObject];
        completion (result);
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)logoutCompletion:(void (^)(NSString *succes))completion
                    fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_logout paramDic:nil needGeetest:NO finish:^(id responseObject) {
        completion (responseObject);
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)signUpWithEmail:(NSString *)email
               password:(NSString *)password
             Completion:(void (^)(NSString *succes))completion
                   fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_signup paramDic:@{@"email":email,@"password":password,@"lang":@"zh_CN"} needGeetest:YES finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (responseObject);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)signUpWithPhone:(NSString *)Phone
               password:(NSString *)password
            countryCode:(NSString *)countryCode
                   code:(NSString *)code
             Completion:(void (^)(NSString *succes))completion
                   fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_signup_phone paramDic:@{@"phone":Phone,@"password":password,@"countryCode":countryCode,@"code":code} needGeetest:YES finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (responseObject);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)profileCompletion:(void (^)(UserModel *user))completion
                     fail:(void (^)(NSString *error))fail{
    if (![UerManger shareUserManger].isLogin) {
        fail (@"请登录");
        return;
    }
    [ENetWorkManage requestPOSTWithURLStr:url_profile paramDic:nil needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        
        if (model.errors.count) {
            
            [UerManger cleanLogin];
            
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            UserModel *user = [UserModel yy_modelWithJSON:responseObject];
            [UerManger shareUserManger].userModel = user;
            [[NSNotificationCenter defaultCenter]postNotificationName:@"contentSizeDidChangeNotification" object:nil];
            completion (user);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)loginWithEmail:(NSString *)email
              password:(NSString *)password
            Completion:(void (^)(UserModel *users))completion
                  fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_Login paramDic:@{@"email":email,@"password":password} needGeetest:YES finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        
        
        NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL: [NSURL URLWithString:url_Login]];//得到cookie
        NSString *JSESSIONID;
        for (NSHTTPCookie*cookie in cookies) {
            if ([cookie.name isEqualToString:@"connect.sid"]) {
                JSESSIONID = cookie.value;
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:JSESSIONID forKey:@"cookie"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            if (error.code == 4006) {
                fail(@"4006");
            }else{
                fail(error.msg);
            }
        }else{
            UserModel *user = [UserModel yy_modelWithJSON:responseObject];
            [UerManger shareUserManger].userModel = user;
            completion (user);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}

- (void)login_gaWithEmail:(NSString *)email
                 password:(NSString *)password
                    token:(NSString *)token
               Completion:(void (^)(UserModel *users))completion
                     fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_login_ga paramDic:@{@"email":email,@"password":password,@"token":token} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
            
        }else{
            NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL: [NSURL URLWithString:url_Login]];//得到cookie
            NSString *JSESSIONID;
            for (NSHTTPCookie*cookie in cookies) {
                if ([cookie.name isEqualToString:@"connect.sid"]) {
                    JSESSIONID = cookie.value;
                }
            }
            [[NSUserDefaults standardUserDefaults] setObject:JSESSIONID forKey:@"cookie"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            UserModel *user = [UserModel yy_modelWithJSON:responseObject];
            completion (user);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)sendSmsWithCountryCode:(NSString *)CountryCode
                         phone:(NSString *)phone
                    Completion:(void (^)(NSString *succes))completion
                          fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_sms_verify_code paramDic:@{@"countryCode":CountryCode,@"phone":phone} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (responseObject);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)VerifySmsWithCountryCode:(NSString *)CountryCode
                           phone:(NSString *)phone
                            code:(NSString *)code
                      Completion:(void (^)(NSString *succes))completion
                            fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_verify_phone paramDic:@{@"countryCode":CountryCode,@"phone":phone,@"code":code} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (responseObject);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
    
}
- (void)tryRestPassWordWithEmai:(NSString *)email
                     Completion:(void (^)(NSString *succes))completion
                           fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_try_reset_password paramDic:@{@"email":email} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (responseObject);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)resetPassWordWithEmail:(NSString *)email
                      passWord:(NSString *)passWord
                          code:(NSString *)code
                    Completion:(void (^)(NSString *succes))completion
                          fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_reset_password paramDic:@{@"email":email,@"password":passWord,@"code":code} needGeetest:YES finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (responseObject);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)tryRestPassWordWithPhone:(NSString *)phone
                     CountryCode:(NSString *)CountryCode
                      Completion:(void (^)(NSString *succes))completion
                            fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_try_reset_password_phone paramDic:@{@"phone":phone,@"countryCode":CountryCode} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (responseObject);
        }
        completion (responseObject);
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)resetPassWordWithPhone:(NSString *)phone
                   CountryCode:(NSString *)CountryCode
                      passWord:(NSString *)passWord
                          code:(NSString *)code
                    Completion:(void (^)(NSString *succes))completion
                          fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_reset_password_phone paramDic:@{@"phone":phone,@"countryCode":CountryCode,@"password":passWord,@"code":code} needGeetest:YES finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (model.success);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)modif_passWordWithEmailPassWord:(NSString *)passWord
                           code:(NSString *)code
                     Completion:(void (^)(NSString *succes))completion
                           fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_modify_password paramDic:@{@"password":passWord,@"code":code} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (model.success);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)modif_passWordWithPhonePassWord:(NSString *)passWord
                                   code:(NSString *)code
                             Completion:(void (^)(NSString *succes))completion
                                   fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_modify_password_phone paramDic:@{@"password":passWord,@"code":code} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (model.success);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)resetTwoStepCompletion:(void (^)(NSString *succes))completion
                          fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_reset_two_step paramDic:nil needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (responseObject);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)twpStepVerifyCode:(NSString *)code
               Completion:(void (^)(GoogleModel *model))completion
                     fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_twp_step_verify_code paramDic:@{@"code":code} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            GoogleModel *mo = [GoogleModel yy_modelWithJSON:responseObject];
            completion (mo);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)enableTwpStepVerifyToken:(NSString *)token
                      Completion:(void (^)(NSString *succes))completion
                            fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_enable_twp_step paramDic:@{@"token":token} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (model.success);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)disableTwpStepVerifyToken:(NSString *)token
                       Completion:(void (^)(NSString *succes))completion
                             fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_disable_twp_step paramDic:@{@"token":token} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (model.success);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)orderMakePair:(NSString *)pair
                 type:(NSInteger)type
                price:(NSString *)price
               amount:(NSString *)amount
           Completion:(void (^)(NSString *succes))completion
                 fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_make paramDic:@{@"pair":pair,@"type":type == 0?@"buy":@"sell",@"price":price,@"amount":amount} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (model.success);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)orderCancelPair:(NSString *)pair
                   type:(NSString *)type
                orderId:(NSString *)orderId
             Completion:(void (^)(NSString *succes))completion
                   fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_cancel paramDic:@{@"pair":pair,@"type":type,@"orderId":orderId} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (model.success);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)orderOpenPair:(Markets *)pair
           Completion:(void (^)(NSArray *orders))completion
                 fail:(void (^)(NSString *error))fail{
    NSString *url;
    if (pair.marketCoin) {
        url = [NSString stringWithFormat:@"%@%@-%@",url_open_pair,pair.targetCoin,pair.marketCoin];
    }else{
        url = url_open;
    }
    [ENetWorkManage requestPOSTWithURLStr:url paramDic:nil needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            NSArray *result = [NSArray yy_modelArrayWithClass:[orderModel class] json:responseObject];
            completion (result);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];

}
- (void)orderOpenPairCompletion:(void (^)(NSArray *orders))completion
                           fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestGETWithURLStr:url_open_pair paramDic:nil finish:^(id responseObject) {
        NSMutableArray *tempArr = [NSMutableArray new];
        NSArray *result = (NSArray *)responseObject;
        
        for (int i = 0; i < result.count;  i ++ ) {
            orderModel *coin = [orderModel yy_modelWithJSON:result[i]];
            [tempArr addObject:coin];
        }
        completion (tempArr);
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)orderDealtPair:(GlenContionModel *)contion
                  page:(NSInteger)page
                 limit:(NSInteger)limit
            Completion:(void (^)(NSArray *orders))completion
                  fail:(void (^)(NSString *error))fail{
    NSString *url;
    NSMutableDictionary *para = [[NSMutableDictionary alloc]initWithDictionary:@{@"page":[NSString stringWithFormat:@"%ld",(long)page],@"limit":[NSString stringWithFormat:@"%ld",(long)limit]}];
    if (contion.marke.targetCoin) {
        url = [NSString stringWithFormat:@"%@%@-%@",url_dealt_pair,contion.marke.targetCoin,contion.marke.marketCoin];
        if (contion.typeStr) {
            [para setValue:contion.type?@"buy":@"sell" forKey:@"type"];
        }
        
    }else{
        return;
    }
    [ENetWorkManage requestPOSTWithURLStr:url paramDic:para needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            hisModel *result = [hisModel yy_modelWithJSON:responseObject];
            completion (result.docs);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];

}
- (void)walletBalanceCompletion:(void (^)(NSArray *result))completion
                           fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_balance paramDic:nil needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            NSArray *result = [NSArray yy_modelArrayWithClass:[BalanceModel class] json:responseObject];
    
            completion (result);
        }
        
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)depositHistoryCompletion:(void (^)(NSArray *result))completion
                            fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_deposit_history paramDic:nil needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            NSArray *result = [NSArray yy_modelArrayWithClass:[ChongzhiModel class] json:responseObject];
            completion (result);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)withdrawHistoryCompletion:(void (^)(NSArray *result))completion
                             fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_withdraw_history paramDic:nil needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            NSArray *result = [NSArray yy_modelArrayWithClass:[TixianModel class] json:responseObject];
            completion (result);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)addressCoin:(NSString *)coin
         Completion:(void (^)(NSString *succes))completion
               fail:(void (^)(NSString *error))fail;{
    [ENetWorkManage requestPOSTWithURLStr:url_address paramDic:@{@"coin":coin} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (responseObject);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)tryWithdrawCoin:(NSString *)coin
             Completion:(void (^)(TixianInfoModel  *succes))completion
                   fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_try_withdraw paramDic:@{@"coin":coin} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            TixianInfoModel *tixian = [TixianInfoModel yy_modelWithJSON:responseObject];
            completion (tixian);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)withdrawAddress:(NSString *)address
                   coin:(NSString *)coin
                 amount:(NSString *)amount
                  token:(NSString *)token
             Completion:(void (^)(NSString *succes))completion
                   fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_withdraw paramDic:@{@"address":address,@"coin":coin,@"amount":amount,@"token":token} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (model.success);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)referralInfoCompletion:(void (^)(NSString *succes))completion
                          fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_info paramDic:nil needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (model.success);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)referralinvitationsCompletion:(void (^)(NSString *succes))completion
                                 fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_invitations paramDic:nil needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (model.success);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)referralcommissionsCompletion:(void (^)(NSString *succes))completion
                                 fail:(void (^)(NSString *error))fail{
    [ENetWorkManage requestPOSTWithURLStr:url_commissions paramDic:nil needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            completion (model.success);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
- (void)kLineType:(NSInteger)type
             pair:(NSString *)pair
       Completion:(void (^)(NSArray *result))completion
             fail:(void (^)(NSString *error))fail{
    NSString *url;
    NSString *star;
    NSString *endtime;
    [ENetWorkManage requestPOSTWithURLStr:url_k_hour paramDic:@{@"pair":pair,@"start":@"1535187972",@"end":@"1545555930"} needGeetest:NO finish:^(id responseObject) {
        RequestModel *model = [RequestModel yy_modelWithJSON:responseObject];
        if (model.errors.count) {
            ErrorsModel *error = model.errors[0];
            fail(error.msg);
        }else{
            NSArray *result = [NSArray yy_modelArrayWithClass:[KlineModel class] json:responseObject];
            completion (result);
        }
    } enError:^(NSError *error) {
        fail([error localizedDescription]);
    }];
}
@end
