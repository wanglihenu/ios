//
//  GlenTools.m
//  GlenBit
//
//  Created by Lee on 2019/1/10.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "GlenTools.h"

@implementation GlenTools

+ (NSString *)formatString:(NSString *)string scale:(NSInteger)scale{
    NSMutableString *fomatString = [[NSMutableString alloc]initWithString:@"###,##0."];
    for (int i = 0 ; i < scale; i ++) {
        [fomatString appendString:@"0"];
    }
    [fomatString appendString:@";"];
    NSDecimalNumberHandler *hander = [[NSDecimalNumberHandler alloc]initWithRoundingMode:NSRoundPlain scale:scale raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    
    NSDecimalNumber *decimal = [[NSDecimalNumber alloc]initWithString:string?:@"0"];
    decimal = [decimal decimalNumberByRoundingAccordingToBehavior:hander];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = kCFNumberFormatterNoStyle;
    [formatter setPositiveFormat:fomatString];
    return [formatter stringFromNumber:decimal];
}
+ (NSString *)formatString:(NSString *)string{
    NSDecimalNumber *decimal = [[NSDecimalNumber alloc]initWithString:string?:@"0"];
    return decimal.stringValue;
}
+ (NSString *)formatPesent:(NSString *)string scale:(NSInteger)scale{
    NSMutableString *fomatString = [[NSMutableString alloc]initWithString:@"###,##0."];
    for (int i = 0 ; i < scale; i ++) {
        [fomatString appendString:@"0"];
    }
    [fomatString appendString:@";"];
    NSDecimalNumberHandler *hander = [[NSDecimalNumberHandler alloc]initWithRoundingMode:NSRoundPlain scale:scale raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    NSDecimalNumber *decimal = [[NSDecimalNumber alloc]initWithString:string?:@"0"];
    NSDecimalNumber *pesent = [decimal decimalNumberByMultiplyingBy:[[NSDecimalNumber alloc]initWithString:@"100"]];
    
    
    pesent = [pesent decimalNumberByRoundingAccordingToBehavior:hander];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = kCFNumberFormatterNoStyle;
    [formatter setPositiveFormat:fomatString];
    return [NSString stringWithFormat:@"%@%%",[formatter stringFromNumber:pesent]];
}
+ (NSString *)total:(NSString *)price amount:(NSString *)amount scale:(NSInteger)scale{
    NSMutableString *fomatString = [[NSMutableString alloc]initWithString:@"###,##0."];
    for (int i = 0 ; i < scale; i ++) {
        [fomatString appendString:@"0"];
    }
    [fomatString appendString:@";"];
    NSDecimalNumberHandler *hander = [[NSDecimalNumberHandler alloc]initWithRoundingMode:NSRoundPlain scale:scale raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    NSDecimalNumber *decimal = [[NSDecimalNumber alloc]initWithString:price.length?price:@"0"];
    NSDecimalNumber *pesent = [decimal decimalNumberByMultiplyingBy:[[NSDecimalNumber alloc]initWithString:amount.length?amount:@"0"]];
    
    
    pesent = [pesent decimalNumberByRoundingAccordingToBehavior:hander];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = kCFNumberFormatterNoStyle;
    [formatter setPositiveFormat:fomatString];
    return [formatter stringFromNumber:pesent];
}
+ (NSString *)amout:(NSString *)price total:(NSString *)total scale:(NSInteger)scale{
    NSMutableString *fomatString = [[NSMutableString alloc]initWithString:@"###,##0."];
    for (int i = 0 ; i < scale; i ++) {
        [fomatString appendString:@"0"];
    }
    [fomatString appendString:@";"];
    NSDecimalNumberHandler *hander = [[NSDecimalNumberHandler alloc]initWithRoundingMode:NSRoundPlain scale:scale raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    NSDecimalNumber *decimal = [[NSDecimalNumber alloc]initWithString:total?:@"0"];
    NSDecimalNumber *pesent = [decimal decimalNumberByDividingBy:[[NSDecimalNumber alloc]initWithString:price.length?price:@"1"]];
    
    
    pesent = [pesent decimalNumberByRoundingAccordingToBehavior:hander];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = kCFNumberFormatterNoStyle;
    [formatter setPositiveFormat:fomatString];
    return [formatter stringFromNumber:pesent];
}
+ (NSString *)addtotal:(NSString *)one amount:(NSString *)two scale:(NSInteger)scale{
    NSMutableString *fomatString = [[NSMutableString alloc]initWithString:@"###,##0."];
    for (int i = 0 ; i < scale; i ++) {
        [fomatString appendString:@"0"];
    }
    [fomatString appendString:@";"];
    NSDecimalNumberHandler *hander = [[NSDecimalNumberHandler alloc]initWithRoundingMode:NSRoundPlain scale:scale raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    NSDecimalNumber *decimal = [[NSDecimalNumber alloc]initWithString:one?:@"0"];
    NSDecimalNumber *pesent = [decimal decimalNumberByAdding:[[NSDecimalNumber alloc]initWithString:two?:@"0"]];
    
    
    pesent = [pesent decimalNumberByRoundingAccordingToBehavior:hander];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = kCFNumberFormatterNoStyle;
    [formatter setPositiveFormat:fomatString];
    return [formatter stringFromNumber:pesent];
}
+ (NSString *)totalBalance:(NSArray *)markets balanse:(NSArray *)balanec coin:(NSString *)coin scale:(NSInteger)scale{
    
    NSDecimalNumber *total = [[NSDecimalNumber alloc]initWithString:@"0"];
    for (BalanceModel *model in balanec) {
        total = [total decimalNumberByAdding:[GlenTools getTemTotal:[[NSDecimalNumber alloc]initWithString:[self rateFrom:model.coin to:coin lastRate:nil]] free:model]];
    }
    return [GlenTools formatString:total.stringValue scale:scale];
}

+ (NSString *)rateFrom:(NSString *)from to:(NSString *)to lastRate:(NSString *)lastRate{
    NSString *newprice;
    NSArray *markets = [UerManger shareUserManger].marke_price;
    NSPredicate  *toPredicate1 = [NSPredicate predicateWithFormat:@"%K == %@", @"to",to];
    ///所有可以转为usd的数组
    NSArray *toCanUsed1 = [markets filteredArrayUsingPredicate:toPredicate1];
    for (marketPrices *price in toCanUsed1) {
        if ([price.from isEqualToString:from]) {
            newprice  = price.price;
            break;
        }
    }
    if (newprice) {
        if (lastRate) {
            NSDecimalNumber *now = [[NSDecimalNumber alloc]initWithString:lastRate];
            NSDecimalNumber *newp = [now decimalNumberByMultiplyingBy:[[NSDecimalNumber alloc]initWithString:newprice]];
            return newp.stringValue;
        }else{
           return newprice;
        }
        
    }else{
        for (marketPrices *price in toCanUsed1) {
            NSString *str = [GlenTools rateFrom:from to:price.from lastRate:price.price];
            if (![str isEqualToString:@"没找到"]) {
                return str;
            }
        }
    }
    return @"没找到";
}
+ (NSDecimalNumber *)getTemTotal:(NSDecimalNumber *)price free:(BalanceModel *)balance{
    NSDecimalNumber *fee = [[NSDecimalNumber alloc]initWithString:balance.free];
    NSDecimalNumber *total = [fee decimalNumberByAdding:[[NSDecimalNumber alloc]initWithString:balance.locked]];
    NSDecimalNumber *tempToal = [total decimalNumberByMultiplyingBy:price];
    return tempToal;
}
@end
