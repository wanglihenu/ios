//
//  GlenTools.h
//  GlenBit
//
//  Created by Lee on 2019/1/10.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlenTools : NSObject

+ (NSString *)formatString:(NSString *)string scale:(NSInteger)scale;
+ (NSString *)formatString:(NSString *)string;
+ (NSString *)formatPesent:(NSString *)string scale:(NSInteger)scale;
+ (NSString *)total:(NSString *)price amount:(NSString *)amount scale:(NSInteger)scale;
+ (NSString *)amout:(NSString *)price total:(NSString *)total scale:(NSInteger)scale;
+ (NSString *)addtotal:(NSString *)one amount:(NSString *)two scale:(NSInteger)scale;
+ (NSString *)totalBalance:(NSArray *)markets balanse:(NSArray *)balanec coin:(NSString *)coin scale:(NSInteger)scale;
+ (NSString *)rateFrom:(NSString *)from to:(NSString *)to lastRate:(NSString *)lastRate;
@end
