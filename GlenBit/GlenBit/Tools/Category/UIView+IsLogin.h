//
//  UIView+IsLogin.h
//  EWallet
//
//  Created by Lee on 2018/3/16.
//  Copyright © 2018年 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (IsLogin)

- (void)isneetLogin:(BOOL)isNeedLogin LoginSucces:(void (^)(void))login Nologin:(void (^)(void))Nologin LoginFaile:(void (^)(void))faile;
@end
