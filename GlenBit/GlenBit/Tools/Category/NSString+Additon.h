//
//  NSString+Additon.h
//  GlenBit
//
//  Created by Lee on 2019/1/21.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additon)


/**
 验证邮箱地址

 @return 结果
 */
- (BOOL)isEmail;

/**
 获取当前时间戳

 @return 时间戳
 */
+(NSString*)getCurrentTimestamp;


/**
 时间戳转时间

 @param timestamp 时间戳
 @return 时间
 */
+ (NSString *)StringFromTimestamp:(NSString *)timestamp;
/**
 时间戳转时间
 
 @param timestamp 时间戳
 @return 时间
 */
+ (NSString *)StringfENFromTimestamp:(NSString *)timestamp;

/**
 字符串转时间戳

 @param string 字符串
 @return 时间戳
 */
+ (NSString *)TimestampFromString:(NSString *)string;

/**
 字符串转日期

 @param string 字符串
 @return 日期
 */
+ (NSDate *)dateFromString:(NSString *)string;

/**
 日期转时间戳

 @param date 日期
 @return 时间戳
 */
+ (NSString *)TimestampFromDate:(NSDate *)date;

@end
