//
//  UIView+IsLogin.m
//  EWallet
//
//  Created by Lee on 2018/3/16.
//  Copyright © 2018年 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "UIView+IsLogin.h"
#import "GlenLoginViewController.h"
#import "UserModel.h"
#import "GlenCYLTabBarControllerConfig.h"

@implementation UIView (IsLogin)

- (void)isneetLogin:(BOOL)isNeedLogin LoginSucces:(void (^)(void))login Nologin:(void (^)(void))Nologin LoginFaile:(void (^)(void))faile{
    
    if (isNeedLogin){
        if ([UerManger shareUserManger].isLogin){
            login();
        }else{
            GlenLoginViewController *loginVC= [GlenLoginViewController new];
            CYLBaseNavigationController *NAV = [[CYLBaseNavigationController alloc] initWithRootViewController:loginVC];

            loginVC.loginSuccess = ^(BOOL loginSuccess) {
                if (loginSuccess){
                    login();
                }else{
                    faile();
                }
            };
            [[self viewController] presentViewController:NAV animated:YES completion:nil];
        }
    }else{
        if ([UerManger shareUserManger].isLogin){
            login();
        }else{
            Nologin();
        }
    }
}
@end
