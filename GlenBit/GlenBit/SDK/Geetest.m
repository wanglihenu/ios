//
//  Geetest.m
//  GlenBit
//
//  Created by Lee on 2019/1/21.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import "Geetest.h"
#import <GT3Captcha/GT3Captcha.h>

@interface Geetest () <UITextFieldDelegate,GT3CaptchaManagerDelegate>

@property (nonatomic, strong) GT3CaptchaManager *manager;
@property (nonatomic, strong) HandleResclut result;
@property (nonatomic, strong) HandleCancel cancel;
@end

@implementation Geetest

+ (Geetest *)shareInstance {
    static Geetest *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[[self class] alloc] init];
    });
    return instance;
}

- (void)showGeetestSuccse:(HandleResclut)succse Cancel:(HandleCancel)cancel{
    _manager = [[GT3CaptchaManager alloc] initWithAPI1:[NSString stringWithFormat:@"https://rest.glenbit.com/gt/register?t=%@",[NSString getCurrentTimestamp]] API2:nil timeout:5.0];
//#ifdef DEBUG
//    [_manager disableSecurityAuthentication:YES];
//#endif
//    [_manager useVisualViewWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    [_manager registerCaptcha:nil];
    _manager.delegate = self;
    [_manager startGTCaptchaWithAnimated:YES];
    _result = succse;
    _cancel = cancel;
}
#pragma mark - GT3CaptchaManager Delegate 行为验证代理方法

- (void)gtCaptchaUserDidCloseGTView:(GT3CaptchaManager *)manager {
    [[Toast makeUpText:@"用户取消了行为验证"] showWithType:ShortTime];
    if (self.cancel) {
        self.cancel();
    }
}

- (void)gtCaptcha:(GT3CaptchaManager *)manager errorHandler:(GT3Error *)error {
    [[Toast makeUpText:error.description] showWithType:ShortTime];
    if (self.cancel) {
        self.cancel();
    }
}
- (BOOL)shouldUseDefaultRegisterAPI:(GT3CaptchaManager *)manager{
    return YES;
}
// disable secondary validate when using OnePass
- (BOOL)shouldUseDefaultSecondaryValidate:(GT3CaptchaManager *)manager {
    return NO;
}

- (void)gtCaptcha:(GT3CaptchaManager *)manager didReceiveSecondaryCaptchaData:(NSData *)data response:(NSURLResponse *)response error:(GT3Error *)error decisionHandler:(void (^)(GT3SecondaryCaptchaPolicy))decisionHandler {
    // If `shouldUseDefaultSecondaryValidate:` return NO, do nothing here
    
}

// put captcha result into OnePass
- (void)gtCaptcha:(GT3CaptchaManager *)manager didReceiveCaptchaCode:(NSString *)code result:(NSDictionary *)result message:(NSString *)message {
    
    if (![code isEqualToString:@"1"]) return;
    
    NSString *challenge = [result objectForKey:@"geetest_challenge"];
    NSString *validate = [result objectForKey:@"geetest_validate"];
    NSString *seccode = [result objectForKey:@"geetest_seccode"];
    if (!validate || validate.length != 32) return;
    if (self.result) {
        self.result(challenge, validate, seccode);
    }
    [_manager stopGTCaptcha];
}

- (void)gtCaptcha:(GT3CaptchaManager *)manager willSendRequestAPI1:(NSURLRequest *)originalRequest withReplacedHandler:(void (^)(NSURLRequest *))replacedHandler {
    // add timestamp to api1
    NSMutableURLRequest *mRequest = [originalRequest mutableCopy];
    
    NSURLComponents *comp = [[NSURLComponents alloc] initWithString:mRequest.URL.absoluteString];
    NSMutableArray *items = [comp.queryItems mutableCopy];
    [items enumerateObjectsUsingBlock:^(NSURLQueryItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSURLQueryItem *item = (NSURLQueryItem *)obj;
        if (item.name && [item.name isEqualToString:@"t"]) {
            NSString *time = [NSString stringWithFormat:@"%.0f", [[NSDate date] timeIntervalSince1970] * 1000];
            [items removeObject:obj];
            NSURLQueryItem *new = [[NSURLQueryItem alloc] initWithName:@"t" value:time];
            [items addObject:new];
        }
    }];
    comp.queryItems = items;
    mRequest.URL = comp.URL;
    
    replacedHandler(mRequest);
}

@end
