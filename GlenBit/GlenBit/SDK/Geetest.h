//
//  Geetest.h
//  GlenBit
//
//  Created by Lee on 2019/1/21.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^HandleResclut)(NSString *challenge, NSString *validate, NSString *seccode);
typedef void(^HandleCancel)(void);

@interface Geetest : NSObject

+ (Geetest *)shareInstance;

- (void)showGeetestSuccse:(HandleResclut)succse Cancel:(HandleCancel)cancel;

@end

NS_ASSUME_NONNULL_END
