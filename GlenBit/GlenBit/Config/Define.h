//
//  Define.h
//  DDClass
//
//  Created by Lee on 2018/9/11.
//  Copyright © 2018年 河南贝利塔网络科技有限公司. All rights reserved.
//

#ifndef Define_h
#define Define_h



#define NOTIFTICKER @"NOTIFTICKER"
#define NOTIFTRADING @"NOTIFTRADING"


// 颜色(RGB)
#define RGBCOLOR(r, g, b)       [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define RGBACOLOR(r, g, b, a)   [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]

// 弱引用
#define GlenWeakSelf __weak typeof(self) weakSelf = self;

#define RandColor RGBColor(arc4random_uniform(255), arc4random_uniform(255), arc4random_uniform(255))

#define CYL_DEPRECATED(explain) __attribute__((deprecated(explain)))
#define DD_IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define DD_IS_IOS_11  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.f)
#define DD_IS_IPHONE_X (DD_IS_IOS_11 && DD_IS_IPHONE && ((MIN([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height) == 375 && MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height) == 812)||(MIN([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height) == 414 && MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height) == 896)))


#define rightWidth                 230
#define SafeMargin                 (DD_IS_IPHONE_X ? 34.f : 0.f)
#define size_height                 [UIScreen mainScreen].bounds.size.height - (DD_IS_IPHONE_X ? 34.f : 0.f)
#define size_width                  [UIScreen mainScreen].bounds.size.width
#define StatusBar_height [[UIApplication sharedApplication] statusBarFrame].size.height
#define TabBarHeight 60
#define ScaleSize(size) (floor((size)/375.0*size_width))
#define kImageWithName(imgName)     [UIImage imageNamed:imgName]
#define NULLString(string) ((![string isKindOfClass:[NSString class]])||[string isEqualToString:@""] || (string == nil) || [string isEqualToString:@""] || [string isKindOfClass:[NSNull class]]||[[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0)


#endif /* Define_h */
