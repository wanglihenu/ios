//
//  GlenInterface.h
//  GlenBit
//
//  Created by Lee on 2019/1/21.
//  Copyright © 2019 河南贝利塔网络科技有限公司. All rights reserved.
//

#ifndef GlenInterface_h
#define GlenInterface_h


#ifdef DEBUG //处于开发阶段
#define DevepMent 1  //1为开发接口， 2为正式接口
#else //处于开发阶段
#define DevepMent 0  //0为正式接口，永远不要改
#endif


/****开发接口*****/
#if DevepMent == 1
#define url_base                    @"https://rest.glenbit.com/"
/****正式接口*****/
#elif DevepMent == 2
#define url_base                    @"https://rest.glenbit.com/"
/****打包接口*****/
#elif DevepMent == 0
#define url_base                    @"https://rest.glenbit.com/"
#endif

#pragma mark   Coins
/***************Coins*******************/
#define url_coins [url_base stringByAppendingString:@"coins"]

#pragma mark   Markets
/***************Markets*******************/
#define url_markets [url_base stringByAppendingString:@"markets"]

#pragma mark   Market Price
/***************Markets*******************/
#define url_market_prices [url_base stringByAppendingString:@"market_prices"]



#pragma mark   Account 账号接口
/***************账号基础接口*******************/
#define url_Account [url_base stringByAppendingString:@"account/"]
/***************User info*******************/
#define url_profile [url_Account stringByAppendingString:@"profile"]
/***************Log out*******************/
#define url_logout [url_Account stringByAppendingString:@"logout"]
/***************Sign up with Email*******************/
#define url_signup [url_Account stringByAppendingString:@"signup"]
/***************Sign up with Phone*******************/
#define url_signup_phone [url_Account stringByAppendingString:@"signup_phone"]
/***************Login*******************/
#define url_Login [url_Account stringByAppendingString:@"login"]
/***************Login if Google Two-Step Authentication is turned ON*******************/
#define url_login_ga [url_Account stringByAppendingString:@"login_ga"]
/***************Send SMS verify code*******************/
#define url_sms_verify_code [url_Account stringByAppendingString:@"sms_verify_code"]
/***************Verify phone number*******************/
#define url_verify_phone [url_Account stringByAppendingString:@"verify_phone"]
/***************Try to reset password by Email*******************/
#define url_try_reset_password [url_Account stringByAppendingString:@"try_reset_password"]
/***************Actually reset password by Email*******************/
#define url_reset_password [url_Account stringByAppendingString:@"reset_password"]
/***************Try to reset password by Phone*******************/
#define url_try_reset_password_phone [url_Account stringByAppendingString:@"try_reset_password_phone"]
/***************Actually reset password by Phone*******************/
#define url_reset_password_phone [url_Account stringByAppendingString:@"reset_password_phone"]
/***************Modify password by Email (for logged in users)*******************/
#define url_modify_password [url_Account stringByAppendingString:@"modify_password"]

/***************Modify password by phone (for logged in users)*******************/
#define url_modify_password_phone [url_Account stringByAppendingString:@"modify_password_phone"]
/***************Reset Google Two-step Authentication*******************/
#define url_reset_two_step [url_Account stringByAppendingString:@"reset_two_step"]
/***************Verify SMS code for Google Two-step Authentication*******************/
#define url_twp_step_verify_code [url_Account stringByAppendingString:@"two_step_verify_code"]
/***************Enable Google Two-step Authentication*******************/
#define url_enable_twp_step [url_Account stringByAppendingString:@"enable_two_step"]
/***************Disable Google Two-step Authentication*******************/
#define url_disable_twp_step [url_Account stringByAppendingString:@"disable_two_step"]



#pragma mark   Trade 交易接口
/***************交易基础接口*******************/
#define url_Trade [url_base stringByAppendingString:@"order/"]
/***************Make order*******************/
#define url_make [url_Trade stringByAppendingString:@"make"]
/***************Cancel order*******************/
#define url_cancel [url_Trade stringByAppendingString:@"cancel"]
/*************** All open orders*******************/
#define url_open [url_Trade stringByAppendingString:@"open"]
/***************Open orders for a pair*******************/
#define url_open_pair [url_Trade stringByAppendingString:@"open/"]
/***************Dealt orders for a pair*******************/
#define url_dealt_pair [url_Trade stringByAppendingString:@"dealt/"]

#pragma mark   Wallet 钱包接口
/***************钱包基础接口*******************/
#define url_wallet [url_base stringByAppendingString:@"wallet/"]
/***************Balances*******************/
#define url_balance [url_wallet stringByAppendingString:@"balance"]
/***************Deposit history*******************/
#define url_deposit_history [url_wallet stringByAppendingString:@"deposit_history"]
/***************Withdraw history*******************/
#define url_withdraw_history [url_wallet stringByAppendingString:@"withdraw_history"]
/***************Get Deposit address for a coin*******************/
#define url_address [url_wallet stringByAppendingString:@"address"]
/***************Try to withdraw*******************/
#define url_try_withdraw [url_wallet stringByAppendingString:@"try_withdraw"]
/***************Actually withdraw*******************/
#define url_withdraw [url_wallet stringByAppendingString:@"withdraw"]

#pragma mark   Referral Commissions 钱包接口
/***************Referral Commissions*******************/
#define url_referral [url_base stringByAppendingString:@"referral/"]
/***************My referral info*******************/
#define url_info [url_referral stringByAppendingString:@"info"]
/***************My Invitees*******************/
#define url_invitations [url_referral stringByAppendingString:@"invitations"]
/***************My Commissions*******************/
#define url_commissions [url_referral stringByAppendingString:@"commissions"]
/***************K-line*******************/
#define url_K_line @"https://tv-feeder.glenbit.com/history/"

#define url_k_hour [url_K_line stringByAppendingString:@"hour"]
#define url_k_minute [url_K_line stringByAppendingString:@"minute"]

#endif /* GlenInterface_h */
